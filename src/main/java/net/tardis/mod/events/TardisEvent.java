package net.tardis.mod.events;

import net.minecraftforge.eventbus.api.Cancelable;
import net.minecraftforge.eventbus.api.Event;
import net.tardis.mod.tileentities.ConsoleTile;

@Cancelable
public class TardisEvent extends Event{

	ConsoleTile tile;
	
	public TardisEvent(ConsoleTile tile) {
		this.tile = tile;
	}
	
	public ConsoleTile getConsole() {
		return tile;
	}
	
	public static class Takeoff extends TardisEvent{

		public Takeoff(ConsoleTile tile) {
			super(tile);
		}
		
	}
	
	public static class Land extends TardisEvent{

		public Land(ConsoleTile tile) {
			super(tile);
		}
		
	}
}
