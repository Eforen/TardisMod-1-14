package net.tardis.mod.commands.subcommands;

import java.util.Collection;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.helper.TardisHelper;

public class LoyaltyCommand extends TCommand{

	public static final LoyaltyCommand CMD = new LoyaltyCommand(PermissionEnum.UNLOCK);
	
	public LoyaltyCommand(PermissionEnum permission) {
		super(permission);
	}

	@Override
	public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
		return Command.SINGLE_SUCCESS;
	}

	public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
		return Commands.literal("loyalty")
		        .requires(context -> context.hasPermissionLevel(2))
		        .then(Commands.argument("amount", IntegerArgumentType.integer(0, 200))
		            .executes(context -> {
		                return setLoyaltyForSelf(context, context.getSource().asPlayer());
		            })
		            .then(Commands.argument("targets", EntityArgument.players())
				        .executes(context -> {
	                        return setLoyalty(context, EntityArgument.getPlayers(context, "targets"));
	                    })));
	}
	
	private static int setLoyalty(CommandContext<CommandSource> context, ServerPlayerEntity player){
        CommandSource source = context.getSource();
        int loyaltyToSet = context.getArgument("amount", Integer.class);
        TardisHelper.getConsole(context.getSource().getServer(), player.getUniqueID()).ifPresent(tile -> {
            tile.getEmotionHandler().setLoyalty(loyaltyToSet);
        });
        source.sendFeedback(new TranslationTextComponent("message.tardis.loyalty.success", player.getDisplayName(), loyaltyToSet), true);
        return Command.SINGLE_SUCCESS;
    }
	
	private static int setLoyaltyForSelf(CommandContext<CommandSource> context, ServerPlayerEntity player)  throws CommandSyntaxException {
	    return setLoyalty(context, player);
	}
	
	private static int setLoyalty(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets) throws CommandSyntaxException {
	    targets.forEach(player -> {
	       setLoyalty(context, player);
	    });
	    return Command.SINGLE_SUCCESS;
	}
	
}
