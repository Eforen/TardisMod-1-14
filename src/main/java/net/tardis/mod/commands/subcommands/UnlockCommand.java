package net.tardis.mod.commands.subcommands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

public class UnlockCommand extends TCommand{
    
	public UnlockCommand(PermissionEnum permission) {
		super(permission);
	}

	@Override
	public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
		return Command.SINGLE_SUCCESS;
	}
	
	 public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
	        return Commands.literal("unlock")
	                .requires((context) -> context.hasPermissionLevel(2))
	                .then(Commands.argument("targets", EntityArgument.players()) //We need to use Commands.literial, then duplicate the argument name to make this compatible with the execute command. Mojang hardcodes the execute command to force us to do this :/
                        .then(Commands.literal("exterior").then(Commands.argument("item", StringArgumentType.greedyString()).suggests((context, builder) -> ISuggestionProvider.suggest(getExteriorEntries(), builder)).executes(context -> unlockObject(context, EntityArgument.getPlayers(context, "targets"), RegistryType.EXTERIOR, StringArgumentType.getString(context, "item")))))
                        .then(Commands.literal("interior").then(Commands.argument("item", StringArgumentType.greedyString()).suggests((context, builder) -> ISuggestionProvider.suggest(getInteriorEntries(), builder)).executes(context -> unlockObject(context, EntityArgument.getPlayers(context, "targets"), RegistryType.INTERIOR, StringArgumentType.getString(context, "item"))))))
	                .then(Commands.literal("exterior").then(Commands.argument("item", StringArgumentType.greedyString()).suggests((context, builder) -> ISuggestionProvider.suggest(getExteriorEntries(), builder)).executes(context -> unlockObjectForSelf(context, context.getSource().asPlayer(), RegistryType.EXTERIOR, StringArgumentType.getString(context, "item")))))
                    .then(Commands.literal("interior").then(Commands.argument("item", StringArgumentType.greedyString()).suggests((context, builder) -> ISuggestionProvider.suggest(getInteriorEntries(), builder)).executes(context -> unlockObjectForSelf(context, context.getSource().asPlayer(), RegistryType.INTERIOR, StringArgumentType.getString(context, "item")))));
	 }
	 
	 private static int unlockObject(CommandContext<CommandSource> context, ServerPlayerEntity player, RegistryType type, String item) {
	     CommandSource source = context.getSource();
	     ConsoleTile console = TardisHelper.getConsole(context.getSource().getServer(), player.getUniqueID()).orElse(null);
         //For exteriors
         if(type == RegistryType.EXTERIOR) {
             if(item.contentEquals("*")) {
                 for(IExterior ext : ExteriorRegistry.getRegistry().values()) {
                     Helper.unlockExterior(console, player, ext);
                 }
                 source.sendFeedback(new TranslationTextComponent("command.tardis.unlock.exterior_all", player.getDisplayName()), true);
             }
             else {
                 IExterior ext = ExteriorRegistry.getExterior(new ResourceLocation(item));
                 Helper.unlockExterior(console, player, ext);
                 source.sendFeedback(new TranslationTextComponent("command.tardis.unlock.exterior", ext.getDisplayName(), player.getDisplayName()), true);
             }
             
         }
         //For interior
         else if(type == RegistryType.INTERIOR) {
             if(item.contentEquals("*")) {
                 for(ConsoleRoom room : ConsoleRoom.REGISTRY.values()) {
                     Helper.unlockInterior(console, player, room);
                 }
                 source.sendFeedback(new TranslationTextComponent("command.tardis.unlock.interior_all", player.getDisplayName()), true);
             }
             else {
                 ConsoleRoom room = ConsoleRoom.REGISTRY.get(new ResourceLocation(item));
                 Helper.unlockInterior(console, player, room);
                 source.sendFeedback(new TranslationTextComponent("command.tardis.unlock.interior", room.getDisplayName(), player.getDisplayName()), true);
             }
         }
         console.updateClient();
         return Command.SINGLE_SUCCESS;
	 }
	 
	 private static int unlockObjectForSelf(CommandContext<CommandSource> context, ServerPlayerEntity player, RegistryType type, String item) throws CommandSyntaxException {
	     return unlockObject(context, player, type, item);
	 }
	 
	 private static int unlockObject(CommandContext<CommandSource> context, Collection<ServerPlayerEntity> targets, RegistryType type, String item) throws CommandSyntaxException {
	    targets.forEach(target -> {
	        unlockObject(context, target, type, item);
	    });
        return Command.SINGLE_SUCCESS;
	 }
	 
	 private static Collection<String> getExteriorEntries(){
	     List<String> items = new ArrayList<String>();
         items.add("*");
         for(IExterior ext : ExteriorRegistry.getRegistry().values()) {
             items.add(ext.getRegistryName().toString());
         }
         return items;
	 }
	 
	 private static Collection<String> getInteriorEntries(){
         List<String> items = new ArrayList<String>();
         items.add("*");
         for(ConsoleRoom room : ConsoleRoom.REGISTRY.values()) {
             items.add(room.getRegistryName().toString());
         }
         return items;
     }
	 
	 public enum RegistryType {
	     INTERIOR,
	     EXTERIOR
	 }

}
