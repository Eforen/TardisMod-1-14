package net.tardis.mod.network;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import net.tardis.mod.Tardis;
import net.tardis.mod.network.packets.ARSDeleteMessage;
import net.tardis.mod.network.packets.ARSSpawnMessage;
import net.tardis.mod.network.packets.BOTIMessage;
import net.tardis.mod.network.packets.BessieHornMessage;
import net.tardis.mod.network.packets.BrokenTardisSpawn;
import net.tardis.mod.network.packets.ChangeExtAnimationMessage;
import net.tardis.mod.network.packets.ChangeExtVarMessage;
import net.tardis.mod.network.packets.ChangeHumMessage;
import net.tardis.mod.network.packets.ChangeInteriorMessage;
import net.tardis.mod.network.packets.CommunicatorMessage;
import net.tardis.mod.network.packets.CompleteMissionMessage;
import net.tardis.mod.network.packets.ConsoleChangeMessage;
import net.tardis.mod.network.packets.ConsoleRoomSyncMessage;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.ConsoleVariantMessage;
import net.tardis.mod.network.packets.DiagnosticMessage;
import net.tardis.mod.network.packets.ExteriorChangeMessage;
import net.tardis.mod.network.packets.FailEngineMessage;
import net.tardis.mod.network.packets.HoloObservatoryMessage;
import net.tardis.mod.network.packets.LightUpdateMessage;
import net.tardis.mod.network.packets.MissControlMessage;
import net.tardis.mod.network.packets.MissionUpdateMessage;
import net.tardis.mod.network.packets.MonitorRemoteMessage;
import net.tardis.mod.network.packets.ProtocolMessage;
import net.tardis.mod.network.packets.QuantiscopeTabMessage;
import net.tardis.mod.network.packets.RecipeSyncMessage;
import net.tardis.mod.network.packets.RequestBotiMessage;
import net.tardis.mod.network.packets.SendTardisDistressSignal;
import net.tardis.mod.network.packets.SetMissionStageMessage;
import net.tardis.mod.network.packets.SnapMessage;
import net.tardis.mod.network.packets.SonicPartChangeMessage;
import net.tardis.mod.network.packets.StopHumMessage;
import net.tardis.mod.network.packets.SyncPlayerMessage;
import net.tardis.mod.network.packets.SyncSonicMessage;
import net.tardis.mod.network.packets.TelepathicMessage;
import net.tardis.mod.network.packets.UpdateARSTablet;
import net.tardis.mod.network.packets.UpdateControlMessage;
import net.tardis.mod.network.packets.UpdateManualPageMessage;
import net.tardis.mod.network.packets.UpdateTransductionMessage;
import net.tardis.mod.network.packets.VMDistressMessage;
import net.tardis.mod.network.packets.VMFunctionMessage;
import net.tardis.mod.network.packets.VMTeleportMessage;
import net.tardis.mod.network.packets.WaypointDeleteMessage;
import net.tardis.mod.network.packets.WaypointLoadMessage;
import net.tardis.mod.network.packets.WaypointOpenMessage;
import net.tardis.mod.network.packets.WaypointSaveMessage;
import net.tardis.mod.network.packets.WorldShellEntityMessage;

public class Network {

	private static int id = 0;
    private static final String PROTOCOL_VERSION = Integer.toString(1);
    private static final SimpleChannel NETWORK_CHANNEL = NetworkRegistry.newSimpleChannel(new ResourceLocation(Tardis.MODID, "main_channel"), () -> PROTOCOL_VERSION, PROTOCOL_VERSION::equals, PROTOCOL_VERSION::equals);

    
    public static void init() {
        NETWORK_CHANNEL.registerMessage(++id, UpdateControlMessage.class, UpdateControlMessage::encode, UpdateControlMessage::decode, UpdateControlMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, ProtocolMessage.class, ProtocolMessage::encode, ProtocolMessage::decode, ProtocolMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, LightUpdateMessage.class, LightUpdateMessage::encode, LightUpdateMessage::decode, LightUpdateMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, VMTeleportMessage.class, VMTeleportMessage::encode, VMTeleportMessage::decode, VMTeleportMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, ChangeInteriorMessage.class, ChangeInteriorMessage::encode, ChangeInteriorMessage::decode, ChangeInteriorMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, ConsoleChangeMessage.class, ConsoleChangeMessage::encode, ConsoleChangeMessage::decode, ConsoleChangeMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, SyncSonicMessage.class, SyncSonicMessage::encode, SyncSonicMessage::decode, SyncSonicMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, ARSSpawnMessage.class, ARSSpawnMessage::encode, ARSSpawnMessage::decode, ARSSpawnMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, SonicPartChangeMessage.class, SonicPartChangeMessage::encode, SonicPartChangeMessage::decode, SonicPartChangeMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, UpdateManualPageMessage.class, UpdateManualPageMessage::encode, UpdateManualPageMessage::decode, UpdateManualPageMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, QuantiscopeTabMessage.class, QuantiscopeTabMessage::encode, QuantiscopeTabMessage::decode, QuantiscopeTabMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, StopHumMessage.class, StopHumMessage::encode, StopHumMessage::decode, StopHumMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, ChangeHumMessage.class, ChangeHumMessage::encode, ChangeHumMessage::decode, ChangeHumMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, BessieHornMessage.class, BessieHornMessage::encode, BessieHornMessage::decode, BessieHornMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, ExteriorChangeMessage.class, ExteriorChangeMessage::encode, ExteriorChangeMessage::decode, ExteriorChangeMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, WaypointSaveMessage.class, WaypointSaveMessage::encode, WaypointSaveMessage::decode, WaypointSaveMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, WaypointLoadMessage.class, WaypointLoadMessage::encode, WaypointLoadMessage::decode, WaypointLoadMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, WaypointDeleteMessage.class, WaypointDeleteMessage::encode, WaypointDeleteMessage::decode, WaypointDeleteMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, ChangeExtAnimationMessage.class, ChangeExtAnimationMessage::encode, ChangeExtAnimationMessage::decode, ChangeExtAnimationMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, TelepathicMessage.class, TelepathicMessage::encode, TelepathicMessage::decode, TelepathicMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, UpdateARSTablet.class, UpdateARSTablet::encode, UpdateARSTablet::decode, UpdateARSTablet::handle);
        NETWORK_CHANNEL.registerMessage(++id, CommunicatorMessage.class, CommunicatorMessage::encode, CommunicatorMessage::decode, CommunicatorMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, VMFunctionMessage.class, VMFunctionMessage::encode, VMFunctionMessage::decode, VMFunctionMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, ChangeExtVarMessage.class, ChangeExtVarMessage::encode, ChangeExtVarMessage::decode, ChangeExtVarMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, ConsoleVariantMessage.class, ConsoleVariantMessage::encode, ConsoleVariantMessage::decode, ConsoleVariantMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, VMDistressMessage.class, VMDistressMessage::encode, VMDistressMessage::decode, VMDistressMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, ConsoleUpdateMessage.class, ConsoleUpdateMessage::encode, ConsoleUpdateMessage::decode, ConsoleUpdateMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, UpdateTransductionMessage.class, UpdateTransductionMessage::encode, UpdateTransductionMessage::decode, UpdateTransductionMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, RequestBotiMessage.class, RequestBotiMessage::encode, RequestBotiMessage::decode, RequestBotiMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, ARSDeleteMessage.class, ARSDeleteMessage::encode, ARSDeleteMessage::decode, ARSDeleteMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, SnapMessage.class, SnapMessage::encode, SnapMessage::decode, SnapMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, MonitorRemoteMessage.class, MonitorRemoteMessage::encode, MonitorRemoteMessage::decode, MonitorRemoteMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, HoloObservatoryMessage.class, HoloObservatoryMessage::encode, HoloObservatoryMessage::decode, HoloObservatoryMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, WaypointOpenMessage.class, WaypointOpenMessage::encode, WaypointOpenMessage::decode, WaypointOpenMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, SendTardisDistressSignal.class, SendTardisDistressSignal::encode, SendTardisDistressSignal::decode, SendTardisDistressSignal::handle);
        NETWORK_CHANNEL.registerMessage(++id, FailEngineMessage.class, FailEngineMessage::encode, FailEngineMessage::decode, FailEngineMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, CompleteMissionMessage.class, CompleteMissionMessage::encode, CompleteMissionMessage::decode, CompleteMissionMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, SetMissionStageMessage.class, SetMissionStageMessage::encode, SetMissionStageMessage::decode, SetMissionStageMessage::handle);
        
        //To Client
        NETWORK_CHANNEL.registerMessage(++id, MissControlMessage.class, MissControlMessage::encode, MissControlMessage::decode, MissControlMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, BrokenTardisSpawn.class, BrokenTardisSpawn::encode, BrokenTardisSpawn::decode, BrokenTardisSpawn::handle);
        NETWORK_CHANNEL.registerMessage(++id, BOTIMessage.class, BOTIMessage::encode, BOTIMessage::decode, BOTIMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, RecipeSyncMessage.class, RecipeSyncMessage::encode, RecipeSyncMessage::decode, RecipeSyncMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, ConsoleRoomSyncMessage.class, ConsoleRoomSyncMessage::encode, ConsoleRoomSyncMessage::decode, ConsoleRoomSyncMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, DiagnosticMessage.class, DiagnosticMessage::encode, DiagnosticMessage::decode, DiagnosticMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, WorldShellEntityMessage.class, WorldShellEntityMessage::encode, WorldShellEntityMessage::decode, WorldShellEntityMessage::handle);
        NETWORK_CHANNEL.registerMessage(++id, MissionUpdateMessage.class, MissionUpdateMessage::encode, MissionUpdateMessage::decode, MissionUpdateMessage::handle);
        
        NETWORK_CHANNEL.registerMessage(++id, SyncPlayerMessage.class, SyncPlayerMessage::encode, SyncPlayerMessage::decode, SyncPlayerMessage::handle);
    }

    /**
     * Sends a packet to the server.<br>
     * Must be called Client side.
     */
    public static void sendToServer(Object msg) {
        NETWORK_CHANNEL.sendToServer(msg);
    }

    /**
     * Send a packet to a specific player.<br>
     * Must be called Server side.
     */
    public static void sendTo(Object msg, ServerPlayerEntity player) {
        if (!(player instanceof FakePlayer)) {
            NETWORK_CHANNEL.send(PacketDistributor.PLAYER.with(() -> player), msg);
        }
    }

    public static void sendPacketToAll(Object packet) {
    	NETWORK_CHANNEL.send(PacketDistributor.ALL.noArg(), packet);
    }

	public static SUpdateTileEntityPacket createTEUpdatePacket(TileEntity tile) {
		return new SUpdateTileEntityPacket(tile.getPos(), -1, tile.getUpdateTag());
	}

	public static void sendToAllAround(Object mes, DimensionType dim, BlockPos pos, int radius) {
        NETWORK_CHANNEL.send(PacketDistributor.NEAR.with(() -> new PacketDistributor.TargetPoint(pos.getX(), pos.getY(), pos.getZ(), radius, dim)), mes);
	}

	public static void sendToAllInWorld(Object mes, ServerWorld world) {
        NETWORK_CHANNEL.send(PacketDistributor.DIMENSION.with(world.dimension::getType), mes);
	}

}
