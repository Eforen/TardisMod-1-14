package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.client.guis.TelepathicScreen.Type;
//import net.tardis.mod.config.TConfig;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

public class TelepathicMessage {
	
//	private static final TranslationTextComponent INVALID = new TranslationTextComponent("status.tardis.telepathic.not_found");
//	private static final TranslationTextComponent INVALID_BIOME = new TranslationTextComponent("status.tardis.telepathic.biome.not_found");
	private static final TranslationTextComponent VALID = new TranslationTextComponent("status.tardis.telepathic.success");
    private String name;
    private Type type;

    public TelepathicMessage(Type type, String key) {
        this.name = key;
        this.type = type;
    }

    public static void encode(TelepathicMessage mes, PacketBuffer buf) {
        buf.writeInt(mes.type.ordinal());
        buf.writeString(mes.name, 64);
    }

    public static TelepathicMessage decode(PacketBuffer buf) {
        return new TelepathicMessage(Type.values()[buf.readInt()], buf.readString(64));
    }

    public static void handle(TelepathicMessage mes, Supplier<NetworkEvent.Context> cont) {
        cont.get().enqueueWork(() -> {
            TileEntity te = cont.get().getSender().world.getTileEntity(TardisHelper.TARDIS_POS);
            if (te instanceof ConsoleTile) {
                ConsoleTile console = (ConsoleTile) te;
                ServerWorld world = console.getWorld().getServer().getWorld(console.getDestinationDimension());
                if (world != null) {
                    BlockPos dest = console.getDestination();
                    PlayerEntity player = cont.get().getSender();
//                    int configRadius = TConfig.CONFIG.telepathicSearchRadius.get();
                    int rangeLimit = 500;
//                    int searchRadius = configRadius >= 500 ? rangeLimit : configRadius; //Use config value if not above 500 blocks
                    switch (mes.type) {
					case BIOME:
						BlockPos biome = world.getChunkProvider().getChunkGenerator().getBiomeProvider()
							.findBiomePosition(dest.getX(), dest.getZ(), rangeLimit, Lists.newArrayList(ForgeRegistries.BIOMES.getValue(new ResourceLocation(mes.name))), world.rand);
						
						if(biome != null) {
                    		console.setDestination(console.getDestinationDimension(), biome.add(0, 64, 0));
                    		player.sendStatusMessage(VALID, true);
                    		
                    	}
						else player.sendStatusMessage(new TranslationTextComponent("status.tardis.telepathic.biome.not_found", rangeLimit), true);
						break;
					case SPAWN:
						BlockPos spawn = world.getSpawnPoint();
                		if(spawn != null) {
                			console.setDestination(console.getDestinationDimension(), console.randomizeCoords(spawn, 25));
                			player.sendStatusMessage(VALID, true);
                		}
						break;
					case STRUCTURE:
                		BlockPos pos = world.findNearestStructure(mes.name, console.getDestination(), 100, true);
                		if(pos != null) {
                			console.setDestination(console.getDestinationDimension(), pos);
                		}
						break;
					default:
						break;
                    }
                }
            }
        });
        cont.get().setPacketHandled(true);
    }

}
