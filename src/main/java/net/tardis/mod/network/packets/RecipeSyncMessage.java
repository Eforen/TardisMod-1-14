package net.tardis.mod.network.packets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.recipe.WeldRecipe;

public class RecipeSyncMessage {

	public static final int STRING_SIZE = 16;
	private List<WeldRecipe> recipes = new ArrayList<WeldRecipe>();
	private HashMap<ResourceLocation, String> dimNames = new HashMap<ResourceLocation, String>();
	
	public RecipeSyncMessage(List<WeldRecipe> recipes, HashMap<ResourceLocation, String> dimNames) {
		this.recipes.clear();
		this.recipes.addAll(recipes);
	}
	
	public static void encode(RecipeSyncMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.recipes.size());
		for(WeldRecipe rec : mes.recipes) {
			buf.writeCompoundTag(rec.serialize());
		}
		buf.writeInt(mes.dimNames.size());
		for(Entry<ResourceLocation, String> names : mes.dimNames.entrySet()) {
			buf.writeResourceLocation(names.getKey());
			buf.writeString(names.getValue(), STRING_SIZE);
		}
	}
	
	public static RecipeSyncMessage decode(PacketBuffer buf) {
		int size = buf.readInt();
		
		List<WeldRecipe> rec = new ArrayList<WeldRecipe>();
		
		for(int i = 0; i < size; ++i) {
			rec.add(WeldRecipe.deserialize(buf.readCompoundTag()));
		}

		int dimSize = buf.readInt();
		HashMap<ResourceLocation, String> dimNames = new HashMap<ResourceLocation, String>();
		
		for(int i = 0; i < dimSize; ++i) {
			dimNames.put(buf.readResourceLocation(), buf.readString(STRING_SIZE));
		}
		
		return new RecipeSyncMessage(rec, dimNames);

	}
	
	public static void handle(RecipeSyncMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			WeldRecipe.WELD_RECIPE.clear();
			WeldRecipe.WELD_RECIPE.addAll(mes.recipes);

			//ModelRegistry.DIMENSION_NAMES.clear();
			//ModelRegistry.DIMENSION_NAMES.putAll(mes.dimNames);

		});
	}
}
