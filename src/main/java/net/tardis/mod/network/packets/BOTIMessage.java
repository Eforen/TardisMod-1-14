package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.boti.IBotiEnabled;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.cap.Capabilities;

public class BOTIMessage {

	WorldShell shell = null;
	BlockPos pos = null;
	int type = 0;
	
	public BOTIMessage(BlockPos pos, WorldShell shell) {
		this.type = 0;
		this.pos = pos;
		this.shell = shell;
	}
	
	public BOTIMessage(WorldShell shell) {
		this.type = 1;
		this.shell = shell;
	}
	
	public static void encode(BOTIMessage mes, PacketBuffer buf) {
		
		buf.writeInt(mes.type);
		if(mes.type == 0)
			buf.writeBlockPos(mes.pos);
		
		mes.shell.writeToBuffer(buf);
	}
	
	public static BOTIMessage decode(PacketBuffer buf) {
		
		int type = buf.readInt();
		if(type == 0)
			return new BOTIMessage(buf.readBlockPos(), WorldShell.readFromBuffer(buf));
		
		return new BOTIMessage(WorldShell.readFromBuffer(buf));
	}
	
	public static void handle(BOTIMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			if(mes.type == 0) {
				TileEntity te = Minecraft.getInstance().world.getTileEntity(mes.pos);
				if(te instanceof IBotiEnabled) {
					IBotiEnabled boti = (IBotiEnabled)te;
					combineOrSetBOTI(boti, mes.shell);
				}
			}
			//If meant for the world cap
			else {
				Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
					combineOrSetBOTI(data, mes.shell);
				});
			}
		});
		cont.get().setPacketHandled(true);
	}
	
	public static void combineOrSetBOTI(IBotiEnabled boti, WorldShell shell) {
		
		if(boti.getBotiWorld() == null || !boti.getBotiWorld().getOffset().equals(shell.getOffset())) {
			boti.setBotiWorld(shell);
			shell.setNeedsUpdate(true);
			return;
		}
		
		if(boti.getBotiWorld().getOffset().equals(shell.getOffset())) {
			boti.getBotiWorld().combine(shell);
		}
		
	}
	
}
