package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class SnapMessage {

	public SnapMessage() {}
	
	public static void encode(SnapMessage mes, PacketBuffer buf) {}
	
	public static SnapMessage decode(PacketBuffer buf) {
		return new SnapMessage();
	}
	
	public static void handle(SnapMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			
			TardisHelper.getConsole(context.get().getSender().server, context.get().getSender().getUniqueID()).ifPresent(tile -> {
				if(tile.getEmotionHandler().getLoyalty() > 100) {
					int consoleRange = 20;
					boolean withinDistanceOfExterior = context.get().getSender().getPosition().withinDistance(tile.getLocation(), consoleRange);
					if(withinDistanceOfExterior) {
						ExteriorTile ext = tile.getExterior().getExterior(tile);
						if(ext != null) {
							if(ext.getLocked())
								ext.setLocked(false);
							ext.setDoorState(ext.getOpen() != EnumDoorState.CLOSED ? EnumDoorState.CLOSED : EnumDoorState.BOTH);
							ext.setOther();
							context.get().getSender().world.playSound(null, context.get().getSender().getPosition(), TSounds.SNAP, SoundCategory.PLAYERS, 0.2F, 1F);
						}
					}
					else if(tile.getWorld().dimension.getType() == context.get().getSender().dimension) {
						tile.getDoor().ifPresent(door -> {
							int range = 16;
							if(door.getPosition().withinDistance(context.get().getSender().getPosition(), range)) {
								door.setOpenState(door.getOpenState() != EnumDoorState.CLOSED ? EnumDoorState.CLOSED : EnumDoorState.BOTH);
								door.updateOther();
								context.get().getSender().world.playSound(null, context.get().getSender().getPosition(), TSounds.SNAP, SoundCategory.PLAYERS, 0.2F, 1F);
							}
						});
					}
				}
			});
		});
		context.get().setPacketHandled(true);
	}
}
