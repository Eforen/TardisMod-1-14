package net.tardis.mod.items;

import net.minecraft.entity.LivingEntity;

public interface ISpaceHelmet {

	boolean shouldSufficate(LivingEntity ent);
}
