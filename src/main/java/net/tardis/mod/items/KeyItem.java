package net.tardis.mod.items;

import java.util.List;
import java.util.UUID;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.UsernameCache;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.upgrades.KeyFobUpgrade;

public class KeyItem extends BaseItem{
	
	public KeyItem() {
		super(new Properties().maxStackSize(1).group(TItemGroups.MAIN));
	}
	
	public static void setOwner(ItemStack stack, UUID id) {
		if(!stack.hasTag())
			stack.setTag(new CompoundNBT());
		stack.getTag().putString("tardis_key", id.toString());
	}
	
	public static UUID getOwner(ItemStack stack) {
		if(stack.hasTag() && stack.getTag().contains("tardis_key")) {
			return UUID.fromString(stack.getTag().getString("tardis_key"));
		}
		return null;
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		if(getOwner(stack) != null) {
			tooltip.add(new TranslationTextComponent("tooltip.key.owner", UsernameCache.containsUUID(getOwner(stack)) ? UsernameCache.getLastKnownUsername(getOwner(stack)) : getOwner(stack).toString()));
		}
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}

	@Override
	public void inventoryTick(ItemStack stack, World worldIn, Entity entity, int itemSlot, boolean isSelected) {
		if (!worldIn.isRemote) {
			if(entity instanceof PlayerEntity && getOwner(stack) == null) {
				PlayerEntity player = (PlayerEntity)entity;
				if (player != null)
					setOwner(stack, player.getUniqueID());
			}
		}
		super.inventoryTick(stack, worldIn, entity, itemSlot, isSelected);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		ItemStack stack = playerIn.getHeldItem(handIn);
		if(stack.getItem() instanceof KeyItem) {
			if(!worldIn.isRemote) {
				TardisHelper.getConsole(worldIn.getServer(), getOwner(stack)).ifPresent(tile -> {
					tile.getUpgrade(KeyFobUpgrade.class).ifPresent(fob -> {
						if(fob.isUsable()) {
							//Open near the exterior
							if(tile.getLocation().withinDistance(playerIn.getPosition(), 16) && tile.getDimension() == playerIn.dimension) {
								ExteriorTile ext = tile.getExterior().getExterior(tile);
								if(ext != null) {
									ext.setLocked(!ext.getLocked());
									if(ext.getLocked()) {
										ext.setDoorState(EnumDoorState.CLOSED);
									}
									ext.setOther();
									worldIn.playSound(null, playerIn.getPosition(), TSounds.CAR_LOCK, SoundCategory.BLOCKS, 1F, 1F);
									playerIn.sendStatusMessage(ext.getLocked() ? ExteriorBlock.LOCKED : ExteriorBlock.UNLOCKED, true);
								}
							}
							//Open in the TARDIS dimension
							else if(playerIn.dimension == tile.getWorld().getDimension().getType()) {
								tile.getDoor().ifPresent(door -> {
									door.setLocked(!door.isLocked());
									if(door.isLocked())
										door.setOpenState(EnumDoorState.CLOSED);
									door.updateOther();
									worldIn.playSound(null, playerIn.getPosition(), TSounds.CAR_LOCK, SoundCategory.BLOCKS, 1F, 1F);
								});
							}
						}
					});
				});
			}
			return ActionResult.newResult(ActionResultType.SUCCESS, stack);
		}
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}
	
	
}
