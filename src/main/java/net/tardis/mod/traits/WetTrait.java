package net.tardis.mod.traits;

import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.Direction;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;
import net.tardis.mod.tileentities.console.misc.TardisTrait;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class WetTrait extends TardisTrait{

	public WetTrait(TardisTraitType type) {
		super(type);
	}

	@Override
	public void tick(ConsoleTile tile) {
		ExteriorTile ext = tile.getExterior().getExterior(tile);
		
		double mod = 1.0 - this.getWeight();
		
		if(ext != null && tile.getWorld().getGameTime() % (int)Math.ceil(60 + 100 * mod) == 0){
			
			if(!ext.getBlockState().has(BlockStateProperties.HORIZONTAL_FACING))
				return;
			
			Direction dir = ext.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING);
			if(ext.getWorld().getFluidState(ext.getPos().offset(dir)).isTagged(FluidTags.WATER)) {
				if(tile.getEmotionHandler().getMood() < EnumHappyState.CONTENT.getTreshold()) {
					tile.getEmotionHandler().addMood(1);
					this.warnPlayer(tile, LIKES_LOCATION, 400);
				}
			}
		}
	}

}
