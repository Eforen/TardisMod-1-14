package net.tardis.mod.traits;

import java.util.function.Function;
import java.util.function.Predicate;

import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.console.misc.TardisTrait;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class Traits {

	public static final TardisTraitType MURDEROUS = register(MurderousTrait::new, test -> test.getTraitClass() == PeaceTrait.class, "murderous");
	public static final TardisTraitType PEACEFUL = register(PeaceTrait::new, test -> test.getTraitClass() == MurderousTrait.class, "peaceful");
	public static final TardisTraitType WET = register(WetTrait::new, "wet");
	public static final TardisTraitType ARACHOPHOBIA = register(ArachnophobiaTrait::new, "arachnophobia");
	public static final TardisTraitType PYRO = register(PyroTrait::new, "pyro");
	public static final TardisTraitType COLDBLOODED = register(ColdBloodedTrait::new, "cold_blood");
	public static final TardisTraitType JEALOUS = register(JealousTrait::new, "jealous");
	public static final TardisTraitType CLAUSTROPHOBIC = register(ClaustrophobicTrait::new, "claustrophobic");
	public static final TardisTraitType AGORAPHOBIC = register(AgoraphobicTrait::new, "agoraphobic");
	
	public static TardisTraitType register(Function<TardisTraitType, TardisTrait> supplier, Predicate<TardisTraitType> test, String name){
		TardisTraitType type = new TardisTraitType(supplier, test);
		type.setRegistryName(Helper.createRL(name));
		return type;
	}
	
	public static TardisTraitType register(Function<TardisTraitType, TardisTrait> supplier, String name) {
		return register(supplier, test -> false, name);
	}
	
	@SubscribeEvent
	public static void register(Register<TardisTraitType> event) {
		event.getRegistry().registerAll(
				WET,
				MURDEROUS,
				PEACEFUL,
				ARACHOPHOBIA,
				PYRO,
				COLDBLOODED,
				JEALOUS,
				CLAUSTROPHOBIC
		);
	}
}
