package net.tardis.mod.cap;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.boti.IBotiEnabled;
import net.tardis.mod.energy.TardisEnergy;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public interface ITardisWorldData extends INBTSerializable<CompoundNBT>, IBotiEnabled{
	
	PanelInventory getEngineInventoryForSide(Direction dir);
	TardisEnergy getEnergy();
	ItemStackHandler getItemBuffer();
	void tick();
	
	public static class TardisWorldProvider implements ICapabilitySerializable<CompoundNBT>{

		private ITardisWorldData data;
		final private LazyOptional<ITardisWorldData> holder;
		
		public TardisWorldProvider(World world) {
			this.data = new TardisWorldCapability(world);
			this.holder = LazyOptional.of(() -> this.data);
		}
		
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
			return cap == Capabilities.TARDIS_DATA ? this.holder.cast() : LazyOptional.empty();
		}

		@Override
		public CompoundNBT serializeNBT() {
			return data.serializeNBT();
		}

		@Override
		public void deserializeNBT(CompoundNBT nbt) {
			data.deserializeNBT(nbt);	
		}
		
	}
	
	public static class TardisWorldStorage implements IStorage<ITardisWorldData>{

		@Override
		public INBT writeNBT(Capability<ITardisWorldData> capability, ITardisWorldData instance, Direction side) {
			return instance.serializeNBT();
		}

		@Override
		public void readNBT(Capability<ITardisWorldData> capability, ITardisWorldData instance, Direction side, INBT nbt) {
			instance.deserializeNBT((CompoundNBT)nbt);
		}
		
	}

}
