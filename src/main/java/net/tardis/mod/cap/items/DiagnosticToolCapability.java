package net.tardis.mod.cap.items;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.google.common.collect.Lists;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceContext.BlockMode;
import net.minecraft.util.math.RayTraceContext.FluidMode;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.TardisDiagnosticItem;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.DiagnosticMessage;
import net.tardis.mod.network.packets.DiagnosticMessage.ArtronUseInfo;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.subsystem.SubsystemInfo;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.ArtronUse;

public class DiagnosticToolCapability implements IDiagnostic{

	private ItemStack stack;
	private UUID owner;
	private ConsoleTile tile;
	private int power;
	private List<SubsystemInfo> subsystems = new ArrayList<SubsystemInfo>();
	private float rotationToTarget;
	private boolean isOn = false;
	
	public DiagnosticToolCapability(ItemStack stack) {
		this.stack = stack;
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		
		ListNBT list = new ListNBT();
		for(SubsystemInfo s : this.subsystems) {
			list.add(s.serializeNBT());
		}
		tag.put("subsystems", list);
		tag.putInt("power", this.power);
		if(owner != null)
			tag.putUniqueId("owner", this.owner);
		tag.putFloat("rotation_target", this.rotationToTarget);
		tag.putBoolean("on", this.isOn);
		
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		ListNBT list = tag.getList("subsystems", NBT.TAG_COMPOUND);
		this.subsystems.clear();
		for(INBT nbt : list) {
			this.subsystems.add(new SubsystemInfo((CompoundNBT)nbt));
		}
		this.power = tag.getInt("power");
		if(tag.contains("owner"))
			this.owner = tag.getUniqueId("owner");
		this.rotationToTarget = tag.getFloat("rotation_target");
		this.isOn = tag.getBoolean("on");
	}

	@Override
	public ConsoleTile getConsoleTile() {
		if(tile == null || tile.isRemoved()) {
			TardisHelper.getConsole(ServerLifecycleHooks.getCurrentServer(), this.owner).ifPresent(t -> this.tile = t);
		}
		return this.tile;
	}

	@Override
	public List<SubsystemInfo> getSubsystems() {
		return this.subsystems;
	}

	@Override
	public void tick(LivingEntity liv) {
		if(!liv.world.isRemote) {
			
			if(this.owner == null)
				this.owner = liv.getUniqueID();
			
			if(liv.world.getGameTime() % 20 == 0) {
				Vec3d pos = liv.getPositionVec().add(0, liv.getEyeHeight(), 0);
				RayTraceResult res = liv.world.rayTraceBlocks(new RayTraceContext(pos, pos.add(liv.getLookVec().scale(12)), BlockMode.COLLIDER, FluidMode.NONE, liv));
				if(res instanceof BlockRayTraceResult) {
					BlockRayTraceResult block = (BlockRayTraceResult)res;
					TileEntity te = liv.world.getTileEntity(block.getPos());
					if(te instanceof ConsoleTile) {
						this.subsystems.clear();
						for(Subsystem s : ((ConsoleTile)te).getSubSystems()){
							this.subsystems.add(new SubsystemInfo(s));
						}
						te.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> this.power = cap.getEnergy().getEnergyStored());
					}
				}
				
				if(this.getConsoleTile() != null && !this.getConsoleTile().isRemoved()) { 
					//Checks for exterior if not inside Tardis and checks for console block if inside Tardis
					this.rotationToTarget = Helper.getAngleBetweenPoints(liv.getPositionVector(), Helper.vecFromPos(liv.world.getDimension().getType().getModType() != TDimensions.TARDIS ? this.getConsoleTile().getLocation() : this.getConsoleTile().getPos()));
					
					float diff = Helper.getFixedRotation(Helper.getFixedRotation(liv.rotationYaw) - Helper.getFixedRotation(this.rotationToTarget));
					if((diff > 350 || diff < 10)) {
						liv.world.playSound(null, liv.getPosition(), TSounds.REMOTE_ACCEPT, SoundCategory.BLOCKS, 1F, 1F);
						this.isOn = true;
					}
					else {
						this.isOn = false;
					}
					
				}
				TardisDiagnosticItem.syncCapability(stack);
			}
		}
	}

	@Override
	public UUID getOwner() {
		return this.owner;
	}

	@Override
	public void setOwner(UUID owner) {
		this.owner = owner;
	}
	
	private void update() {
		TardisDiagnosticItem.syncCapability(stack);
	}

	@Override
	public int getPower() {
		return power;
	}
	
	public void onRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		if(!worldIn.isRemote) {
			if(this.owner == null) {
				this.owner = playerIn.getUniqueID();
				update();
			}
			ConsoleTile console = this.getConsoleTile();
			if(console != null) {
				
				this.subsystems.clear();
				for(Subsystem s : console.getSubSystems()) {
					this.subsystems.add(new SubsystemInfo(s));
				}
				
				List<ArtronUseInfo> uses = Lists.newArrayList();
				for(ArtronUse use : console.getArtronUses().values()) {
					if(use.isActive()) {
						uses.add(new ArtronUseInfo(use));
					}
				}
				
				Network.sendTo(new DiagnosticMessage(this.subsystems, console.getPositionInFlight(), uses), (ServerPlayerEntity)playerIn);
			}
		}
	}

	@Override
	public float getAngleFromTarget() {
		return this.rotationToTarget;
	}

	@Override
	public void setOn(boolean on) {
		this.isOn = on;
	}

	@Override
	public boolean getOn() {
		return this.isOn;
	}

}
