package net.tardis.mod.cap.items;

import java.util.List;
import java.util.UUID;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.subsystem.SubsystemInfo;
import net.tardis.mod.tileentities.ConsoleTile;

public interface IDiagnostic extends INBTSerializable<CompoundNBT>{

	UUID getOwner();
	void setOwner(UUID owner);
	int getPower();
	
	ConsoleTile getConsoleTile();
	
	List<SubsystemInfo> getSubsystems();
	float getAngleFromTarget();
	
	void tick(LivingEntity liv);
	void onRightClick(World worldIn, PlayerEntity playerIn, Hand handIn);
	//Item Property stuff
	void setOn(boolean on);
	boolean getOn();
	
	public static class Storage implements Capability.IStorage<IDiagnostic>{

		@Override
		public INBT writeNBT(Capability<IDiagnostic> capability, IDiagnostic instance, Direction side) {
			return instance.serializeNBT();
		}

		@Override
		public void readNBT(Capability<IDiagnostic> capability, IDiagnostic instance, Direction side, INBT nbt) {
			instance.deserializeNBT((CompoundNBT)nbt);
		}
		
	}
	
	public static class Provider implements ICapabilitySerializable<CompoundNBT>{

		LazyOptional<IDiagnostic> opt;
		
		public Provider(IDiagnostic loc) {
			this.opt = LazyOptional.of(() -> loc);
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
			return cap == Capabilities.DIAGNOSTIC ? (LazyOptional<T>)opt : LazyOptional.empty();
		}

		@Override
		public CompoundNBT serializeNBT() {
			return opt.orElse(null).serializeNBT();
		}

		@Override
		public void deserializeNBT(CompoundNBT nbt) {
			opt.ifPresent(cap -> cap.deserializeNBT(nbt));
		}
		
	}
}
