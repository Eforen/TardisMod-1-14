package net.tardis.mod.world.feature;

import java.util.Random;
import java.util.function.Function;

import com.mojang.datafixers.Dynamic;

import net.minecraft.block.BlockState;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationSettings;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.world.WorldGen;

public class TardisGenFeature extends Feature<NoFeatureConfig>{

	public TardisGenFeature(Function<Dynamic<?>, ? extends NoFeatureConfig> configFactoryIn) {
		super(configFactoryIn);
	}

	@Override
	public boolean place(IWorld worldIn, ChunkGenerator<? extends GenerationSettings> generator, Random rand, BlockPos pos, NoFeatureConfig config) {
		if(worldIn.getBiome(pos) == Biomes.THE_VOID)
			return false;
    	for(int y = 0; y < 65; ++y) {
    		BlockPos test = new BlockPos(pos.getX(), y, pos.getZ());
    		boolean water = worldIn.getFluidState(test).getFluidState().isTagged(FluidTags.WATER);
    		if(worldIn.getBlockState(test).isAir(worldIn, test) || water) {
    			//Ensures block below pos is a solid block
    			if(!worldIn.canBlockSeeSky(test.down())) {
    				BlockState state = TBlocks.broken_exterior.getDefaultState();
    				if(water) {
    					state = state.with(BlockStateProperties.WATERLOGGED, true);
    				}
    				worldIn.setBlockState(test.up(), state, 2); //Offset exterior up by one block, because exterior is rendered below its block
    				WorldGen.BROKEN_EXTERIORS.add(test);
            		return true;
    			}
    		}
    	}
        return false;
	}
}
