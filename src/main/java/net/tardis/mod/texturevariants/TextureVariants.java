package net.tardis.mod.texturevariants;

import net.tardis.mod.misc.TexVariant;

public class TextureVariants {

	public static final TexVariant[] TRUNK = {
			new TexVariant("trunk_base", "tardis.common.normal"),
			new TexVariant("trunk_dark", "exterior.trunk.dark")
	};
	
	public static final TexVariant[] FORTUNE = {
			new TexVariant("fortune", "tardis.common.normal"),
			new TexVariant("fortune_blue", "exterior.fortune.blue"),
			new TexVariant("fortune_red", "exterior.fortune.red")
	};
	
	public static final TexVariant[] STEAM = {
			new TexVariant("steampunk", "tardis.common.normal"),
			new TexVariant("steam_blue", "exterior.steam.blue"),
			new TexVariant("steam_rust", "exterior.steam.rust")
	};
}
