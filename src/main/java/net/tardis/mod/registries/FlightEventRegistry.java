package net.tardis.mod.registries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.util.Direction.Axis;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.tardis.mod.Tardis;
import net.tardis.mod.controls.ControlRegistry;
import net.tardis.mod.flight.BulkheadFlightEvent;
import net.tardis.mod.flight.CrashEvent;
import net.tardis.mod.flight.DimensionFlightEvent;
import net.tardis.mod.flight.DirectionalFlightEvent;
import net.tardis.mod.flight.FlightEvent;
import net.tardis.mod.flight.FlightEventFactory;
import net.tardis.mod.flight.RefuelerFlightEvent;
import net.tardis.mod.flight.ResidualArtronEvent;
import net.tardis.mod.flight.ScrapFlightEvent;
import net.tardis.mod.flight.TardisCollideInstagate;
import net.tardis.mod.flight.TardisCollideRecieve;
import net.tardis.mod.flight.TimeWindFlightEvent;
import net.tardis.mod.flight.VerticalFlightEvent;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class FlightEventRegistry {
	
	
	public static List<FlightEventFactory> FLIGHT_EVENTS;
	
	public static FlightEventFactory SCRAP;
	public static FlightEventFactory X;
	public static FlightEventFactory Y;
	public static FlightEventFactory Z;
	public static FlightEventFactory TIMEWIND;
	public static FlightEventFactory BULKHEAD;
	public static FlightEventFactory REFUELER;
	public static FlightEventFactory VERTICAL;
	public static FlightEventFactory DIMENSION;
	
	//Non-normal
	public static FlightEventFactory COLLIDE_INSTAGATE;
	public static FlightEventFactory COLLIDE_RECIEVE;
	public static FlightEventFactory CRASH_EVENT;
	
	//Positive
	public static FlightEventFactory RESIDUAL_ARTRON;
	
	@SubscribeEvent
	public static void register(RegistryEvent.Register<FlightEventFactory> event) {
		ArrayList<FlightEventFactory> events = new ArrayList<FlightEventFactory>();
		
		event.getRegistry().registerAll(
			SCRAP = register(ScrapFlightEvent::new, () -> Lists.newArrayList(ControlRegistry.RANDOM.getRegistryName(), ControlRegistry.FACING.getRegistryName()), "scrap", events),
			X = register((keys) -> new DirectionalFlightEvent(Axis.X, keys), () -> Lists.newArrayList(ControlRegistry.X.getRegistryName()), "x", events),
			Y = register((keys) -> new DirectionalFlightEvent(Axis.Y, keys), () -> Lists.newArrayList(ControlRegistry.Y.getRegistryName()), "y", events),
			Z = register((keys) -> new DirectionalFlightEvent(Axis.Z, keys), () -> Lists.newArrayList(ControlRegistry.Z.getRegistryName()), "z", events),
			TIMEWIND = register(TimeWindFlightEvent::new, () -> Lists.newArrayList(ControlRegistry.THROTTLE.getRegistryName()), "time_wind", events),
			BULKHEAD = register(BulkheadFlightEvent::new, () -> Lists.newArrayList(ControlRegistry.DOOR.getRegistryName()), "bulkhead", events),
			REFUELER = register(RefuelerFlightEvent::new, () -> Lists.newArrayList(ControlRegistry.REFUELER.getRegistryName()), "refueler", events),
			DIMENSION = register(DimensionFlightEvent::new, () -> Lists.newArrayList(ControlRegistry.DIMENSION.getRegistryName()), "dimension", events),
			VERTICAL = register(VerticalFlightEvent::new, () -> Lists.newArrayList(ControlRegistry.LAND_TYPE.getRegistryName()), "vertical", events),
			
			COLLIDE_INSTAGATE = register(TardisCollideInstagate::new, TardisCollideInstagate.CONTROLS, "collide_instigate", events, false),
			COLLIDE_RECIEVE = register(TardisCollideRecieve::new, TardisCollideRecieve.CONTROLS, "collide_recieve", events, false),
			CRASH_EVENT = register(CrashEvent::new, CrashEvent.CONTROLS, "crash", events, false),
			
			RESIDUAL_ARTRON = register(ResidualArtronEvent::new, () -> Lists.newArrayList(ControlRegistry.REFUELER.getRegistryName(), ControlRegistry.FACING.getRegistryName()), "residual_artron", events)
			
		);
		
		FLIGHT_EVENTS = Collections.unmodifiableList(events);
	}
	
	public static FlightEventFactory register(Function<ArrayList<ResourceLocation>, FlightEvent> event, Supplier<ArrayList<ResourceLocation>> sequence, String name, List<FlightEventFactory> list){
		FlightEventFactory fact = new FlightEventFactory(event, sequence);
		fact.setRegistryName(new ResourceLocation(Tardis.MODID, name));
		list.add(fact);
		return fact;
	}
	
	public static FlightEventFactory register(Function<ArrayList<ResourceLocation>, FlightEvent> event, Supplier<ArrayList<ResourceLocation>> sequence, String name, List<FlightEventFactory> list, boolean normal){
		FlightEventFactory fact = register(event, sequence, name, list);
		fact.setNormal(false);
		return fact;
	}
	
	public static FlightEventFactory getRandomEvent(Random r) {
		FlightEventFactory event = FLIGHT_EVENTS.get(r.nextInt(FLIGHT_EVENTS.size()));
		while(!event.isNormal()) {
			event = FLIGHT_EVENTS.get(r.nextInt(FLIGHT_EVENTS.size()));
		}
		return event;
	}
}
