package net.tardis.mod.tileentities.console.misc;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.tardis.mod.misc.SpaceTimeCoord;

public class DistressSignal {

	public static final int STRING_SIZE = 16;
	SpaceTimeCoord coord;
	String message;
	
	public DistressSignal(String message, SpaceTimeCoord coord) {
		this.coord = coord;
		this.message = message;
	}
	
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.put("coord", this.coord.serialize());
		tag.putString("message", this.message);
		return tag;
	}
	
	public SpaceTimeCoord getSpaceTimeCoord() {
		return this.coord;
	}
	
	public String getMessage() {
		return this.message;
	}
	
	public static DistressSignal deserializeNBT(CompoundNBT tag) {
		return new DistressSignal(tag.getString("message"), SpaceTimeCoord.deserialize(tag.getCompound("coord")));
	}
	
	public void encode(PacketBuffer buf) {
		buf.writeCompoundTag(this.coord.serialize());
		buf.writeInt(this.message.length());
		buf.writeString(message, message.length());
	}
	
	public static DistressSignal decode(PacketBuffer buf) {
		SpaceTimeCoord coord = SpaceTimeCoord.deserialize(buf.readCompoundTag());
		int stringSize = buf.readInt();
		String message = buf.readString(stringSize);
		return new DistressSignal(message, coord);
	}
}
