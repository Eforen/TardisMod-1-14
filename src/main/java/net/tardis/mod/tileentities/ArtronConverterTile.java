package net.tardis.mod.tileentities;

import net.minecraft.block.Blocks;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.tardis.mod.blocks.ArtronConverterBlock;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.console.misc.ArtronUse;
import net.tardis.mod.tileentities.console.misc.ArtronUse.ArtronType;

public class ArtronConverterTile extends TileEntity implements ITickableTileEntity{

	private static int TRANSFER = 10;
	//TODO: breaks after 256 au generated
	private EnergyStorage energy = new EnergyStorage(256);
	private float createdArtron = 0;
	
	private ConsoleTile tile;
	
	public ArtronConverterTile() {
		super(TTiles.ARTRON_CONVERTER);
	}

	@Override
	public void tick() {
		this.getCapability(CapabilityEnergy.ENERGY).ifPresent(cap -> {
			
			if(tile == null)
				return;
			
			if(this.getBlockState().get(ArtronConverterBlock.EMIT)) {
				if(tile.getArtron() < tile.getMaxArtron()) {
					
					float rate = 10000; //TConfig.CONFIG.artron_convert;
					
					int amt = cap.extractEnergy((int)rate, false);
					if(amt > 0) {
						float artron = amt / rate;
						tile.setArtron(tile.getArtron() + artron);
						this.createdArtron += artron;
						tile.updateClient();
						
						if(this.createdArtron > 256 && !world.isRemote) {
							world.playSound(null, this.getPos(), SoundEvents.BLOCK_ANVIL_DESTROY, SoundCategory.BLOCKS, 1F, 1F);
							world.setBlockState(getPos(), Blocks.AIR.getDefaultState());
						}
					}
				}
			}
			else {
				//Convert Artron to FE
				if(tile != null && tile.getArtron() > 1) {
					int amt = cap.receiveEnergy(100, false);
					ArtronUse use = tile.getOrCreateArtronUse(ArtronType.CONVERTER);
					use.setArtronUse(amt / 100.0F);
					use.setTimeToDrain(1);
				}
				
				//Push FE to valid tiles
				for(Direction dir : Direction.values()) {
					TileEntity te = world.getTileEntity(getPos().offset(dir));
					if(te != null)
						te.getCapability(CapabilityEnergy.ENERGY, dir.getOpposite()).ifPresent(power -> {
							int amt = cap.extractEnergy(TRANSFER, false);
							power.receiveEnergy(amt, false);
						});
				}
			}
			
		});
		
		if(tile == null || tile.isRemoved())
			TardisHelper.getConsoleInWorld(world).ifPresent(console -> this.tile = console);
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		return cap == CapabilityEnergy.ENERGY ? LazyOptional.of(() -> (T)this.energy) : super.getCapability(cap, side);
	}

}
