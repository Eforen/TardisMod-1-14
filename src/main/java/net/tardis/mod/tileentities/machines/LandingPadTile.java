package net.tardis.mod.tileentities.machines;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.tardis.mod.tileentities.TTiles;

public class LandingPadTile extends TileEntity{

	
	public LandingPadTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public LandingPadTile() {
		super(TTiles.LANDING_PAD);
	}

	public boolean getOccupied() {
		return !world.getBlockState(this.getPos().up(2)).isAir(world, pos.up(2));
	}
	
}
