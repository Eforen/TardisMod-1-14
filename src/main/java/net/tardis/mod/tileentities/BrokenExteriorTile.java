package net.tardis.mod.tileentities;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.LeadItem;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.Tardis;
import net.tardis.mod.experimental.advancement.TTriggers;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.BrokenExteriorType;
import net.tardis.mod.misc.BrokenExteriors;
import net.tardis.mod.misc.TardisLikes;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.BrokenTardisSpawn;
import net.tardis.mod.sounds.TSounds;

public class BrokenExteriorTile extends TileEntity implements ITickableTileEntity{
	
	public static final AxisAlignedBB RENDER_BOX = new AxisAlignedBB(-2, -2, -2, 2, 5, 2);
	private DimensionType consoleDim = null;
	private BrokenExteriors brokenType = null;
	
	private boolean hasPlayed = false;
	
	public BrokenExteriorTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public BrokenExteriorTile() {
		this(TTiles.BROKEN_TARDIS);
	}

	public void setConsoleDimension(DimensionType type) {
		this.consoleDim = type;
		this.markDirty();
	}
	
	public DimensionType getConsoleDimension() {
		return this.consoleDim;
	}
	
	public void increaseLoyalty(int amt) {
		if(!world.isRemote && consoleDim != null) {
			ServerWorld ws = world.getServer().getWorld(consoleDim);
			if(ws != null) {
				TileEntity te = ws.getTileEntity(TardisHelper.TARDIS_POS);
				if(te instanceof ConsoleTile) {
					ConsoleTile console = (ConsoleTile)te;
					console.getEmotionHandler().setLoyalty(console.getEmotionHandler().getLoyalty() + amt);
					console.getEmotionHandler().setMood(console.getEmotionHandler().getMood() + ((int) amt / 4));
				}
			}
		}
	}
	
	public int getLoyalty() {
		if(!world.isRemote && this.consoleDim != null) {
			ServerWorld ws = world.getServer().getWorld(consoleDim);
			if(ws != null) {
				TileEntity te = ws.getTileEntity(TardisHelper.TARDIS_POS);
				if(te instanceof ConsoleTile) {
					return ((ConsoleTile)te).getEmotionHandler().getLoyalty();
				}
			}
		}
		return -1;
	}

	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		if(compound.contains("console"))
			this.consoleDim = DimensionType.byName(new ResourceLocation(compound.getString("console")));
		if(compound.contains("broken_type"))
			this.brokenType = BrokenExteriors.values()[compound.getInt("broken_type")];
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		if(this.consoleDim != null)
			compound.putString("console", this.consoleDim.getRegistryName().toString());
		if(this.brokenType != null)
			compound.putInt("broken_type", this.brokenType.ordinal());
		return super.write(compound);
	}
	
	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return RENDER_BOX.offset(this.getPos());
	}

	@Override
	public void tick() {
		if(world.isRemote && !this.hasPlayed) {
			PlayerEntity player = Tardis.proxy.getClientPlayer();
			//TODO: Finish music playing
		}
	}

	public BrokenExteriorType getBrokenType() {
		return this.brokenType.getType();
	}

	public void onPlayerRightClick(PlayerEntity player, ItemStack stack) {
		
		//Stop if client
		if(world.isRemote)
			return;
		
		//If console dimension is null (unclaimed), or if this activating player is not the owner
		if(this.consoleDim == null) {
			this.getBrokenType().setup((ServerWorld)world, player, this);
			return;
		}
		
		//If this activating player is the owner, search likes
		for(TardisLikes like : TardisLikes.LIKES) {
			if(stack.getItem() == like.getItem()) {
				this.increaseLoyalty(like.getLoyaltyMod());
				if(!player.isCreative()) //Don't decrease stack size in creative mode
					stack.shrink(1);
				Network.sendToAllAround(new BrokenTardisSpawn(this.getPos()), this.world.getDimension().getType(), this.getPos(), 24);
			}
		}
		
		if(stack.getItem() == Items.LEAD) { //Readd lead functionality - if you use a lead the Tardis starts off with low loyalty
            if(LeadItem.attachToFence(player, player.world, pos)) {
                this.increaseLoyalty(-100); //Decrease loyalty for players that forcibly open Tardis
                tameTardis(player);
            }
		}
		
		//If this loyalty is greater than or equal to our specified number, replace with a real exterior.
		if(this.getLoyalty() >= 0) {
		    tameTardis(player);
		}
		
	}
	
	public void tameTardis(PlayerEntity player) {
	    Direction dir = Direction.NORTH;
        if(this.getBlockState() != null && this.getBlockState().has(BlockStateProperties.HORIZONTAL_FACING))
            dir = this.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING);
        this.getBrokenType().swapWithReal((ServerWorld)world, this.getPos(), this.consoleDim, dir);
        TTriggers.OBTAINED.trigger((ServerPlayerEntity) player); //Trigger Advancement
        this.world.playSound(null, pos, TSounds.DOOR_UNLOCK, SoundCategory.BLOCKS, 0.75F, 1F); //Play an audio cue
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		super.onDataPacket(net, pkt);
		this.deserializeNBT(pkt.getNbtCompound());
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return Network.createTEUpdatePacket(this);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.serializeNBT();
	}

	@Override
	public void onLoad() {
		super.onLoad();
		if(this.brokenType == null) {
			BlockState state = this.getBlockState();
			if(state.has(BlockStateProperties.WATERLOGGED) && state.get(BlockStateProperties.WATERLOGGED))
				this.brokenType = BrokenExteriors.WATER;
			else this.brokenType = BrokenExteriors.STEAM;
		}
	}


}
