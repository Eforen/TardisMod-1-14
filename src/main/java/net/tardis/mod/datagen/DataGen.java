package net.tardis.mod.datagen;

import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;
import net.tardis.mod.Tardis;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class DataGen {
	
	
	@SubscribeEvent
	public static void onDataGen(GatherDataEvent event) {
//	    event.getGenerator().addProvider(new QuantiscopeRecipeGen(event.getGenerator()));
		event.getGenerator().addProvider(new TardisBlockstateGen(event.getGenerator()));
		event.getGenerator().addProvider(new TardisBlockItemModelGen(event.getGenerator()));
	}

}
