package net.tardis.mod.client.guis.monitors;

import net.minecraft.client.Minecraft;
import net.tardis.mod.client.guis.widgets.TextButton;

public class MonitorTitleScreen extends MonitorScreen{

	BaseMonitorScreen screen;
	
	public MonitorTitleScreen(BaseMonitorScreen screen) {
		super(screen, "title");
		this.screen = screen;
	}
	
	@Override
	protected void init() {
		super.init();
		this.addButton(new TextButton(this.parent.getMinX(), this.parent.getMinY(), "> Continue", but -> {
			Minecraft.getInstance().displayGuiScreen(screen);
		}));
	}

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		super.render(p_render_1_, p_render_2_, p_render_3_);
		String title = "Type 40 TT Capsule";
		this.font.drawStringWithShadow(title, this.width / 2 - this.font.getStringWidth(title) / 2, this.parent.getMaxY(), 0xFFFFFF);
		
		String type = "Mark III";
		this.font.drawStringWithShadow(type, this.width / 2 - this.font.getStringWidth(type) / 2, this.parent.getMaxY() + (font.FONT_HEIGHT + 2), 0xFFFFFF);
	}
	
	

}
