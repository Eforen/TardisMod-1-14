package net.tardis.mod.client.guis.containers;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ClickType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.tardis.mod.client.guis.minigame.WireGameScreen;
import net.tardis.mod.containers.EngineContainer;
import net.tardis.mod.containers.slot.EngineSlot;

public class EngineContainerScreen extends ContainerScreen<EngineContainer>{

	public EngineContainerScreen(EngineContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
		super(screenContainer, inv, titleIn);
		this.xSize = 176;
		this.ySize = 97 + 35;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		this.renderBackground();
		Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation("textures/gui/container/generic_54.png"));
		
		int y = this.height / 2 - 35;
		this.blit(width / 2 - this.xSize / 2, y, 0, 125, this.xSize, 97);
		this.blit(width / 2 - this.xSize / 2, y - 35, 0, 0, this.xSize, 35);
	}

	@Override
	public void render(int mouseX, int mouseY, float p_render_3_) {
		super.render(mouseX, mouseY, p_render_3_);
		this.renderHoveredToolTip(mouseX, mouseY);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
		super.drawGuiContainerForegroundLayer(mouseX, mouseY);
		this.font.drawString(this.title.getFormattedText(), 8.0F, 3.0F, 4210752);
        this.font.drawString(this.playerInventory.getDisplayName().getFormattedText(), 8.0F, (float)(this.ySize - 97), 4210752);
		
	}

	@Override
	protected void handleMouseClick(Slot slotIn, int slotId, int mouseButton, ClickType type) {
		if(type == ClickType.PICKUP) {
			ItemStack held = this.playerInventory.getItemStack();
			if(!held.isEmpty() && slotIn instanceof EngineSlot) {
				Minecraft.getInstance().enqueue(() -> Minecraft.getInstance().displayGuiScreen(new WireGameScreen(slotId, this.container.getPanelDirection())));
			}
		}
		super.handleMouseClick(slotIn, slotId, mouseButton, type);
	}

}
