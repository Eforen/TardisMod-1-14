package net.tardis.mod.client.guis.monitors;

import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.ConsoleUpdateMessage.DataTypes;
import net.tardis.mod.network.packets.console.SoundSchemeData;
import net.tardis.mod.proxy.ClientProxy;
import net.tardis.mod.registries.TardisForgeRegistries;
import net.tardis.mod.sounds.SoundSchemeBase;

public class SoundSchemeMonitorScreen extends MonitorScreen {

	public SoundSchemeMonitorScreen(IMonitorGui monitor, String type) {
		super(monitor, type);
	}

	@Override
	protected void init() {
		super.init();
		
		for(SoundSchemeBase sound : TardisForgeRegistries.SOUND_SCHEME.getValues()) {
			this.addSubmenu(new TranslationTextComponent("gui." + sound.getRegistryName().getNamespace() + ".sound_scheme." + sound.getRegistryName().getPath()),
					but -> {
						Network.sendToServer(new ConsoleUpdateMessage(DataTypes.SOUND_SCHEME, new SoundSchemeData(sound.getRegistryName())));
						ClientProxy.openGui(null);
					});
		}
		
	}

	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		super.render(mouseX, mouseY, partialTicks);
		this.drawCenteredString(this.minecraft.fontRenderer, new TranslationTextComponent(Constants.Strings.GUI + "sound_scheme.title").getFormattedText(), this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2), this.parent.getMaxY() + 10, 0xFFFFFF);
		this.drawCenteredString(this.minecraft.fontRenderer, new TranslationTextComponent(Constants.Strings.GUI + "sound_scheme.desc").getFormattedText(), this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2), this.parent.getMaxY() + 30, 0xFFFFFF);
	}

}
