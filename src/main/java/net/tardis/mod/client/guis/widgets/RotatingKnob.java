package net.tardis.mod.client.guis.widgets;

import net.minecraft.client.gui.widget.Widget;

public class RotatingKnob extends Widget {

	private float rotation;
	
	public RotatingKnob(int x, int y, String msg) {
		super(x, y, 16, 16, msg);
	}

	@Override
	public void renderButton(int p_renderButton_1_, int p_renderButton_2_, float p_renderButton_3_) {
		//BufferBuilder bb = Tessellator.getInstance().getBuffer();
		//bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
	}

	@Override
	public boolean mouseDragged(double p_mouseDragged_1_, double p_mouseDragged_3_, int p_mouseDragged_5_, double p_mouseDragged_6_, double p_mouseDragged_8_) {
		// TODO Auto-generated method stub
		return super.mouseDragged(p_mouseDragged_1_, p_mouseDragged_3_, p_mouseDragged_5_, p_mouseDragged_6_, p_mouseDragged_8_);
	}
	
	public void setRotation(float rot) {
		this.rotation = rot;
	}
	
	public float getRotation() {
		return this.rotation;
	}

}
