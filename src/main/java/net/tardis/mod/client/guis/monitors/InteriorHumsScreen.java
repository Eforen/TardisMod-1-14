package net.tardis.mod.client.guis.monitors;

import java.util.Map;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ChangeHumMessage;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.sounds.InteriorHum;

public class InteriorHumsScreen extends MonitorScreen {

    public InteriorHumsScreen(IMonitorGui screen, String menu) {
        super(screen, menu);
    }

    @Override
    protected void init() {
        super.init();

        int index = 0;
        for(Map.Entry<ResourceLocation, InteriorHum> entry : TardisRegistries.HUM_REGISTRY.getRegistry().entrySet()){
            ResourceLocation humName = entry.getKey();
            InteriorHum hum = entry.getValue();

            this.addButton(new TextButton(this.parent.getMinX(), this.parent.getMinY() - this.minecraft.fontRenderer.FONT_HEIGHT * index + 2, "> " + hum.getTranslatedName(),
            		button -> Network.sendToServer(new ChangeHumMessage(humName))));
            ++index;
        }
    }
}
