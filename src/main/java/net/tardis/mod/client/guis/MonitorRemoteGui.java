package net.tardis.mod.client.guis;

import java.text.DecimalFormat;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.ImageButton;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.widgets.ImageAndTextButton;
import net.tardis.mod.contexts.gui.EntityContext;
import net.tardis.mod.contexts.gui.GuiContextBlock;
import net.tardis.mod.controls.IControl;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.misc.IMonitor;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.MonitorRemoteMessage;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorMode;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorView;
import net.tardis.mod.tileentities.monitors.RotateMonitorTile;

public class MonitorRemoteGui extends Screen{

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/monitor_remote.png");
	private TranslationTextComponent updateExtendTooltip = new TranslationTextComponent("tooltip.gui.monitor_remote.update_arm_length");
	private TranslationTextComponent monitorModeTooltip = new TranslationTextComponent("tooltip.gui.monitor_remote.monitor_mode");
	private TextFieldWidget extendInput;
	private Button updateExtend;
	private Button monitorMode;
	private IMonitor tile;
	
	private float monitorviewExtension = 0.0F;
	private MonitorMode mode = MonitorMode.INFO;
	private MonitorView view = MonitorView.PANORAMIC;

	public MonitorRemoteGui(GuiContext context) {
		super(new StringTextComponent(""));
		
		if(context instanceof GuiContextBlock) {
			TileEntity te = Minecraft.getInstance().world.getTileEntity(((GuiContextBlock)context).pos);
			if(te instanceof IMonitor)
				this.tile = (IMonitor)te;
			else Minecraft.getInstance().displayGuiScreen(null);
		}
		else if(context instanceof EntityContext) {
			EntityContext ent = (EntityContext)context;
			if(ent.entity instanceof ControlEntity && ((ControlEntity)ent.entity).getControl() instanceof IMonitor) {
				tile = ((IMonitor)((ControlEntity)ent.entity).getControl());
			}
			else Minecraft.getInstance().displayGuiScreen(null);
		}
		else Minecraft.getInstance().displayGuiScreen(null);
		
	}

	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		this.renderBackground();
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		int width = 233, height = 163;
		this.blit(this.width / 2 - width / 2, this.height / 2 - height / 2, 0, 0, width, height);
		super.render(mouseX, mouseY, partialTicks);
		this.drawCenteredString(this.font, "Scanner View Mode:", this.width / 2 + 3, this.height / 2 - 70, 0xFFFFFF);
		this.drawCenteredString(this.font, "Rotatable Monitor Arm Extension Length", this.width / 2 + 3, this.height / 2 - 8, 0xFFFFFF);
		if (this.extendInput.isFocused() || !this.extendInput.getText().isEmpty()) {
			this.extendInput.setSuggestion("");
		}
		else if (this.extendInput.getText().isEmpty() && !this.extendInput.isFocused()){
			this.extendInput.setSuggestion("Set a number");
		}
		if (this.updateExtend.isHovered()) {
			this.renderTooltip(updateExtendTooltip.getFormattedText(), mouseX, mouseY);
		}
		if (this.monitorMode.isHovered()) {
            this.renderTooltip(monitorModeTooltip.getFormattedText(), mouseX, mouseY);
        }
	}

	@Override
	protected void init() {
		super.init();
		this.buttons.clear();
		String updateButton = "Update Arm Length";
		this.addModeButton(42, 44, 4, "NW", 0xFFFFF, MonitorView.NORTH_WEST); //1
		this.addModeButton(15, 60, 4, "SE", 0xFFFFF, MonitorView.SOUTH_EAST); //2
		this.addModeButton(-12, 44, 4, "Orbit", 0xFFFFF, MonitorView.PANORAMIC); //3
		this.addExtensionArrowButton(62, -3, 39, -1F, 0.0F, 1.0F); //Left arrow
		this.addExtensionArrowButton(-45, -3, 64, 1F, 0.0F, 1.0F); //Right arrow
		this.addButton(this.extendInput = new TextFieldWidget(this.font, width / 2 - 32, height / 2 + 7, 70, font.FONT_HEIGHT + 14, ""));
		this.addButton(updateExtend = new Button(this.width / 2 - 43, height / 2 + 32, 100, this.font.FONT_HEIGHT + 11, updateButton, but -> {
			this.monitorviewExtension = MathHelper.clamp(this.getNumber(this.extendInput.getText()) / 10.0F, 0, 1.0F);
			this.sendPacket();
			this.onClose();
		}));
		
		this.mode = tile.getMode();
		this.view = tile.getView();
		if(tile instanceof RotateMonitorTile) {
			float amt = ((RotateMonitorTile)tile).extendAmount;
			this.extendInput.setText(FORMAT.format(amt * 10.0) + "");
			this.monitorviewExtension = amt;
		}
		
		this.addButton(monitorMode = new Button(this.width / 2 - 47, height / 2 + 59, 110, this.font.FONT_HEIGHT + 11, this.getMonitorButtonText(), but -> {
			mode = (mode == MonitorMode.INFO ? MonitorMode.SCANNER : MonitorMode.INFO);
			but.setMessage(this.getMonitorButtonText());
			this.sendPacket();
			this.onClose();
		}));
	}
	
	public String getMonitorButtonText() {
		return "View Mode: " + (this.mode == MonitorMode.INFO ? "Info Mode" : "Scanner");
	}
	
	public float getNumber(String s) {
		try {
			float f = Float.parseFloat(s);
			return f;
		}
		catch(NumberFormatException e) {
			return 0;
		}
	}

	@Override
	public void onClose() {
		super.onClose();		
	}
	/**
	 * Draw hexagonal view type button
	 * @param x
	 * @param y
	 * @param u
	 * @param text
	 * @param textColour
	 * @param viewType
	 */
	public void addModeButton(int x, int y, int u, String text, int textColour, MonitorView viewType) {
		this.addButton(new ImageAndTextButton(width / 2 - x, height / 2 - y, 33, 31, u, 166, 31, TEXTURE, text, textColour, but -> {
			this.view = viewType;
			this.sendPacket();
			this.onClose();
		}));
	}
	
	public static DecimalFormat FORMAT = new DecimalFormat("####");
	
	/**
	 * Draw arrow used for extending view
	 * @param x
	 * @param y
	 * @param u
	 * @param mod
	 * @param min
	 * @param max
	 */
	public void addExtensionArrowButton(int x, int y, int u, float mod, float min, float max) {
		this.addButton(new ImageButton(width / 2 - x, height / 2 - y, 23, 29, u, 168, 30, TEXTURE, but -> {
			
			float num = this.getNumber(this.extendInput.getText()) + mod;
			num = MathHelper.clamp(num, 0, 10);
			
			this.extendInput.setText(FORMAT.format(num));
			this.extendInput.setCursorPositionZero();
		}));
	}
	
	public void sendPacket() {
		
		if(this.tile instanceof TileEntity)
			Network.sendToServer(new MonitorRemoteMessage(((TileEntity)tile).getPos(), this.mode, this.view, this.monitorviewExtension));
		else if(this.tile instanceof IControl)
			Network.sendToServer(new MonitorRemoteMessage(((IControl)tile).getEntity().getEntityId(), this.mode, this.view, this.monitorviewExtension));
	}

}
