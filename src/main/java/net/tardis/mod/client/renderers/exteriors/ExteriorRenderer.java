package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.GlStateManager.DestFactor;
import com.mojang.blaze3d.platform.GlStateManager.SourceFactor;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public abstract class ExteriorRenderer<T extends ExteriorTile> extends TileEntityRenderer<T>{

	@Override
	public void render(T tile, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y - 0.5, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);

		if(tile.getBlockState() != null && tile.getBlockState().getBlock() instanceof ExteriorBlock) {
			Direction face = tile.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING);
			GlStateManager.rotated(face.getHorizontalAngle() - 180, 0, 1, 0);
		}
			
		if (this.isInverted(tile)) {
			GlStateManager.rotated(180, 0, 0, 1);
			GlStateManager.translated(0, 1.75, 0);
		}
		if(tile.getMatterState() != EnumMatterState.SOLID) {
			GlStateManager.enableAlphaTest();
			GlStateManager.enableBlend();
            GlStateManager.blendFunc(SourceFactor.SRC_ALPHA, DestFactor.ONE_MINUS_SRC_ALPHA);
            GlStateManager.color4f(1, 1, 1, tile.alpha);
			this.renderExterior(tile);
			GlStateManager.disableAlphaTest();
			GlStateManager.disableBlend();
		}
		else{
			if(tile.getWorld() != null && tile.getWorld().getBlockState(tile.getPos().down(2)).isAir() && this.floatInAir()) {
				double offY = Math.cos(Minecraft.getInstance().world.getGameTime() * 0.05) * 0.06;
				double offX = Math.cos(Minecraft.getInstance().world.getGameTime() * 0.05) * 0.07;
				double offZ = Math.cos(Minecraft.getInstance().world.getGameTime() * 0.05) * 0.07;
				GlStateManager.translated(offX, offY, offZ);
			}
			this.renderExterior(tile);
		}
		GlStateManager.popMatrix();
		
		//Debug
		
		if(Minecraft.getInstance().player != null && Helper.InEitherHand(Minecraft.getInstance().player, stack -> stack.getItem() == TItems.DEBUG)) {
			GlStateManager.translated(x, y, z);
			GlStateManager.enableBlend();
			net.tardis.mod.helper.RenderHelper.renderAABB(tile.getDoorAABB(), 1.0F, 0.0F, 0.0F, 0.75F);
			GlStateManager.disableBlend();
		}
		
	}
	
	public boolean floatInAir() {
		return true;
	}
	
	public abstract void renderExterior(T tile);
	
	public boolean isInverted(T tile) {
		if (tile.getCustomName().contains("Dinnerbone") || tile.getCustomName().contains("Grumm") || tile.getCustomName().contains("boti")) {
			return true;
		}
		return false;
	}

}
