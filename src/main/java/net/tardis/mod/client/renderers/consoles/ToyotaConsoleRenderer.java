package net.tardis.mod.client.renderers.consoles;

import java.text.DecimalFormat;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.models.consoles.ToyotaConsoleModel;
import net.tardis.mod.client.renderers.monitor.RenderScanner;
import net.tardis.mod.controls.MonitorControl;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.RenderHelper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorMode;

public class ToyotaConsoleRenderer extends TileEntityRenderer<ToyotaConsoleTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/consoles/toyota.png");
	public static final ToyotaConsoleModel MODEL = new ToyotaConsoleModel();
	
	public static final DecimalFormat FORMAT = new DecimalFormat("###");
	public static final WorldText TEXT = new WorldText(0.5F, 0.3F, 0.004F, 0x000000);
	
	@Override
	public void render(ToyotaConsoleTile console, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 0.9, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.enableRescaleNormal();
		
		//Monitor text
        MonitorControl monitor = console.getControl(MonitorControl.class);
        if(monitor != null) {
            if (monitor.getMode() == MonitorMode.INFO) {
                GlStateManager.pushMatrix();
                GlStateManager.rotated(60, 0, 1, 0);
                GlStateManager.rotated(-70, 1, 0, 0);
                GlStateManager.translated(-0.23, 11.25 / 16.0, -9.84 / 16.0);
                TEXT.renderMonitor(Helper.getConsoleText(console));
                GlStateManager.popMatrix();
            }
        }
		
		
		GlStateManager.scaled(0.6, 0.6, 0.6);
		if(console.getVariant() != null)
			Minecraft.getInstance().getTextureManager().bindTexture(console.getVariant().getTexture());
		else Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(console);
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
		
		//Scanner
        Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
            if(console.getControl(MonitorControl.class).getMode() == MonitorMode.SCANNER) {
                GlStateManager.pushMatrix();
                GlStateManager.translated(x + 1.375, y + 1.125, z - 0.35);
                GlStateManager.rotated(-60, 0, 1, 0); //Rotate to be in line with monitor on y axis
                GlStateManager.rotated(70, 1, 0, 0); //Make it inline on x axis
                GlStateManager.color3f(1, 1, 1);
                RenderHelper.setupRenderLightning();
                RenderScanner.renderWorldToPlane(data, 0.06F, 0.01F, 0.54F, 0.375F, -0.01F, monitor.getView().getAngle(), partialTicks);
                RenderHelper.finishRenderLightning();
                GlStateManager.popMatrix();
            }
        });
	}
}
