package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.NeutronConsoleModel;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;

public class NeutronConsoleRenderer extends TileEntityRenderer<NeutronConsoleTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/consoles/neutron_thaumic.png");
	public static final NeutronConsoleModel MODEL = new NeutronConsoleModel();
	
	@Override
	public void render(NeutronConsoleTile console, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.enableRescaleNormal();
		GlStateManager.translated(x + 0.5, y + 0.45, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		double scale = 0.3;
		GlStateManager.scaled(scale, scale, scale); //Use a local variable to scale console easier
		
		if(console.getVariant() != null)
			this.bindTexture(console.getVariant().getTexture());
		else this.bindTexture(TEXTURE);
		
		MODEL.render(console);
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}

	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().textureManager.bindTexture(location);
	}

}
