package net.tardis.mod.client.renderers.sky;

public class Planet {

	public double minHU, maxHU, minHV, maxHV;
	
	public double minVU, maxVU, minVV, maxVV;
	
	public Planet() {}
	
	public void setHorizontalUVs(double minU, double minV, double maxU, double maxV) {
		this.minHU = minU;
		this.minHV = minV;
		this.maxHU = maxU;
		this.maxHV = maxV;
	}
	
	public void setVerticleUVs(double minU, double minV, double maxU, double maxV) {
		this.minVU = minU;
		this.minVV = minV;
		this.maxVU = maxU;
		this.maxVV = maxV;
	}
}
