package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.models.consoles.CoralConsoleModel;
import net.tardis.mod.client.renderers.monitor.RenderScanner;
import net.tardis.mod.controls.MonitorControl;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.helper.RenderHelper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorMode;

public class CoralConsoleRenderer extends TileEntityRenderer<CoralConsoleTile> {

    public static CoralConsoleModel model = new CoralConsoleModel();
    public static ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/consoles/coral.png");

    @Override
    public void render(CoralConsoleTile console, double x, double y, double z, float partialTicks, int destroyStage) {
        GlStateManager.pushMatrix();
        GlStateManager.translated(x + 0.5, y + 1.125, z + 0.5);
        GlStateManager.rotated(180, 0, 0, 1);
        
        //Monitor Text
        MonitorControl monitor = console.getControl(MonitorControl.class);
        if(monitor != null) {
            if (monitor.getMode() == MonitorMode.INFO) {
                GlStateManager.pushMatrix();
                GlStateManager.rotated(150, 0, 1, 0);
                GlStateManager.translated(-0.15, -0.32, -10.86 / 16.0);
                WorldText TEXT = new WorldText(0.31F, 0.26F, 0.003F, 0xFFFFFF);
                TEXT.renderMonitor(Helper.getConsoleText(console));
                GlStateManager.popMatrix();
            }
        }
        
        if(console.getVariant() != null) 
			Minecraft.getInstance().getTextureManager().bindTexture(console.getVariant().getTexture());
		else Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
        double scale = 0.75;
        GlStateManager.enableRescaleNormal();
        GlStateManager.scaled(scale, scale, scale);
        model.render(console, ModelHelper.RENDER_SCALE);
        GlStateManager.disableRescaleNormal();
        GlStateManager.popMatrix();
        
        //Scanner
        Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
			if(monitor.getMode() == MonitorMode.SCANNER) {
                GlStateManager.pushMatrix();
                GlStateManager.translated(x + 1.4, y + 1.125, z + 1.925);
                GlStateManager.rotated(180, 0, 1, 0); //flip image so it faces the correct direction
                GlStateManager.rotated(30, 0, 1, 0);
                GlStateManager.color3f(1, 1, 1);
                RenderHelper.setupRenderLightning();
                RenderScanner.renderWorldToPlane(data, -0.128F, 0F, 0.225F, 0.35F, 1F, monitor.getView().getAngle(), partialTicks);
                RenderHelper.finishRenderLightning();
                GlStateManager.popMatrix();
            }
        });
    }

    @Override
    protected void bindTexture(ResourceLocation location) {
        Minecraft.getInstance().getTextureManager().bindTexture(location);
    }

}
