package net.tardis.mod.client.models.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.controls.CommunicatorControl;
import net.tardis.mod.controls.DimensionControl;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.controls.RandomiserControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.ConsoleTile;

@SuppressWarnings("deprecation")
public class NemoConsoleModel extends Model {
	
	private final RendererModel glow_timerotor_slide_y;
	private final RendererModel glow_baseoffset_1;
	private final RendererModel glow_baseoffset_2;
	private final RendererModel glow_baseoffset_3;
	private final RendererModel console;
	private final RendererModel bottom_plate;
	private final RendererModel stations;
	private final RendererModel station_1;
	private final RendererModel edge_1;
	private final RendererModel cooling_blades_1;
	private final RendererModel plasma_coil_1;
	private final RendererModel belly_1;
	private final RendererModel bolts;
	private final RendererModel bolts_b;
	private final RendererModel bolts_c;
	private final RendererModel plane_1;
	private final RendererModel rib_1;
	private final RendererModel rib_tilt_1;
	private final RendererModel rib_deco_1;
	private final RendererModel lowerrib_tilt_2;
	private final RendererModel base_fin_1;
	private final RendererModel clawfoot_1;
	private final RendererModel leg_1;
	private final RendererModel ball_1;
	private final RendererModel station_2;
	private final RendererModel edge_2;
	private final RendererModel cooling_blades_2;
	private final RendererModel plasma_coil_2;
	private final RendererModel belly_2;
	private final RendererModel bolts2;
	private final RendererModel bolts_b2;
	private final RendererModel bolts_c2;
	private final RendererModel plane_2;
	private final RendererModel rib_2;
	private final RendererModel rib_tilt_2;
	private final RendererModel rib_deco_2;
	private final RendererModel lowerrib_tilt_3;
	private final RendererModel base_fin_2;
	private final RendererModel clawfoot_2;
	private final RendererModel leg_2;
	private final RendererModel ball_2;
	private final RendererModel station_3;
	private final RendererModel edge_3;
	private final RendererModel cooling_blades_3;
	private final RendererModel plasma_coil_3;
	private final RendererModel belly_3;
	private final RendererModel bolts3;
	private final RendererModel bolts_b3;
	private final RendererModel bolts_c3;
	private final RendererModel plane_3;
	private final RendererModel rib_3;
	private final RendererModel rib_tilt_3;
	private final RendererModel rib_deco_3;
	private final RendererModel lowerrib_tilt_4;
	private final RendererModel base_fin_3;
	private final RendererModel clawfoot_3;
	private final RendererModel leg_3;
	private final RendererModel ball_3;
	private final RendererModel station_4;
	private final RendererModel edge_4;
	private final RendererModel cooling_blades_4;
	private final RendererModel plasma_coil_4;
	private final RendererModel belly_4;
	private final RendererModel bolts4;
	private final RendererModel bolts_b4;
	private final RendererModel bolts_c4;
	private final RendererModel plane_4;
	private final RendererModel rib_4;
	private final RendererModel rib_tilt_4;
	private final RendererModel rib_deco_4;
	private final RendererModel lowerrib_tilt_5;
	private final RendererModel base_fin_4;
	private final RendererModel clawfoot_4;
	private final RendererModel leg_4;
	private final RendererModel ball_4;
	private final RendererModel station_5;
	private final RendererModel edge_5;
	private final RendererModel cooling_blades_5;
	private final RendererModel plasma_coil_5;
	private final RendererModel belly_5;
	private final RendererModel bolts5;
	private final RendererModel bolts_b5;
	private final RendererModel bolts_c5;
	private final RendererModel plane_5;
	private final RendererModel rib_5;
	private final RendererModel rib_tilt_5;
	private final RendererModel rib_deco_5;
	private final RendererModel lowerrib_tilt_6;
	private final RendererModel base_fin_5;
	private final RendererModel clawfoot_5;
	private final RendererModel leg_5;
	private final RendererModel ball_5;
	private final RendererModel station_6;
	private final RendererModel edge_6;
	private final RendererModel cooling_blades_6;
	private final RendererModel plasma_coil_6;
	private final RendererModel belly_6;
	private final RendererModel bolts6;
	private final RendererModel bolts_b6;
	private final RendererModel bolts_c6;
	private final RendererModel plane_6;
	private final RendererModel rib_6;
	private final RendererModel rib_tilt_6;
	private final RendererModel rib_deco_6;
	private final RendererModel lowerrib_tilt_7;
	private final RendererModel base_fin_6;
	private final RendererModel clawfoot_6;
	private final RendererModel leg_6;
	private final RendererModel ball_6;
	private final RendererModel controls;
	private final RendererModel side1;
	private final RendererModel station_offset_1;
	private final RendererModel xyz_increment_rotate_z;
	private final RendererModel spokes;
	private final RendererModel spokes2;
	private final RendererModel center;
	private final RendererModel station_tilt_1;
	private final RendererModel coorddial_x;
	private final RendererModel coorddial_y;
	private final RendererModel coorddial_z;
	private final RendererModel fancy_nameplate;
	private final RendererModel dummy_toggle_a1;
	private final RendererModel tilt;
	private final RendererModel tilt2;
	private final RendererModel dummy_toggle_a2;
	private final RendererModel tilt3;
	private final RendererModel tilt4;
	private final RendererModel dummy_toggle_a3;
	private final RendererModel tilt5;
	private final RendererModel tilt6;
	private final RendererModel dummy_toggle_a4;
	private final RendererModel tilt7;
	private final RendererModel tilt8;
	private final RendererModel dummy_toggle_a5;
	private final RendererModel tilt9;
	private final RendererModel tilt10;
	private final RendererModel dummy_toggle_a6;
	private final RendererModel tilt11;
	private final RendererModel tilt12;
	private final RendererModel dummy_toggle_a7;
	private final RendererModel tilt13;
	private final RendererModel tilt14;
	private final RendererModel dummy_toggle_a8;
	private final RendererModel tilt15;
	private final RendererModel tilt16;
	private final RendererModel dummy_button_a1;
	private final RendererModel dummy_button_a2;
	private final RendererModel dummy_button_a3;
	private final RendererModel side2;
	private final RendererModel station_tilt_2;
	private final RendererModel facing_dial;
	private final RendererModel landing_type;
	private final RendererModel slider_bar;
	private final RendererModel slider_plate;
	private final RendererModel up;
	private final RendererModel up2;
	private final RendererModel dummy_button_b1;
	private final RendererModel randomizer;
	private final RendererModel rotate_y;
	private final RendererModel pipes_2;
	private final RendererModel side3;
	private final RendererModel dummy_barometer;
	private final RendererModel station_tilt_3;
	private final RendererModel throttle;
	private final RendererModel throttle_lever_rotate_x;
	private final RendererModel throttle_wheel;
	private final RendererModel bone2;
	private final RendererModel spacer;
	private final RendererModel spacer_wheel1;
	private final RendererModel spacer_tilt1;
	private final RendererModel handbreak;
	private final RendererModel handbreak_lever2_rotate_x;
	private final RendererModel handbreak_wheel2;
	private final RendererModel handbreak_tilt2;
	private final RendererModel refueler;
	private final RendererModel dummy_dial_c1;
	private final RendererModel dummy_dial_c2;
	private final RendererModel dummy_button_C1;
	private final RendererModel side4;
	private final RendererModel dimention_select;
	private final RendererModel dialframe;
	private final RendererModel dialframe2;
	private final RendererModel needle_rotate_z;
	private final RendererModel station_tilt_4;
	private final RendererModel waypoint_selector;
	private final RendererModel dummy_button_d1;
	private final RendererModel dummy_button_d2;
	private final RendererModel sonic_port;
	private final RendererModel dummy_dial_d1;
	private final RendererModel dummy_dial_d2;
	private final RendererModel side5;
	private final RendererModel telepathic_circuits;
	private final RendererModel station_tilt_5;
	private final RendererModel pipes_5;
	private final RendererModel bone;
	private final RendererModel bone3;
	private final RendererModel dummy_plate;
	private final RendererModel dummy_bell_e1;
	private final RendererModel dummy_bell_e2;
	private final RendererModel dummy_bell_e3;
	private final RendererModel dummy_bell_e4;
	private final RendererModel fast_return;
	private final RendererModel stablizer;
	private final RendererModel side6;
	private final RendererModel station_tilt_6;
	private final RendererModel door;
	private final RendererModel door_plate;
	private final RendererModel door_knob_rotate_z;
	private final RendererModel dummy_button_f1;
	private final RendererModel dummy_button_f2;
	private final RendererModel dummy_button_f3;
	private final RendererModel dummy_button_f4;
	private final RendererModel dummy_button_f5;
	private final RendererModel dummy_button_f6;
	private final RendererModel coms;
	private final RendererModel bell_rotate_x;
	private final RendererModel time_rotor_slide_y;
	private final RendererModel rotor_plate;
	private final RendererModel rotor_post_1;
	private final RendererModel rotor_post_2;
	private final RendererModel rotor_post_3;
	public NemoConsoleModel() {
		textureWidth = 256;
		textureHeight = 256;

		glow_timerotor_slide_y = new RendererModel(this);
		glow_timerotor_slide_y.setRotationPoint(0.0F, 31.0F, 0.0F);
		glow_timerotor_slide_y.cubeList.add(new ModelBox(glow_timerotor_slide_y, 172, 10, -5.25F, -100.6F, -5.0F, 10, 10, 10, 0.0F, false));
		glow_timerotor_slide_y.cubeList.add(new ModelBox(glow_timerotor_slide_y, 172, 10, -5.25F, -110.6F, -5.0F, 10, 10, 10, 0.0F, false));

		glow_baseoffset_1 = new RendererModel(this);
		glow_baseoffset_1.setRotationPoint(0.0F, -8.0F, 0.5F);
		glow_baseoffset_1.cubeList.add(new ModelBox(glow_baseoffset_1, 171, 2, -10.0F, -14.0F, -17.5F, 20, 28, 1, 0.0F, false));
		glow_baseoffset_1.cubeList.add(new ModelBox(glow_baseoffset_1, 171, 2, -10.0F, -14.0F, 16.5F, 20, 28, 1, 0.0F, false));

		glow_baseoffset_2 = new RendererModel(this);
		glow_baseoffset_2.setRotationPoint(0.0F, -8.0F, 0.5F);
		setRotationAngle(glow_baseoffset_2, 0.0F, -1.0472F, 0.0F);
		glow_baseoffset_2.cubeList.add(new ModelBox(glow_baseoffset_2, 171, 2, -10.0F, -14.0F, -17.5F, 20, 28, 1, 0.0F, false));
		glow_baseoffset_2.cubeList.add(new ModelBox(glow_baseoffset_2, 171, 2, -10.0F, -14.0F, 16.5F, 20, 28, 1, 0.0F, false));

		glow_baseoffset_3 = new RendererModel(this);
		glow_baseoffset_3.setRotationPoint(0.0F, -8.0F, 0.5F);
		setRotationAngle(glow_baseoffset_3, 0.0F, -2.0944F, 0.0F);
		glow_baseoffset_3.cubeList.add(new ModelBox(glow_baseoffset_3, 171, 2, -10.0F, -14.0F, -17.5F, 20, 28, 1, 0.0F, false));
		glow_baseoffset_3.cubeList.add(new ModelBox(glow_baseoffset_3, 171, 2, -10.0F, -14.0F, 16.5F, 20, 28, 1, 0.0F, false));

		console = new RendererModel(this);
		console.setRotationPoint(0.0F, 24.0F, 0.0F);

		bottom_plate = new RendererModel(this);
		bottom_plate.setRotationPoint(0.0F, 0.0F, 0.0F);
		console.addChild(bottom_plate);
		bottom_plate.cubeList.add(new ModelBox(bottom_plate, 11, 95, -20.0F, -1.6F, -8.0F, 40, 2, 16, 0.0F, false));
		bottom_plate.cubeList.add(new ModelBox(bottom_plate, 21, 96, -16.0F, -1.6F, -16.0F, 32, 2, 8, 0.0F, false));
		bottom_plate.cubeList.add(new ModelBox(bottom_plate, 26, 102, -16.0F, -1.6F, 8.0F, 32, 2, 8, 0.0F, false));

		stations = new RendererModel(this);
		stations.setRotationPoint(0.0F, 0.0F, 0.0F);
		console.addChild(stations);

		station_1 = new RendererModel(this);
		station_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(station_1);

		edge_1 = new RendererModel(this);
		edge_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_1.addChild(edge_1);
		edge_1.cubeList.add(new ModelBox(edge_1, 13, 1, -34.0F, -60.0F, -61.0F, 67, 4, 6, 0.0F, false));
		edge_1.cubeList.add(new ModelBox(edge_1, 59, 96, -10.0F, -79.0F, -21.0F, 20, 16, 6, 0.0F, false));
		edge_1.cubeList.add(new ModelBox(edge_1, 40, 95, -10.0F, -71.0F, -15.0F, 20, 8, 6, 0.0F, false));
		edge_1.cubeList.add(new ModelBox(edge_1, 41, 102, -13.0F, -91.0F, -23.0F, 25, 4, 6, 0.0F, false));
		edge_1.cubeList.add(new ModelBox(edge_1, 38, 93, -10.0F, -82.0F, -23.0F, 19, 4, 6, 0.0F, false));
		edge_1.cubeList.add(new ModelBox(edge_1, 49, 95, -9.0F, -93.0F, -20.0F, 20, 14, 4, 0.0F, false));
		edge_1.cubeList.add(new ModelBox(edge_1, 193, 64, -8.0F, -117.0F, -16.0F, 16, 1, 2, 0.0F, false));
		edge_1.cubeList.add(new ModelBox(edge_1, 80, 110, -8.0F, -116.0F, -17.0F, 16, 1, 3, 0.0F, false));
		edge_1.cubeList.add(new ModelBox(edge_1, 194, 71, -8.0F, -115.0F, -16.0F, 16, 1, 2, 0.0F, false));

		cooling_blades_1 = new RendererModel(this);
		cooling_blades_1.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_1.addChild(cooling_blades_1);
		cooling_blades_1.cubeList.add(new ModelBox(cooling_blades_1, 41, 90, -10.9F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));
		cooling_blades_1.cubeList.add(new ModelBox(cooling_blades_1, 30, 85, -2.0F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));
		cooling_blades_1.cubeList.add(new ModelBox(cooling_blades_1, 46, 84, 7.1F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));

		plasma_coil_1 = new RendererModel(this);
		plasma_coil_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		edge_1.addChild(plasma_coil_1);
		plasma_coil_1.cubeList.add(new ModelBox(plasma_coil_1, 146, 12, -10.0F, -81.0F, -19.0F, 20, 2, 2, 0.0F, false));

		belly_1 = new RendererModel(this);
		belly_1.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_1.addChild(belly_1);
		belly_1.cubeList.add(new ModelBox(belly_1, 46, 103, -14.0F, -19.0F, -24.0F, 28, 8, 3, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 41, 88, -13.0F, -61.0F, -24.0F, 26, 20, 3, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 42, 117, -14.0F, -2.0F, -28.0F, 28, 3, 12, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 46, 120, -14.0F, -4.0F, -27.0F, 28, 2, 2, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 48, 112, -14.0F, -11.0F, -26.0F, 27, 7, 3, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 47, 105, -14.0F, -59.0F, -26.0F, 27, 10, 3, 0.0F, false));

		bolts = new RendererModel(this);
		bolts.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_1.addChild(bolts);
		bolts.cubeList.add(new ModelBox(bolts, 79, 80, -11.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts.cubeList.add(new ModelBox(bolts, 79, 80, -6.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts.cubeList.add(new ModelBox(bolts, 79, 80, -1.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts.cubeList.add(new ModelBox(bolts, 79, 80, 4.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts.cubeList.add(new ModelBox(bolts, 79, 80, 9.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));

		bolts_b = new RendererModel(this);
		bolts_b.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_1.addChild(bolts_b);
		bolts_b.cubeList.add(new ModelBox(bolts_b, 72, 82, -11.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b.cubeList.add(new ModelBox(bolts_b, 72, 82, -6.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b.cubeList.add(new ModelBox(bolts_b, 72, 82, -1.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b.cubeList.add(new ModelBox(bolts_b, 72, 82, 4.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b.cubeList.add(new ModelBox(bolts_b, 72, 82, 9.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));

		bolts_c = new RendererModel(this);
		bolts_c.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_1.addChild(bolts_c);
		bolts_c.cubeList.add(new ModelBox(bolts_c, 77, 80, -9.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c.cubeList.add(new ModelBox(bolts_c, 77, 80, -4.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c.cubeList.add(new ModelBox(bolts_c, 77, 80, 2.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c.cubeList.add(new ModelBox(bolts_c, 77, 80, 7.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));

		plane_1 = new RendererModel(this);
		plane_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(plane_1, 0.5236F, 0.0F, 0.0F);
		station_1.addChild(plane_1);
		plane_1.cubeList.add(new ModelBox(plane_1, 5, 42, -31.2F, -80.0F, -20.0F, 62, 4, 6, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 5, 42, -28.2F, -80.0F, -14.0F, 56, 4, 6, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 5, 42, -26.0F, -80.0F, -8.0F, 51, 4, 6, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 5, 42, -22.0F, -80.0F, -2.0F, 44, 4, 6, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 2, 82, -17.0F, -77.0F, -17.0F, 34, 8, 30, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 17, 92, -11.0F, -69.0F, -11.0F, 22, 4, 24, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 5, 42, -19.0F, -80.0F, 4.0F, 38, 4, 6, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 5, 42, -16.0F, -80.0F, 10.0F, 32, 4, 6, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 5, 42, -14.0F, -80.0F, 16.0F, 27, 4, 6, 0.0F, false));

		rib_1 = new RendererModel(this);
		rib_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_1, 0.0F, -0.5236F, 0.0F);
		station_1.addChild(rib_1);

		rib_tilt_1 = new RendererModel(this);
		rib_tilt_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_tilt_1, 0.4363F, 0.0F, 0.0F);
		rib_1.addChild(rib_tilt_1);
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 20, 76, -2.0F, -83.4F, -39.5F, 4, 8, 48, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 20, 75, -1.0F, -76.0F, -39.0F, 2, 6, 52, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 65, 94, -1.0F, -76.0F, -13.0F, 2, 26, 11, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 65, 94, -2.0F, -70.0F, -8.0F, 4, 14, 22, 0.0F, false));

		rib_deco_1 = new RendererModel(this);
		rib_deco_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_tilt_1.addChild(rib_deco_1);
		rib_deco_1.cubeList.add(new ModelBox(rib_deco_1, 87, 112, -3.0F, -84.5F, -40.0F, 6, 8, 8, 0.0F, false));
		rib_deco_1.cubeList.add(new ModelBox(rib_deco_1, 65, 94, -4.0F, -86.0F, 9.0F, 8, 8, 8, 0.0F, false));
		rib_deco_1.cubeList.add(new ModelBox(rib_deco_1, 65, 94, -3.0F, -84.4F, 8.0F, 6, 7, 8, 0.0F, false));

		lowerrib_tilt_2 = new RendererModel(this);
		lowerrib_tilt_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(lowerrib_tilt_2, 0.4363F, 0.0F, 0.0F);
		rib_1.addChild(lowerrib_tilt_2);
		lowerrib_tilt_2.cubeList.add(new ModelBox(lowerrib_tilt_2, 205, 68, -1.0F, -70.0F, -19.0F, 2, 20, 2, 0.0F, false));
		lowerrib_tilt_2.cubeList.add(new ModelBox(lowerrib_tilt_2, 65, 94, -2.0F, -70.0F, -8.0F, 4, 14, 22, 0.0F, false));
		lowerrib_tilt_2.cubeList.add(new ModelBox(lowerrib_tilt_2, 65, 94, -1.0F, -21.8F, -41.0F, 2, 2, 17, 0.0F, false));
		lowerrib_tilt_2.cubeList.add(new ModelBox(lowerrib_tilt_2, 65, 94, -1.0F, -19.8F, -34.0F, 2, 3, 9, 0.0F, false));
		lowerrib_tilt_2.cubeList.add(new ModelBox(lowerrib_tilt_2, 170, 82, -1.0F, -27.8F, -49.0F, 2, 2, 20, 0.0F, false));

		base_fin_1 = new RendererModel(this);
		base_fin_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(base_fin_1, 0.0F, -0.5236F, 0.0F);
		station_1.addChild(base_fin_1);
		base_fin_1.cubeList.add(new ModelBox(base_fin_1, 91, 90, -1.0F, -48.2F, -23.0F, 2, 28, 4, 0.0F, false));
		base_fin_1.cubeList.add(new ModelBox(base_fin_1, 60, 92, -2.0F, -94.2F, -27.0F, 4, 24, 12, 0.0F, false));
		base_fin_1.cubeList.add(new ModelBox(base_fin_1, 51, 92, -2.0F, -118.2F, -19.0F, 4, 24, 4, 0.0F, false));
		base_fin_1.cubeList.add(new ModelBox(base_fin_1, 76, 112, -2.4F, -13.2F, -32.0F, 5, 13, 6, 0.0F, false));
		base_fin_1.cubeList.add(new ModelBox(base_fin_1, 63, 98, -1.4F, -21.2F, -28.0F, 3, 8, 8, 0.0F, false));
		base_fin_1.cubeList.add(new ModelBox(base_fin_1, 174, 70, -0.4F, -41.0F, -33.0F, 1, 40, 2, 0.0F, false));

		clawfoot_1 = new RendererModel(this);
		clawfoot_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(clawfoot_1, 0.0F, -0.5236F, 0.0F);
		station_1.addChild(clawfoot_1);
		clawfoot_1.cubeList.add(new ModelBox(clawfoot_1, 231, 8, -1.5F, -38.7F, -39.4F, 3, 4, 3, 0.0F, false));
		clawfoot_1.cubeList.add(new ModelBox(clawfoot_1, 225, 12, -1.4F, -26.2F, -34.0F, 3, 4, 3, 0.0F, false));
		clawfoot_1.cubeList.add(new ModelBox(clawfoot_1, 226, 10, -1.4F, -26.2F, -39.4F, 3, 4, 3, 0.0F, false));
		clawfoot_1.cubeList.add(new ModelBox(clawfoot_1, 194, 81, -0.3F, -25.3F, -36.4F, 1, 2, 3, 0.0F, false));
		clawfoot_1.cubeList.add(new ModelBox(clawfoot_1, 178, 66, -0.5F, -39.0F, -39.0F, 1, 28, 2, 0.0F, false));
		clawfoot_1.cubeList.add(new ModelBox(clawfoot_1, 224, 16, -1.5F, -13.2F, -39.4F, 3, 4, 3, 0.0F, false));

		leg_1 = new RendererModel(this);
		leg_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_1.addChild(leg_1);
		leg_1.cubeList.add(new ModelBox(leg_1, 44, 102, -2.4F, -4.2F, -58.0F, 5, 4, 27, 0.0F, false));

		ball_1 = new RendererModel(this);
		ball_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_1.addChild(ball_1);
		ball_1.cubeList.add(new ModelBox(ball_1, 29, 102, -3.4F, -6.0F, -57.0F, 7, 6, 6, 0.0F, false));
		ball_1.cubeList.add(new ModelBox(ball_1, 29, 102, -3.4F, -6.0F, -47.0F, 7, 6, 2, 0.0F, false));
		ball_1.cubeList.add(new ModelBox(ball_1, 29, 102, -3.4F, -6.0F, -44.6F, 7, 6, 2, 0.0F, false));
		ball_1.cubeList.add(new ModelBox(ball_1, 29, 102, -3.4F, -6.0F, -42.1F, 7, 6, 2, 0.0F, false));

		station_2 = new RendererModel(this);
		station_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_2, 0.0F, -1.0472F, 0.0F);
		stations.addChild(station_2);

		edge_2 = new RendererModel(this);
		edge_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_2.addChild(edge_2);
		edge_2.cubeList.add(new ModelBox(edge_2, 13, 1, -34.0F, -60.0F, -61.0F, 67, 4, 6, 0.0F, false));
		edge_2.cubeList.add(new ModelBox(edge_2, 59, 96, -10.0F, -79.0F, -21.0F, 20, 16, 6, 0.0F, false));
		edge_2.cubeList.add(new ModelBox(edge_2, 40, 95, -10.0F, -71.0F, -15.0F, 20, 8, 6, 0.0F, false));
		edge_2.cubeList.add(new ModelBox(edge_2, 41, 102, -13.0F, -91.0F, -23.0F, 25, 4, 6, 0.0F, false));
		edge_2.cubeList.add(new ModelBox(edge_2, 38, 93, -10.0F, -82.0F, -23.0F, 19, 4, 6, 0.0F, false));
		edge_2.cubeList.add(new ModelBox(edge_2, 49, 95, -9.0F, -93.0F, -20.0F, 20, 14, 4, 0.0F, false));
		edge_2.cubeList.add(new ModelBox(edge_2, 193, 64, -8.0F, -117.0F, -16.0F, 16, 1, 2, 0.0F, false));
		edge_2.cubeList.add(new ModelBox(edge_2, 80, 110, -8.0F, -116.0F, -17.0F, 16, 1, 3, 0.0F, false));
		edge_2.cubeList.add(new ModelBox(edge_2, 194, 71, -8.0F, -115.0F, -16.0F, 16, 1, 2, 0.0F, false));

		cooling_blades_2 = new RendererModel(this);
		cooling_blades_2.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_2.addChild(cooling_blades_2);
		cooling_blades_2.cubeList.add(new ModelBox(cooling_blades_2, 41, 90, -10.9F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));
		cooling_blades_2.cubeList.add(new ModelBox(cooling_blades_2, 30, 85, -2.0F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));
		cooling_blades_2.cubeList.add(new ModelBox(cooling_blades_2, 46, 84, 7.1F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));

		plasma_coil_2 = new RendererModel(this);
		plasma_coil_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		edge_2.addChild(plasma_coil_2);
		plasma_coil_2.cubeList.add(new ModelBox(plasma_coil_2, 146, 12, -10.0F, -81.0F, -19.0F, 20, 2, 2, 0.0F, false));

		belly_2 = new RendererModel(this);
		belly_2.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_2.addChild(belly_2);
		belly_2.cubeList.add(new ModelBox(belly_2, 44, 108, -14.0F, -19.0F, -24.0F, 28, 8, 3, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 41, 88, -13.0F, -61.0F, -24.0F, 26, 20, 3, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 47, 117, -14.0F, -2.0F, -28.0F, 28, 3, 12, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 39, 121, -14.0F, -4.0F, -27.0F, 28, 2, 2, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 42, 105, -14.0F, -11.0F, -26.0F, 27, 7, 3, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 40, 105, -14.0F, -59.0F, -26.0F, 27, 10, 3, 0.0F, false));

		bolts2 = new RendererModel(this);
		bolts2.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_2.addChild(bolts2);
		bolts2.cubeList.add(new ModelBox(bolts2, 72, 80, -11.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts2.cubeList.add(new ModelBox(bolts2, 72, 80, -6.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts2.cubeList.add(new ModelBox(bolts2, 72, 80, -1.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts2.cubeList.add(new ModelBox(bolts2, 72, 80, 4.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts2.cubeList.add(new ModelBox(bolts2, 72, 80, 9.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));

		bolts_b2 = new RendererModel(this);
		bolts_b2.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_2.addChild(bolts_b2);
		bolts_b2.cubeList.add(new ModelBox(bolts_b2, 75, 83, -11.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b2.cubeList.add(new ModelBox(bolts_b2, 75, 83, -6.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b2.cubeList.add(new ModelBox(bolts_b2, 75, 83, -1.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b2.cubeList.add(new ModelBox(bolts_b2, 75, 83, 4.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b2.cubeList.add(new ModelBox(bolts_b2, 75, 83, 9.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));

		bolts_c2 = new RendererModel(this);
		bolts_c2.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_2.addChild(bolts_c2);
		bolts_c2.cubeList.add(new ModelBox(bolts_c2, 82, 85, -9.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c2.cubeList.add(new ModelBox(bolts_c2, 82, 85, -4.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c2.cubeList.add(new ModelBox(bolts_c2, 82, 85, 2.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c2.cubeList.add(new ModelBox(bolts_c2, 82, 85, 7.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));

		plane_2 = new RendererModel(this);
		plane_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(plane_2, 0.5236F, 0.0F, 0.0F);
		station_2.addChild(plane_2);
		plane_2.cubeList.add(new ModelBox(plane_2, 5, 42, -31.2F, -80.0F, -20.0F, 62, 4, 6, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 5, 42, -28.2F, -80.0F, -14.0F, 56, 4, 6, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 5, 42, -26.0F, -80.0F, -8.0F, 51, 4, 6, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 5, 42, -22.0F, -80.0F, -2.0F, 44, 4, 6, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 2, 82, -17.0F, -77.0F, -17.0F, 34, 8, 30, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 17, 92, -11.0F, -69.0F, -11.0F, 22, 4, 24, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 5, 42, -19.0F, -80.0F, 4.0F, 38, 4, 6, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 5, 42, -16.0F, -80.0F, 10.0F, 32, 4, 6, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 5, 42, -14.0F, -80.0F, 16.0F, 27, 4, 6, 0.0F, false));

		rib_2 = new RendererModel(this);
		rib_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_2, 0.0F, -0.5236F, 0.0F);
		station_2.addChild(rib_2);

		rib_tilt_2 = new RendererModel(this);
		rib_tilt_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_tilt_2, 0.4363F, 0.0F, 0.0F);
		rib_2.addChild(rib_tilt_2);
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 20, 76, -2.0F, -83.4F, -39.5F, 4, 8, 48, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 20, 75, -1.0F, -76.0F, -39.0F, 2, 6, 52, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 65, 94, -1.0F, -76.0F, -13.0F, 2, 26, 11, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 65, 94, -2.0F, -70.0F, -8.0F, 4, 14, 22, 0.0F, false));

		rib_deco_2 = new RendererModel(this);
		rib_deco_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_tilt_2.addChild(rib_deco_2);
		rib_deco_2.cubeList.add(new ModelBox(rib_deco_2, 87, 114, -3.0F, -84.5F, -40.0F, 6, 8, 8, 0.0F, false));
		rib_deco_2.cubeList.add(new ModelBox(rib_deco_2, 65, 94, -4.0F, -86.0F, 9.0F, 8, 8, 8, 0.0F, false));
		rib_deco_2.cubeList.add(new ModelBox(rib_deco_2, 65, 94, -3.0F, -84.4F, 8.0F, 6, 7, 8, 0.0F, false));

		lowerrib_tilt_3 = new RendererModel(this);
		lowerrib_tilt_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(lowerrib_tilt_3, 0.4363F, 0.0F, 0.0F);
		rib_2.addChild(lowerrib_tilt_3);
		lowerrib_tilt_3.cubeList.add(new ModelBox(lowerrib_tilt_3, 65, 94, -2.0F, -70.0F, -8.0F, 4, 14, 22, 0.0F, false));

		base_fin_2 = new RendererModel(this);
		base_fin_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(base_fin_2, 0.0F, -0.5236F, 0.0F);
		station_2.addChild(base_fin_2);
		base_fin_2.cubeList.add(new ModelBox(base_fin_2, 91, 90, -1.0F, -48.2F, -23.0F, 2, 28, 4, 0.0F, false));
		base_fin_2.cubeList.add(new ModelBox(base_fin_2, 60, 92, -2.0F, -94.2F, -27.0F, 4, 24, 12, 0.0F, false));
		base_fin_2.cubeList.add(new ModelBox(base_fin_2, 51, 92, -2.0F, -118.2F, -19.0F, 4, 24, 4, 0.0F, false));
		base_fin_2.cubeList.add(new ModelBox(base_fin_2, 75, 105, -2.4F, -13.2F, -32.0F, 5, 14, 6, 0.0F, false));
		base_fin_2.cubeList.add(new ModelBox(base_fin_2, 63, 98, -1.4F, -21.2F, -28.0F, 3, 8, 8, 0.0F, false));

		clawfoot_2 = new RendererModel(this);
		clawfoot_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(clawfoot_2, 0.0F, -0.5236F, 0.0F);
		station_2.addChild(clawfoot_2);

		leg_2 = new RendererModel(this);
		leg_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_2.addChild(leg_2);

		ball_2 = new RendererModel(this);
		ball_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_2.addChild(ball_2);

		station_3 = new RendererModel(this);
		station_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_3, 0.0F, -2.0944F, 0.0F);
		stations.addChild(station_3);

		edge_3 = new RendererModel(this);
		edge_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_3.addChild(edge_3);
		edge_3.cubeList.add(new ModelBox(edge_3, 13, 1, -34.0F, -60.0F, -61.0F, 67, 4, 6, 0.0F, false));
		edge_3.cubeList.add(new ModelBox(edge_3, 59, 96, -10.0F, -79.0F, -21.0F, 20, 16, 6, 0.0F, false));
		edge_3.cubeList.add(new ModelBox(edge_3, 40, 95, -10.0F, -71.0F, -15.0F, 20, 8, 6, 0.0F, false));
		edge_3.cubeList.add(new ModelBox(edge_3, 41, 102, -13.0F, -91.0F, -23.0F, 25, 4, 6, 0.0F, false));
		edge_3.cubeList.add(new ModelBox(edge_3, 38, 93, -10.0F, -82.0F, -23.0F, 19, 4, 6, 0.0F, false));
		edge_3.cubeList.add(new ModelBox(edge_3, 49, 95, -9.0F, -93.0F, -20.0F, 20, 14, 4, 0.0F, false));
		edge_3.cubeList.add(new ModelBox(edge_3, 193, 64, -8.0F, -117.0F, -16.0F, 16, 1, 2, 0.0F, false));
		edge_3.cubeList.add(new ModelBox(edge_3, 80, 110, -8.0F, -116.0F, -17.0F, 16, 1, 3, 0.0F, false));
		edge_3.cubeList.add(new ModelBox(edge_3, 194, 71, -8.0F, -115.0F, -16.0F, 16, 1, 2, 0.0F, false));

		cooling_blades_3 = new RendererModel(this);
		cooling_blades_3.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_3.addChild(cooling_blades_3);
		cooling_blades_3.cubeList.add(new ModelBox(cooling_blades_3, 41, 90, -10.9F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));
		cooling_blades_3.cubeList.add(new ModelBox(cooling_blades_3, 30, 85, -2.0F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));
		cooling_blades_3.cubeList.add(new ModelBox(cooling_blades_3, 46, 84, 7.1F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));

		plasma_coil_3 = new RendererModel(this);
		plasma_coil_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		edge_3.addChild(plasma_coil_3);
		plasma_coil_3.cubeList.add(new ModelBox(plasma_coil_3, 146, 12, -10.0F, -81.0F, -19.0F, 20, 2, 2, 0.0F, false));

		belly_3 = new RendererModel(this);
		belly_3.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_3.addChild(belly_3);
		belly_3.cubeList.add(new ModelBox(belly_3, 40, 105, -14.0F, -19.0F, -24.0F, 28, 8, 3, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 41, 88, -13.0F, -61.0F, -24.0F, 26, 20, 3, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 39, 118, -14.0F, -2.0F, -28.0F, 28, 3, 12, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 48, 119, -14.0F, -4.0F, -27.0F, 28, 2, 2, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 45, 114, -14.0F, -11.0F, -26.0F, 27, 7, 3, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 51, 108, -14.0F, -59.0F, -26.0F, 27, 10, 3, 0.0F, false));

		bolts3 = new RendererModel(this);
		bolts3.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_3.addChild(bolts3);
		bolts3.cubeList.add(new ModelBox(bolts3, 82, 83, -11.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts3.cubeList.add(new ModelBox(bolts3, 82, 83, -6.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts3.cubeList.add(new ModelBox(bolts3, 82, 83, -1.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts3.cubeList.add(new ModelBox(bolts3, 82, 83, 4.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts3.cubeList.add(new ModelBox(bolts3, 82, 83, 9.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));

		bolts_b3 = new RendererModel(this);
		bolts_b3.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_3.addChild(bolts_b3);
		bolts_b3.cubeList.add(new ModelBox(bolts_b3, 75, 84, -11.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b3.cubeList.add(new ModelBox(bolts_b3, 75, 84, -6.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b3.cubeList.add(new ModelBox(bolts_b3, 75, 84, -1.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b3.cubeList.add(new ModelBox(bolts_b3, 75, 84, 4.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b3.cubeList.add(new ModelBox(bolts_b3, 75, 84, 9.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));

		bolts_c3 = new RendererModel(this);
		bolts_c3.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_3.addChild(bolts_c3);
		bolts_c3.cubeList.add(new ModelBox(bolts_c3, 57, 80, -9.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c3.cubeList.add(new ModelBox(bolts_c3, 57, 80, -4.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c3.cubeList.add(new ModelBox(bolts_c3, 57, 80, 2.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c3.cubeList.add(new ModelBox(bolts_c3, 57, 80, 7.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));

		plane_3 = new RendererModel(this);
		plane_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(plane_3, 0.5236F, 0.0F, 0.0F);
		station_3.addChild(plane_3);
		plane_3.cubeList.add(new ModelBox(plane_3, 5, 42, -31.2F, -80.0F, -20.0F, 62, 4, 6, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 5, 42, -28.2F, -80.0F, -14.0F, 56, 4, 6, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 5, 42, -26.0F, -80.0F, -8.0F, 51, 4, 6, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 5, 42, -22.0F, -80.0F, -2.0F, 44, 4, 6, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 2, 82, -17.0F, -77.0F, -17.0F, 34, 8, 30, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 17, 92, -11.0F, -69.0F, -11.0F, 22, 4, 24, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 5, 42, -19.0F, -80.0F, 4.0F, 38, 4, 6, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 5, 42, -16.0F, -80.0F, 10.0F, 32, 4, 6, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 5, 42, -14.0F, -80.0F, 16.0F, 27, 4, 6, 0.0F, false));

		rib_3 = new RendererModel(this);
		rib_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_3, 0.0F, -0.5236F, 0.0F);
		station_3.addChild(rib_3);

		rib_tilt_3 = new RendererModel(this);
		rib_tilt_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_tilt_3, 0.4363F, 0.0F, 0.0F);
		rib_3.addChild(rib_tilt_3);
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 20, 76, -2.0F, -83.4F, -39.5F, 4, 8, 48, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 20, 75, -1.0F, -76.0F, -39.0F, 2, 6, 52, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 65, 94, -1.0F, -76.0F, -13.0F, 2, 26, 11, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 65, 94, -2.0F, -70.0F, -8.0F, 4, 14, 22, 0.0F, false));

		rib_deco_3 = new RendererModel(this);
		rib_deco_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_tilt_3.addChild(rib_deco_3);
		rib_deco_3.cubeList.add(new ModelBox(rib_deco_3, 86, 114, -3.0F, -84.5F, -40.0F, 6, 8, 8, 0.0F, false));
		rib_deco_3.cubeList.add(new ModelBox(rib_deco_3, 65, 94, -4.0F, -86.0F, 9.0F, 8, 8, 8, 0.0F, false));
		rib_deco_3.cubeList.add(new ModelBox(rib_deco_3, 65, 94, -3.0F, -84.4F, 8.0F, 6, 7, 8, 0.0F, false));

		lowerrib_tilt_4 = new RendererModel(this);
		lowerrib_tilt_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(lowerrib_tilt_4, 0.4363F, 0.0F, 0.0F);
		rib_3.addChild(lowerrib_tilt_4);
		lowerrib_tilt_4.cubeList.add(new ModelBox(lowerrib_tilt_4, 205, 68, -1.0F, -70.0F, -19.0F, 2, 20, 2, 0.0F, false));
		lowerrib_tilt_4.cubeList.add(new ModelBox(lowerrib_tilt_4, 65, 94, -2.0F, -70.0F, -8.0F, 4, 14, 22, 0.0F, false));
		lowerrib_tilt_4.cubeList.add(new ModelBox(lowerrib_tilt_4, 65, 94, -1.0F, -21.8F, -41.0F, 2, 2, 17, 0.0F, false));
		lowerrib_tilt_4.cubeList.add(new ModelBox(lowerrib_tilt_4, 65, 94, -1.0F, -19.8F, -34.0F, 2, 3, 9, 0.0F, false));
		lowerrib_tilt_4.cubeList.add(new ModelBox(lowerrib_tilt_4, 170, 82, -1.0F, -27.8F, -49.0F, 2, 2, 20, 0.0F, false));

		base_fin_3 = new RendererModel(this);
		base_fin_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(base_fin_3, 0.0F, -0.5236F, 0.0F);
		station_3.addChild(base_fin_3);
		base_fin_3.cubeList.add(new ModelBox(base_fin_3, 91, 90, -1.0F, -48.2F, -23.0F, 2, 28, 4, 0.0F, false));
		base_fin_3.cubeList.add(new ModelBox(base_fin_3, 60, 92, -2.0F, -94.2F, -27.0F, 4, 24, 12, 0.0F, false));
		base_fin_3.cubeList.add(new ModelBox(base_fin_3, 51, 92, -2.0F, -118.2F, -19.0F, 4, 24, 4, 0.0F, false));
		base_fin_3.cubeList.add(new ModelBox(base_fin_3, 84, 105, -2.4F, -13.2F, -32.0F, 5, 13, 6, 0.0F, false));
		base_fin_3.cubeList.add(new ModelBox(base_fin_3, 63, 98, -1.4F, -21.2F, -28.0F, 3, 8, 8, 0.0F, false));
		base_fin_3.cubeList.add(new ModelBox(base_fin_3, 174, 70, -0.4F, -41.0F, -33.0F, 1, 40, 2, 0.0F, false));

		clawfoot_3 = new RendererModel(this);
		clawfoot_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(clawfoot_3, 0.0F, -0.5236F, 0.0F);
		station_3.addChild(clawfoot_3);
		clawfoot_3.cubeList.add(new ModelBox(clawfoot_3, 231, 8, -1.5F, -38.7F, -39.4F, 3, 4, 3, 0.0F, false));
		clawfoot_3.cubeList.add(new ModelBox(clawfoot_3, 225, 12, -1.4F, -26.2F, -34.0F, 3, 4, 3, 0.0F, false));
		clawfoot_3.cubeList.add(new ModelBox(clawfoot_3, 226, 10, -1.4F, -26.2F, -39.4F, 3, 4, 3, 0.0F, false));
		clawfoot_3.cubeList.add(new ModelBox(clawfoot_3, 194, 81, -0.3F, -25.3F, -36.4F, 1, 2, 3, 0.0F, false));
		clawfoot_3.cubeList.add(new ModelBox(clawfoot_3, 178, 66, -0.5F, -39.0F, -39.0F, 1, 28, 2, 0.0F, false));
		clawfoot_3.cubeList.add(new ModelBox(clawfoot_3, 224, 16, -1.5F, -13.2F, -39.4F, 3, 4, 3, 0.0F, false));

		leg_3 = new RendererModel(this);
		leg_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_3.addChild(leg_3);
		leg_3.cubeList.add(new ModelBox(leg_3, 54, 101, -2.4F, -4.2F, -58.0F, 5, 4, 27, 0.0F, false));

		ball_3 = new RendererModel(this);
		ball_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_3.addChild(ball_3);
		ball_3.cubeList.add(new ModelBox(ball_3, 29, 102, -3.4F, -6.0F, -57.0F, 7, 6, 6, 0.0F, false));
		ball_3.cubeList.add(new ModelBox(ball_3, 29, 102, -3.4F, -6.0F, -47.0F, 7, 6, 2, 0.0F, false));
		ball_3.cubeList.add(new ModelBox(ball_3, 29, 102, -3.4F, -6.0F, -44.6F, 7, 6, 2, 0.0F, false));
		ball_3.cubeList.add(new ModelBox(ball_3, 29, 102, -3.4F, -6.0F, -42.1F, 7, 6, 2, 0.0F, false));

		station_4 = new RendererModel(this);
		station_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_4, 0.0F, 3.1416F, 0.0F);
		stations.addChild(station_4);

		edge_4 = new RendererModel(this);
		edge_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_4.addChild(edge_4);
		edge_4.cubeList.add(new ModelBox(edge_4, 13, 1, -34.0F, -60.0F, -61.0F, 67, 4, 6, 0.0F, false));
		edge_4.cubeList.add(new ModelBox(edge_4, 59, 96, -10.0F, -79.0F, -21.0F, 20, 16, 6, 0.0F, false));
		edge_4.cubeList.add(new ModelBox(edge_4, 40, 95, -10.0F, -71.0F, -15.0F, 20, 8, 6, 0.0F, false));
		edge_4.cubeList.add(new ModelBox(edge_4, 41, 102, -13.0F, -91.0F, -23.0F, 25, 4, 6, 0.0F, false));
		edge_4.cubeList.add(new ModelBox(edge_4, 38, 93, -10.0F, -82.0F, -23.0F, 19, 4, 6, 0.0F, false));
		edge_4.cubeList.add(new ModelBox(edge_4, 49, 95, -9.0F, -93.0F, -20.0F, 20, 14, 4, 0.0F, false));
		edge_4.cubeList.add(new ModelBox(edge_4, 193, 64, -8.0F, -117.0F, -16.0F, 16, 1, 2, 0.0F, false));
		edge_4.cubeList.add(new ModelBox(edge_4, 80, 110, -8.0F, -116.0F, -17.0F, 16, 1, 3, 0.0F, false));
		edge_4.cubeList.add(new ModelBox(edge_4, 194, 71, -8.0F, -115.0F, -16.0F, 16, 1, 2, 0.0F, false));

		cooling_blades_4 = new RendererModel(this);
		cooling_blades_4.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_4.addChild(cooling_blades_4);
		cooling_blades_4.cubeList.add(new ModelBox(cooling_blades_4, 41, 90, -10.9F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));
		cooling_blades_4.cubeList.add(new ModelBox(cooling_blades_4, 30, 85, -2.0F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));
		cooling_blades_4.cubeList.add(new ModelBox(cooling_blades_4, 46, 84, 7.1F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));

		plasma_coil_4 = new RendererModel(this);
		plasma_coil_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		edge_4.addChild(plasma_coil_4);
		plasma_coil_4.cubeList.add(new ModelBox(plasma_coil_4, 146, 12, -10.0F, -81.0F, -19.0F, 20, 2, 2, 0.0F, false));

		belly_4 = new RendererModel(this);
		belly_4.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_4.addChild(belly_4);
		belly_4.cubeList.add(new ModelBox(belly_4, 43, 104, -14.0F, -19.0F, -24.0F, 28, 8, 3, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 41, 88, -13.0F, -61.0F, -24.0F, 26, 20, 3, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 41, 119, -14.0F, -2.0F, -28.0F, 28, 3, 12, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 55, 121, -15.0F, -4.0F, -27.0F, 28, 2, 2, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 42, 107, -14.0F, -11.0F, -26.0F, 27, 7, 3, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 46, 114, -14.0F, -59.0F, -26.0F, 27, 10, 3, 0.0F, false));

		bolts4 = new RendererModel(this);
		bolts4.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_4.addChild(bolts4);
		bolts4.cubeList.add(new ModelBox(bolts4, 78, 78, -11.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts4.cubeList.add(new ModelBox(bolts4, 78, 78, -6.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts4.cubeList.add(new ModelBox(bolts4, 78, 78, -1.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts4.cubeList.add(new ModelBox(bolts4, 78, 78, 4.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts4.cubeList.add(new ModelBox(bolts4, 78, 78, 9.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));

		bolts_b4 = new RendererModel(this);
		bolts_b4.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_4.addChild(bolts_b4);
		bolts_b4.cubeList.add(new ModelBox(bolts_b4, 78, 80, -11.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b4.cubeList.add(new ModelBox(bolts_b4, 78, 80, -6.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b4.cubeList.add(new ModelBox(bolts_b4, 78, 80, -1.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b4.cubeList.add(new ModelBox(bolts_b4, 78, 80, 4.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b4.cubeList.add(new ModelBox(bolts_b4, 78, 80, 9.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));

		bolts_c4 = new RendererModel(this);
		bolts_c4.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_4.addChild(bolts_c4);
		bolts_c4.cubeList.add(new ModelBox(bolts_c4, 58, 80, -9.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c4.cubeList.add(new ModelBox(bolts_c4, 58, 80, -4.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c4.cubeList.add(new ModelBox(bolts_c4, 58, 80, 2.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c4.cubeList.add(new ModelBox(bolts_c4, 58, 80, 7.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));

		plane_4 = new RendererModel(this);
		plane_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(plane_4, 0.5236F, 0.0F, 0.0F);
		station_4.addChild(plane_4);
		plane_4.cubeList.add(new ModelBox(plane_4, 5, 42, -31.2F, -80.0F, -20.0F, 62, 4, 6, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 5, 42, -28.2F, -80.0F, -14.0F, 56, 4, 6, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 5, 42, -26.0F, -80.0F, -8.0F, 51, 4, 6, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 5, 42, -22.0F, -80.0F, -2.0F, 44, 4, 6, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 2, 82, -17.0F, -77.0F, -17.0F, 34, 8, 30, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 17, 92, -11.0F, -69.0F, -11.0F, 22, 4, 24, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 5, 42, -19.0F, -80.0F, 4.0F, 38, 4, 6, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 5, 42, -16.0F, -80.0F, 10.0F, 32, 4, 6, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 5, 42, -14.0F, -80.0F, 16.0F, 27, 4, 6, 0.0F, false));

		rib_4 = new RendererModel(this);
		rib_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_4, 0.0F, -0.5236F, 0.0F);
		station_4.addChild(rib_4);

		rib_tilt_4 = new RendererModel(this);
		rib_tilt_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_tilt_4, 0.4363F, 0.0F, 0.0F);
		rib_4.addChild(rib_tilt_4);
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 20, 76, -2.0F, -83.4F, -39.5F, 4, 8, 48, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 20, 75, -1.0F, -76.0F, -39.0F, 2, 6, 52, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 65, 94, -1.0F, -76.0F, -13.0F, 2, 26, 11, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 65, 94, -2.0F, -70.0F, -8.0F, 4, 14, 22, 0.0F, false));

		rib_deco_4 = new RendererModel(this);
		rib_deco_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_tilt_4.addChild(rib_deco_4);
		rib_deco_4.cubeList.add(new ModelBox(rib_deco_4, 79, 114, -3.0F, -84.5F, -40.0F, 6, 8, 8, 0.0F, false));
		rib_deco_4.cubeList.add(new ModelBox(rib_deco_4, 65, 94, -4.0F, -86.0F, 9.0F, 8, 8, 8, 0.0F, false));
		rib_deco_4.cubeList.add(new ModelBox(rib_deco_4, 65, 94, -3.0F, -84.4F, 8.0F, 6, 7, 8, 0.0F, false));

		lowerrib_tilt_5 = new RendererModel(this);
		lowerrib_tilt_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(lowerrib_tilt_5, 0.4363F, 0.0F, 0.0F);
		rib_4.addChild(lowerrib_tilt_5);
		lowerrib_tilt_5.cubeList.add(new ModelBox(lowerrib_tilt_5, 65, 94, -2.0F, -70.0F, -8.0F, 4, 14, 22, 0.0F, false));

		base_fin_4 = new RendererModel(this);
		base_fin_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(base_fin_4, 0.0F, -0.5236F, 0.0F);
		station_4.addChild(base_fin_4);
		base_fin_4.cubeList.add(new ModelBox(base_fin_4, 91, 90, -1.0F, -48.2F, -23.0F, 2, 28, 4, 0.0F, false));
		base_fin_4.cubeList.add(new ModelBox(base_fin_4, 60, 92, -2.0F, -94.2F, -27.0F, 4, 24, 12, 0.0F, false));
		base_fin_4.cubeList.add(new ModelBox(base_fin_4, 51, 92, -2.0F, -118.2F, -19.0F, 4, 24, 4, 0.0F, false));
		base_fin_4.cubeList.add(new ModelBox(base_fin_4, 71, 115, -2.4F, -13.2F, -32.0F, 5, 14, 6, 0.0F, false));
		base_fin_4.cubeList.add(new ModelBox(base_fin_4, 63, 98, -1.4F, -21.2F, -28.0F, 3, 8, 8, 0.0F, false));

		clawfoot_4 = new RendererModel(this);
		clawfoot_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(clawfoot_4, 0.0F, -0.5236F, 0.0F);
		station_4.addChild(clawfoot_4);

		leg_4 = new RendererModel(this);
		leg_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_4.addChild(leg_4);

		ball_4 = new RendererModel(this);
		ball_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_4.addChild(ball_4);

		station_5 = new RendererModel(this);
		station_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_5, 0.0F, 2.0944F, 0.0F);
		stations.addChild(station_5);

		edge_5 = new RendererModel(this);
		edge_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_5.addChild(edge_5);
		edge_5.cubeList.add(new ModelBox(edge_5, 13, 1, -34.0F, -60.0F, -61.0F, 67, 4, 6, 0.0F, false));
		edge_5.cubeList.add(new ModelBox(edge_5, 59, 96, -10.0F, -79.0F, -21.0F, 20, 16, 6, 0.0F, false));
		edge_5.cubeList.add(new ModelBox(edge_5, 40, 95, -10.0F, -71.0F, -15.0F, 20, 8, 6, 0.0F, false));
		edge_5.cubeList.add(new ModelBox(edge_5, 41, 102, -13.0F, -91.0F, -23.0F, 25, 4, 6, 0.0F, false));
		edge_5.cubeList.add(new ModelBox(edge_5, 38, 93, -10.0F, -82.0F, -23.0F, 19, 4, 6, 0.0F, false));
		edge_5.cubeList.add(new ModelBox(edge_5, 49, 95, -9.0F, -93.0F, -20.0F, 20, 14, 4, 0.0F, false));
		edge_5.cubeList.add(new ModelBox(edge_5, 193, 64, -8.0F, -117.0F, -16.0F, 16, 1, 2, 0.0F, false));
		edge_5.cubeList.add(new ModelBox(edge_5, 80, 110, -8.0F, -116.0F, -17.0F, 16, 1, 3, 0.0F, false));
		edge_5.cubeList.add(new ModelBox(edge_5, 194, 71, -8.0F, -115.0F, -16.0F, 16, 1, 2, 0.0F, false));

		cooling_blades_5 = new RendererModel(this);
		cooling_blades_5.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_5.addChild(cooling_blades_5);
		cooling_blades_5.cubeList.add(new ModelBox(cooling_blades_5, 41, 90, -10.9F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));
		cooling_blades_5.cubeList.add(new ModelBox(cooling_blades_5, 30, 85, -2.0F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));
		cooling_blades_5.cubeList.add(new ModelBox(cooling_blades_5, 46, 84, 7.1F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));

		plasma_coil_5 = new RendererModel(this);
		plasma_coil_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		edge_5.addChild(plasma_coil_5);
		plasma_coil_5.cubeList.add(new ModelBox(plasma_coil_5, 146, 12, -10.0F, -81.0F, -19.0F, 20, 2, 2, 0.0F, false));

		belly_5 = new RendererModel(this);
		belly_5.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_5.addChild(belly_5);
		belly_5.cubeList.add(new ModelBox(belly_5, 54, 102, -14.0F, -19.0F, -24.0F, 28, 8, 3, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 41, 88, -13.0F, -61.0F, -24.0F, 26, 20, 3, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 42, 119, -13.0F, -2.0F, -27.0F, 28, 3, 12, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 46, 119, -14.0F, -4.0F, -27.0F, 28, 2, 2, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 41, 112, -14.0F, -11.0F, -26.0F, 27, 7, 3, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 50, 110, -14.0F, -59.0F, -26.0F, 27, 10, 3, 0.0F, false));

		bolts5 = new RendererModel(this);
		bolts5.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_5.addChild(bolts5);
		bolts5.cubeList.add(new ModelBox(bolts5, 70, 79, -11.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts5.cubeList.add(new ModelBox(bolts5, 70, 79, -6.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts5.cubeList.add(new ModelBox(bolts5, 70, 79, -1.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts5.cubeList.add(new ModelBox(bolts5, 70, 79, 4.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts5.cubeList.add(new ModelBox(bolts5, 70, 79, 9.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));

		bolts_b5 = new RendererModel(this);
		bolts_b5.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_5.addChild(bolts_b5);
		bolts_b5.cubeList.add(new ModelBox(bolts_b5, 74, 79, -11.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b5.cubeList.add(new ModelBox(bolts_b5, 74, 79, -6.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b5.cubeList.add(new ModelBox(bolts_b5, 74, 79, -1.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b5.cubeList.add(new ModelBox(bolts_b5, 74, 79, 4.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b5.cubeList.add(new ModelBox(bolts_b5, 74, 79, 9.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));

		bolts_c5 = new RendererModel(this);
		bolts_c5.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_5.addChild(bolts_c5);
		bolts_c5.cubeList.add(new ModelBox(bolts_c5, 79, 81, -9.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c5.cubeList.add(new ModelBox(bolts_c5, 79, 81, -4.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c5.cubeList.add(new ModelBox(bolts_c5, 79, 81, 2.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c5.cubeList.add(new ModelBox(bolts_c5, 79, 81, 7.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));

		plane_5 = new RendererModel(this);
		plane_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(plane_5, 0.5236F, 0.0F, 0.0F);
		station_5.addChild(plane_5);
		plane_5.cubeList.add(new ModelBox(plane_5, 5, 42, -31.2F, -80.0F, -20.0F, 62, 4, 6, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 5, 42, -28.2F, -80.0F, -14.0F, 56, 4, 6, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 5, 42, -26.0F, -80.0F, -8.0F, 51, 4, 6, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 5, 42, -22.0F, -80.0F, -2.0F, 44, 4, 6, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 2, 82, -17.0F, -77.0F, -17.0F, 34, 8, 30, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 17, 92, -11.0F, -69.0F, -11.0F, 22, 4, 24, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 5, 42, -19.0F, -80.0F, 4.0F, 38, 4, 6, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 5, 42, -16.0F, -80.0F, 10.0F, 32, 4, 6, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 5, 42, -14.0F, -80.0F, 16.0F, 27, 4, 6, 0.0F, false));

		rib_5 = new RendererModel(this);
		rib_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_5, 0.0F, -0.5236F, 0.0F);
		station_5.addChild(rib_5);

		rib_tilt_5 = new RendererModel(this);
		rib_tilt_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_tilt_5, 0.4363F, 0.0F, 0.0F);
		rib_5.addChild(rib_tilt_5);
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 20, 76, -2.0F, -83.4F, -39.5F, 4, 8, 48, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 20, 75, -1.0F, -76.0F, -39.0F, 2, 6, 52, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 65, 94, -1.0F, -76.0F, -13.0F, 2, 26, 11, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 65, 94, -2.0F, -70.0F, -8.0F, 4, 14, 22, 0.0F, false));

		rib_deco_5 = new RendererModel(this);
		rib_deco_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_tilt_5.addChild(rib_deco_5);
		rib_deco_5.cubeList.add(new ModelBox(rib_deco_5, 77, 114, -3.0F, -84.5F, -40.0F, 6, 8, 8, 0.0F, false));
		rib_deco_5.cubeList.add(new ModelBox(rib_deco_5, 65, 94, -4.0F, -86.0F, 9.0F, 8, 8, 8, 0.0F, false));
		rib_deco_5.cubeList.add(new ModelBox(rib_deco_5, 65, 94, -3.0F, -84.4F, 8.0F, 6, 7, 8, 0.0F, false));

		lowerrib_tilt_6 = new RendererModel(this);
		lowerrib_tilt_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(lowerrib_tilt_6, 0.4363F, 0.0F, 0.0F);
		rib_5.addChild(lowerrib_tilt_6);
		lowerrib_tilt_6.cubeList.add(new ModelBox(lowerrib_tilt_6, 205, 68, -1.0F, -70.0F, -19.0F, 2, 20, 2, 0.0F, false));
		lowerrib_tilt_6.cubeList.add(new ModelBox(lowerrib_tilt_6, 65, 94, -2.0F, -70.0F, -8.0F, 4, 14, 22, 0.0F, false));
		lowerrib_tilt_6.cubeList.add(new ModelBox(lowerrib_tilt_6, 65, 94, -1.0F, -21.8F, -41.0F, 2, 2, 17, 0.0F, false));
		lowerrib_tilt_6.cubeList.add(new ModelBox(lowerrib_tilt_6, 65, 94, -1.0F, -19.8F, -34.0F, 2, 3, 9, 0.0F, false));
		lowerrib_tilt_6.cubeList.add(new ModelBox(lowerrib_tilt_6, 170, 82, -1.0F, -27.8F, -49.0F, 2, 2, 20, 0.0F, false));

		base_fin_5 = new RendererModel(this);
		base_fin_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(base_fin_5, 0.0F, -0.5236F, 0.0F);
		station_5.addChild(base_fin_5);
		base_fin_5.cubeList.add(new ModelBox(base_fin_5, 91, 90, -1.0F, -48.2F, -23.0F, 2, 28, 4, 0.0F, false));
		base_fin_5.cubeList.add(new ModelBox(base_fin_5, 60, 92, -2.0F, -94.2F, -27.0F, 4, 24, 12, 0.0F, false));
		base_fin_5.cubeList.add(new ModelBox(base_fin_5, 51, 92, -2.0F, -118.2F, -19.0F, 4, 24, 4, 0.0F, false));
		base_fin_5.cubeList.add(new ModelBox(base_fin_5, 65, 113, -2.4F, -13.2F, -32.0F, 5, 13, 6, 0.0F, false));
		base_fin_5.cubeList.add(new ModelBox(base_fin_5, 63, 98, -1.4F, -21.2F, -28.0F, 3, 8, 8, 0.0F, false));
		base_fin_5.cubeList.add(new ModelBox(base_fin_5, 174, 70, -0.4F, -41.0F, -33.0F, 1, 40, 2, 0.0F, false));

		clawfoot_5 = new RendererModel(this);
		clawfoot_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(clawfoot_5, 0.0F, -0.5236F, 0.0F);
		station_5.addChild(clawfoot_5);
		clawfoot_5.cubeList.add(new ModelBox(clawfoot_5, 231, 8, -1.5F, -38.7F, -39.4F, 3, 4, 3, 0.0F, false));
		clawfoot_5.cubeList.add(new ModelBox(clawfoot_5, 225, 12, -1.4F, -26.2F, -34.0F, 3, 4, 3, 0.0F, false));
		clawfoot_5.cubeList.add(new ModelBox(clawfoot_5, 226, 10, -1.4F, -26.2F, -39.4F, 3, 4, 3, 0.0F, false));
		clawfoot_5.cubeList.add(new ModelBox(clawfoot_5, 194, 81, -0.3F, -25.3F, -36.4F, 1, 2, 3, 0.0F, false));
		clawfoot_5.cubeList.add(new ModelBox(clawfoot_5, 178, 66, -0.5F, -39.0F, -39.0F, 1, 28, 2, 0.0F, false));
		clawfoot_5.cubeList.add(new ModelBox(clawfoot_5, 224, 16, -1.5F, -13.2F, -39.4F, 3, 4, 3, 0.0F, false));

		leg_5 = new RendererModel(this);
		leg_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_5.addChild(leg_5);
		leg_5.cubeList.add(new ModelBox(leg_5, 42, 102, -2.4F, -4.2F, -58.0F, 5, 4, 27, 0.0F, false));

		ball_5 = new RendererModel(this);
		ball_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_5.addChild(ball_5);
		ball_5.cubeList.add(new ModelBox(ball_5, 29, 102, -3.4F, -6.0F, -57.0F, 7, 6, 6, 0.0F, false));
		ball_5.cubeList.add(new ModelBox(ball_5, 29, 102, -3.4F, -6.0F, -47.0F, 7, 6, 2, 0.0F, false));
		ball_5.cubeList.add(new ModelBox(ball_5, 29, 102, -3.4F, -6.0F, -44.6F, 7, 6, 2, 0.0F, false));
		ball_5.cubeList.add(new ModelBox(ball_5, 29, 102, -3.4F, -6.0F, -42.1F, 7, 6, 2, 0.0F, false));

		station_6 = new RendererModel(this);
		station_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_6, 0.0F, 1.0472F, 0.0F);
		stations.addChild(station_6);

		edge_6 = new RendererModel(this);
		edge_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_6.addChild(edge_6);
		edge_6.cubeList.add(new ModelBox(edge_6, 13, 1, -34.0F, -60.0F, -61.0F, 67, 4, 6, 0.0F, false));
		edge_6.cubeList.add(new ModelBox(edge_6, 59, 96, -10.0F, -79.0F, -21.0F, 20, 16, 6, 0.0F, false));
		edge_6.cubeList.add(new ModelBox(edge_6, 40, 95, -10.0F, -71.0F, -15.0F, 20, 8, 6, 0.0F, false));
		edge_6.cubeList.add(new ModelBox(edge_6, 41, 102, -13.0F, -91.0F, -23.0F, 25, 4, 6, 0.0F, false));
		edge_6.cubeList.add(new ModelBox(edge_6, 38, 93, -10.0F, -82.0F, -23.0F, 19, 4, 6, 0.0F, false));
		edge_6.cubeList.add(new ModelBox(edge_6, 49, 95, -9.0F, -93.0F, -20.0F, 20, 14, 4, 0.0F, false));
		edge_6.cubeList.add(new ModelBox(edge_6, 193, 64, -8.0F, -117.0F, -16.0F, 16, 1, 2, 0.0F, false));
		edge_6.cubeList.add(new ModelBox(edge_6, 80, 110, -8.0F, -116.0F, -17.0F, 16, 1, 3, 0.0F, false));
		edge_6.cubeList.add(new ModelBox(edge_6, 194, 71, -8.0F, -115.0F, -16.0F, 16, 1, 2, 0.0F, false));

		cooling_blades_6 = new RendererModel(this);
		cooling_blades_6.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_6.addChild(cooling_blades_6);
		cooling_blades_6.cubeList.add(new ModelBox(cooling_blades_6, 41, 90, -10.9F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));
		cooling_blades_6.cubeList.add(new ModelBox(cooling_blades_6, 30, 85, -2.0F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));
		cooling_blades_6.cubeList.add(new ModelBox(cooling_blades_6, 46, 84, 7.1F, -45.0F, -22.0F, 4, 28, 5, 0.0F, false));

		plasma_coil_6 = new RendererModel(this);
		plasma_coil_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		edge_6.addChild(plasma_coil_6);
		plasma_coil_6.cubeList.add(new ModelBox(plasma_coil_6, 146, 12, -10.0F, -81.0F, -19.0F, 20, 2, 2, 0.0F, false));

		belly_6 = new RendererModel(this);
		belly_6.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_6.addChild(belly_6);
		belly_6.cubeList.add(new ModelBox(belly_6, 45, 107, -14.0F, -19.0F, -24.0F, 28, 8, 3, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 41, 88, -13.0F, -61.0F, -24.0F, 26, 20, 3, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 45, 119, -14.0F, -2.0F, -28.0F, 28, 3, 12, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 48, 120, -14.0F, -4.0F, -27.0F, 28, 2, 2, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 37, 116, -14.0F, -11.0F, -26.0F, 27, 7, 3, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 45, 107, -14.0F, -59.0F, -26.0F, 27, 10, 3, 0.0F, false));

		bolts6 = new RendererModel(this);
		bolts6.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_6.addChild(bolts6);
		bolts6.cubeList.add(new ModelBox(bolts6, 85, 81, -11.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts6.cubeList.add(new ModelBox(bolts6, 85, 81, -6.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts6.cubeList.add(new ModelBox(bolts6, 85, 81, -1.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts6.cubeList.add(new ModelBox(bolts6, 85, 81, 4.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts6.cubeList.add(new ModelBox(bolts6, 85, 81, 9.0F, -16.0F, -25.0F, 2, 2, 3, 0.0F, false));

		bolts_b6 = new RendererModel(this);
		bolts_b6.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_6.addChild(bolts_b6);
		bolts_b6.cubeList.add(new ModelBox(bolts_b6, 77, 82, -11.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b6.cubeList.add(new ModelBox(bolts_b6, 77, 82, -6.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b6.cubeList.add(new ModelBox(bolts_b6, 77, 82, -1.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b6.cubeList.add(new ModelBox(bolts_b6, 77, 82, 4.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));
		bolts_b6.cubeList.add(new ModelBox(bolts_b6, 77, 82, 9.0F, -45.0F, -25.0F, 2, 2, 3, 0.0F, false));

		bolts_c6 = new RendererModel(this);
		bolts_c6.setRotationPoint(0.0F, 0.0F, 0.0F);
		belly_6.addChild(bolts_c6);
		bolts_c6.cubeList.add(new ModelBox(bolts_c6, 78, 81, -9.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c6.cubeList.add(new ModelBox(bolts_c6, 78, 81, -4.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c6.cubeList.add(new ModelBox(bolts_c6, 78, 81, 2.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));
		bolts_c6.cubeList.add(new ModelBox(bolts_c6, 78, 81, 7.0F, -89.0F, -24.0F, 2, 2, 3, 0.0F, false));

		plane_6 = new RendererModel(this);
		plane_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(plane_6, 0.5236F, 0.0F, 0.0F);
		station_6.addChild(plane_6);
		plane_6.cubeList.add(new ModelBox(plane_6, 5, 42, -31.2F, -80.0F, -20.0F, 62, 4, 6, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 5, 42, -28.2F, -80.0F, -14.0F, 56, 4, 6, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 5, 42, -26.0F, -80.0F, -8.0F, 51, 4, 6, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 5, 42, -22.0F, -80.0F, -2.0F, 44, 4, 6, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 2, 82, -17.0F, -77.0F, -17.0F, 34, 8, 30, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 17, 92, -11.0F, -69.0F, -11.0F, 22, 4, 24, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 5, 42, -19.0F, -80.0F, 4.0F, 38, 4, 6, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 5, 42, -16.0F, -80.0F, 10.0F, 32, 4, 6, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 5, 42, -14.0F, -80.0F, 16.0F, 27, 4, 6, 0.0F, false));

		rib_6 = new RendererModel(this);
		rib_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_6, 0.0F, -0.5236F, 0.0F);
		station_6.addChild(rib_6);

		rib_tilt_6 = new RendererModel(this);
		rib_tilt_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_tilt_6, 0.4363F, 0.0F, 0.0F);
		rib_6.addChild(rib_tilt_6);
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 20, 76, -2.0F, -83.4F, -39.5F, 4, 8, 48, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 20, 75, -1.0F, -76.0F, -39.0F, 2, 6, 52, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 65, 94, -1.0F, -76.0F, -13.0F, 2, 26, 11, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 65, 94, -2.0F, -70.0F, -8.0F, 4, 14, 22, 0.0F, false));

		rib_deco_6 = new RendererModel(this);
		rib_deco_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_tilt_6.addChild(rib_deco_6);
		rib_deco_6.cubeList.add(new ModelBox(rib_deco_6, 80, 113, -3.0F, -84.5F, -40.0F, 6, 8, 8, 0.0F, false));
		rib_deco_6.cubeList.add(new ModelBox(rib_deco_6, 65, 94, -4.0F, -86.0F, 9.0F, 8, 8, 8, 0.0F, false));
		rib_deco_6.cubeList.add(new ModelBox(rib_deco_6, 65, 94, -3.0F, -84.4F, 8.0F, 6, 7, 8, 0.0F, false));

		lowerrib_tilt_7 = new RendererModel(this);
		lowerrib_tilt_7.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(lowerrib_tilt_7, 0.4363F, 0.0F, 0.0F);
		rib_6.addChild(lowerrib_tilt_7);
		lowerrib_tilt_7.cubeList.add(new ModelBox(lowerrib_tilt_7, 65, 94, -2.0F, -70.0F, -8.0F, 4, 14, 22, 0.0F, false));

		base_fin_6 = new RendererModel(this);
		base_fin_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(base_fin_6, 0.0F, -0.5236F, 0.0F);
		station_6.addChild(base_fin_6);
		base_fin_6.cubeList.add(new ModelBox(base_fin_6, 91, 90, -1.0F, -48.2F, -23.0F, 2, 28, 4, 0.0F, false));
		base_fin_6.cubeList.add(new ModelBox(base_fin_6, 60, 92, -2.0F, -94.2F, -27.0F, 4, 24, 12, 0.0F, false));
		base_fin_6.cubeList.add(new ModelBox(base_fin_6, 51, 92, -2.0F, -118.2F, -19.0F, 4, 24, 4, 0.0F, false));
		base_fin_6.cubeList.add(new ModelBox(base_fin_6, 65, 114, -2.4F, -13.2F, -32.0F, 5, 14, 6, 0.0F, false));
		base_fin_6.cubeList.add(new ModelBox(base_fin_6, 63, 98, -1.4F, -21.2F, -28.0F, 3, 8, 8, 0.0F, false));

		clawfoot_6 = new RendererModel(this);
		clawfoot_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(clawfoot_6, 0.0F, -0.5236F, 0.0F);
		station_6.addChild(clawfoot_6);

		leg_6 = new RendererModel(this);
		leg_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_6.addChild(leg_6);

		ball_6 = new RendererModel(this);
		ball_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_6.addChild(ball_6);

		controls = new RendererModel(this);
		controls.setRotationPoint(0.0F, 24.0F, 0.0F);

		side1 = new RendererModel(this);
		side1.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(side1);

		station_offset_1 = new RendererModel(this);
		station_offset_1.setRotationPoint(0.0F, -14.0F, 12.4F);
		setRotationAngle(station_offset_1, -0.2618F, 0.0F, 0.0F);
		side1.addChild(station_offset_1);
		station_offset_1.cubeList.add(new ModelBox(station_offset_1, 68, 152, -4.0F, -33.0F, -81.0F, 8, 8, 12, 0.0F, false));

		xyz_increment_rotate_z = new RendererModel(this);
		xyz_increment_rotate_z.setRotationPoint(0.0F, -29.0F, -81.4F);
		station_offset_1.addChild(xyz_increment_rotate_z);

		spokes = new RendererModel(this);
		spokes.setRotationPoint(0.2F, -0.1F, -1.7F);
		xyz_increment_rotate_z.addChild(spokes);
		spokes.cubeList.add(new ModelBox(spokes, 182, 152, -1.2F, -9.6F, -3.0F, 2, 8, 2, 0.0F, false));
		spokes.cubeList.add(new ModelBox(spokes, 197, 90, -1.2F, -14.6F, -3.0F, 2, 4, 2, 0.0F, false));
		spokes.cubeList.add(new ModelBox(spokes, 206, 90, -1.2F, 10.8F, -3.0F, 2, 4, 2, 0.0F, false));
		spokes.cubeList.add(new ModelBox(spokes, 203, 88, -0.7F, -11.0F, -2.5F, 1, 2, 1, 0.0F, false));
		spokes.cubeList.add(new ModelBox(spokes, 206, 80, -0.7F, 9.0F, -2.5F, 1, 2, 1, 0.0F, false));
		spokes.cubeList.add(new ModelBox(spokes, 182, 152, -9.6F, -1.0F, -3.0F, 8, 2, 2, 0.0F, false));
		spokes.cubeList.add(new ModelBox(spokes, 182, 152, 1.4F, -1.0F, -3.0F, 8, 2, 2, 0.0F, false));
		spokes.cubeList.add(new ModelBox(spokes, 223, 59, 11.2F, -1.0F, -3.0F, 4, 2, 2, 0.0F, false));
		spokes.cubeList.add(new ModelBox(spokes, 191, 80, -15.4F, -1.0F, -3.0F, 4, 2, 2, 0.0F, false));
		spokes.cubeList.add(new ModelBox(spokes, 216, 95, 9.4F, -0.5F, -2.4F, 2, 1, 1, 0.0F, false));
		spokes.cubeList.add(new ModelBox(spokes, 208, 94, -11.6F, -0.5F, -2.5F, 2, 1, 1, 0.0F, false));
		spokes.cubeList.add(new ModelBox(spokes, 182, 152, -3.6F, -8.5F, -4.0F, 7, 2, 4, 0.0F, false));
		spokes.cubeList.add(new ModelBox(spokes, 182, 152, -3.6F, 6.4F, -4.0F, 7, 2, 4, 0.0F, false));
		spokes.cubeList.add(new ModelBox(spokes, 182, 152, 6.3F, -3.6F, -4.0F, 2, 7, 4, 0.0F, false));
		spokes.cubeList.add(new ModelBox(spokes, 182, 152, -8.6F, -3.6F, -4.0F, 2, 7, 4, 0.0F, false));
		spokes.cubeList.add(new ModelBox(spokes, 182, 152, -1.2F, 1.6F, -3.0F, 2, 8, 2, 0.0F, false));

		spokes2 = new RendererModel(this);
		spokes2.setRotationPoint(0.2F, -0.1F, -1.7F);
		setRotationAngle(spokes2, 0.0F, 0.0F, -0.7854F);
		xyz_increment_rotate_z.addChild(spokes2);
		spokes2.cubeList.add(new ModelBox(spokes2, 182, 152, -1.2F, -9.6F, -3.0F, 2, 8, 2, 0.0F, false));
		spokes2.cubeList.add(new ModelBox(spokes2, 208, 99, -1.2F, -14.6F, -3.0F, 2, 4, 2, 0.0F, false));
		spokes2.cubeList.add(new ModelBox(spokes2, 204, 90, -1.2F, 10.8F, -3.0F, 2, 4, 2, 0.0F, false));
		spokes2.cubeList.add(new ModelBox(spokes2, 208, 95, -0.7F, -11.0F, -2.5F, 1, 2, 1, 0.0F, false));
		spokes2.cubeList.add(new ModelBox(spokes2, 207, 80, -0.7F, 9.0F, -2.5F, 1, 2, 1, 0.0F, false));
		spokes2.cubeList.add(new ModelBox(spokes2, 182, 152, -9.6F, -1.0F, -3.0F, 8, 2, 2, 0.0F, false));
		spokes2.cubeList.add(new ModelBox(spokes2, 182, 152, 1.4F, -1.0F, -3.0F, 8, 2, 2, 0.0F, false));
		spokes2.cubeList.add(new ModelBox(spokes2, 200, 99, 11.2F, -1.0F, -3.0F, 4, 2, 2, 0.0F, false));
		spokes2.cubeList.add(new ModelBox(spokes2, 209, 100, -15.4F, -1.0F, -3.0F, 4, 2, 2, 0.0F, false));
		spokes2.cubeList.add(new ModelBox(spokes2, 205, 89, 9.4F, -0.5F, -2.4F, 2, 1, 1, 0.0F, false));
		spokes2.cubeList.add(new ModelBox(spokes2, 205, 91, -11.6F, -0.5F, -2.5F, 2, 1, 1, 0.0F, false));
		spokes2.cubeList.add(new ModelBox(spokes2, 182, 152, -3.5F, -8.6F, -4.0F, 7, 2, 4, 0.0F, false));
		spokes2.cubeList.add(new ModelBox(spokes2, 182, 152, -3.5F, 6.3F, -4.0F, 7, 2, 4, 0.0F, false));
		spokes2.cubeList.add(new ModelBox(spokes2, 182, 152, 6.4F, -3.6F, -4.0F, 2, 7, 4, 0.0F, false));
		spokes2.cubeList.add(new ModelBox(spokes2, 182, 152, -8.5F, -3.6F, -4.0F, 2, 7, 4, 0.0F, false));
		spokes2.cubeList.add(new ModelBox(spokes2, 182, 152, -1.2F, 1.6F, -3.0F, 2, 8, 2, 0.0F, false));

		center = new RendererModel(this);
		center.setRotationPoint(0.0F, 29.0F, 82.6F);
		xyz_increment_rotate_z.addChild(center);
		center.cubeList.add(new ModelBox(center, 198, 84, -1.4F, -30.6F, -88.0F, 3, 3, 10, 0.0F, false));

		station_tilt_1 = new RendererModel(this);
		station_tilt_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_tilt_1, -1.0472F, 0.0F, 0.0F);
		side1.addChild(station_tilt_1);

		coorddial_x = new RendererModel(this);
		coorddial_x.setRotationPoint(-11.05F, -17.1F, -79.0F);
		setRotationAngle(coorddial_x, 0.0F, 0.0F, 0.7854F);
		station_tilt_1.addChild(coorddial_x);
		coorddial_x.cubeList.add(new ModelBox(coorddial_x, 182, 152, 4.05F, -0.6F, -1.4F, 7, 7, 4, 0.0F, false));
		coorddial_x.cubeList.add(new ModelBox(coorddial_x, 209, 64, 5.15F, 0.5F, -2.4F, 5, 5, 4, 0.0F, false));
		coorddial_x.cubeList.add(new ModelBox(coorddial_x, 138, 157, 5.65F, 1.1F, -3.4F, 4, 4, 4, 0.0F, false));
		coorddial_x.cubeList.add(new ModelBox(coorddial_x, 16, 158, 6.25F, 1.6F, -3.6F, 3, 3, 4, 0.0F, false));

		coorddial_y = new RendererModel(this);
		coorddial_y.setRotationPoint(-11.05F, -17.1F, -79.0F);
		setRotationAngle(coorddial_y, 0.0F, 0.0F, 0.7854F);
		station_tilt_1.addChild(coorddial_y);
		coorddial_y.cubeList.add(new ModelBox(coorddial_y, 182, 152, 7.45F, -8.6F, -1.4F, 7, 7, 4, 0.0F, false));
		coorddial_y.cubeList.add(new ModelBox(coorddial_y, 209, 69, 8.55F, -7.5F, -2.4F, 5, 5, 4, 0.0F, false));
		coorddial_y.cubeList.add(new ModelBox(coorddial_y, 143, 158, 9.05F, -6.9F, -3.4F, 4, 4, 4, 0.0F, false));
		coorddial_y.cubeList.add(new ModelBox(coorddial_y, 31, 156, 9.65F, -6.4F, -3.6F, 3, 3, 4, 0.0F, false));

		coorddial_z = new RendererModel(this);
		coorddial_z.setRotationPoint(-11.05F, -17.1F, -79.0F);
		setRotationAngle(coorddial_z, 0.0F, 0.0F, 0.7854F);
		station_tilt_1.addChild(coorddial_z);
		coorddial_z.cubeList.add(new ModelBox(coorddial_z, 182, 152, 15.45F, -12.1F, -1.4F, 7, 7, 4, 0.0F, false));
		coorddial_z.cubeList.add(new ModelBox(coorddial_z, 209, 64, 16.55F, -11.0F, -2.4F, 5, 5, 4, 0.0F, false));
		coorddial_z.cubeList.add(new ModelBox(coorddial_z, 137, 152, 17.05F, -10.4F, -3.4F, 4, 4, 4, 0.0F, false));
		coorddial_z.cubeList.add(new ModelBox(coorddial_z, 32, 146, 17.65F, -9.9F, -3.6F, 3, 3, 4, 0.0F, false));

		fancy_nameplate = new RendererModel(this);
		fancy_nameplate.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_tilt_1.addChild(fancy_nameplate);
		fancy_nameplate.cubeList.add(new ModelBox(fancy_nameplate, 182, 152, -3.7F, -5.9F, -80.5F, 8, 4, 4, 0.0F, false));
		fancy_nameplate.cubeList.add(new ModelBox(fancy_nameplate, 182, 152, -4.7F, -4.9F, -80.5F, 1, 2, 4, 0.0F, false));
		fancy_nameplate.cubeList.add(new ModelBox(fancy_nameplate, 182, 152, 4.3F, -4.9F, -80.5F, 1, 2, 4, 0.0F, false));
		fancy_nameplate.cubeList.add(new ModelBox(fancy_nameplate, 190, 56, -3.2F, -5.3F, -80.9F, 7, 3, 4, 0.0F, false));

		dummy_toggle_a1 = new RendererModel(this);
		dummy_toggle_a1.setRotationPoint(-12.62F, 7.46F, -47.1F);
		station_tilt_1.addChild(dummy_toggle_a1);
		dummy_toggle_a1.cubeList.add(new ModelBox(dummy_toggle_a1, 203, 156, -9.08F, -0.36F, -33.4F, 4, 4, 4, 0.0F, false));

		tilt = new RendererModel(this);
		tilt.setRotationPoint(-7.08F, 1.74F, -32.4F);
		setRotationAngle(tilt, 0.3491F, 0.0F, 0.0F);
		dummy_toggle_a1.addChild(tilt);
		tilt.cubeList.add(new ModelBox(tilt, 207, 86, -1.0F, -2.0F, -1.0F, 2, 3, 4, 0.0F, false));

		tilt2 = new RendererModel(this);
		tilt2.setRotationPoint(-7.08F, 1.74F, -32.4F);
		setRotationAngle(tilt2, -0.3491F, 0.0F, 0.0F);
		dummy_toggle_a1.addChild(tilt2);
		tilt2.cubeList.add(new ModelBox(tilt2, 207, 86, -1.0F, -1.3F, -1.1F, 2, 3, 4, 0.0F, false));

		dummy_toggle_a2 = new RendererModel(this);
		dummy_toggle_a2.setRotationPoint(-6.82F, 5.46F, -47.1F);
		station_tilt_1.addChild(dummy_toggle_a2);
		dummy_toggle_a2.cubeList.add(new ModelBox(dummy_toggle_a2, 194, 162, -9.88F, 1.64F, -33.4F, 4, 4, 4, 0.0F, false));

		tilt3 = new RendererModel(this);
		tilt3.setRotationPoint(-7.88F, 3.74F, -32.4F);
		setRotationAngle(tilt3, 0.3491F, 0.0F, 0.0F);
		dummy_toggle_a2.addChild(tilt3);
		tilt3.cubeList.add(new ModelBox(tilt3, 204, 96, -1.0F, -2.0F, -1.0F, 2, 3, 4, 0.0F, false));

		tilt4 = new RendererModel(this);
		tilt4.setRotationPoint(-7.88F, 3.74F, -32.4F);
		setRotationAngle(tilt4, -0.3491F, 0.0F, 0.0F);
		dummy_toggle_a2.addChild(tilt4);
		tilt4.cubeList.add(new ModelBox(tilt4, 204, 96, -1.0F, -1.3F, -1.1F, 2, 3, 4, 0.0F, false));

		dummy_toggle_a3 = new RendererModel(this);
		dummy_toggle_a3.setRotationPoint(-11.02F, 8.46F, -47.1F);
		station_tilt_1.addChild(dummy_toggle_a3);
		dummy_toggle_a3.cubeList.add(new ModelBox(dummy_toggle_a3, 195, 160, -10.68F, 3.64F, -33.4F, 4, 4, 4, 0.0F, false));

		tilt5 = new RendererModel(this);
		tilt5.setRotationPoint(-8.68F, 5.74F, -32.4F);
		setRotationAngle(tilt5, 0.3491F, 0.0F, 0.0F);
		dummy_toggle_a3.addChild(tilt5);
		tilt5.cubeList.add(new ModelBox(tilt5, 195, 85, -1.0F, -2.0F, -1.0F, 2, 3, 4, 0.0F, false));

		tilt6 = new RendererModel(this);
		tilt6.setRotationPoint(-8.68F, 5.74F, -32.4F);
		setRotationAngle(tilt6, -0.3491F, 0.0F, 0.0F);
		dummy_toggle_a3.addChild(tilt6);
		tilt6.cubeList.add(new ModelBox(tilt6, 195, 85, -1.0F, -1.3F, -1.1F, 2, 3, 4, 0.0F, false));

		dummy_toggle_a4 = new RendererModel(this);
		dummy_toggle_a4.setRotationPoint(-6.02F, 8.46F, -47.1F);
		station_tilt_1.addChild(dummy_toggle_a4);
		dummy_toggle_a4.cubeList.add(new ModelBox(dummy_toggle_a4, 200, 161, -10.68F, 3.64F, -33.4F, 4, 4, 4, 0.0F, false));

		tilt7 = new RendererModel(this);
		tilt7.setRotationPoint(-8.68F, 5.74F, -32.4F);
		setRotationAngle(tilt7, 0.3491F, 0.0F, 0.0F);
		dummy_toggle_a4.addChild(tilt7);
		tilt7.cubeList.add(new ModelBox(tilt7, 211, 96, -1.0F, -2.0F, -1.0F, 2, 3, 4, 0.0F, false));

		tilt8 = new RendererModel(this);
		tilt8.setRotationPoint(-8.68F, 5.74F, -32.4F);
		setRotationAngle(tilt8, -0.3491F, 0.0F, 0.0F);
		dummy_toggle_a4.addChild(tilt8);
		tilt8.cubeList.add(new ModelBox(tilt8, 211, 96, -1.0F, -1.3F, -1.1F, 2, 3, 4, 0.0F, false));

		dummy_toggle_a5 = new RendererModel(this);
		dummy_toggle_a5.setRotationPoint(22.38F, 7.46F, -47.1F);
		station_tilt_1.addChild(dummy_toggle_a5);
		dummy_toggle_a5.cubeList.add(new ModelBox(dummy_toggle_a5, 195, 153, -9.08F, -0.36F, -33.4F, 4, 4, 6, 0.0F, false));

		tilt9 = new RendererModel(this);
		tilt9.setRotationPoint(-7.08F, 1.74F, -32.4F);
		setRotationAngle(tilt9, 0.3491F, 0.0F, 0.0F);
		dummy_toggle_a5.addChild(tilt9);
		tilt9.cubeList.add(new ModelBox(tilt9, 197, 105, -1.0F, -2.0F, -1.0F, 2, 3, 4, 0.0F, false));

		tilt10 = new RendererModel(this);
		tilt10.setRotationPoint(-7.08F, 1.74F, -32.4F);
		setRotationAngle(tilt10, -0.3491F, 0.0F, 0.0F);
		dummy_toggle_a5.addChild(tilt10);
		tilt10.cubeList.add(new ModelBox(tilt10, 197, 105, -1.0F, -1.3F, -1.1F, 2, 3, 4, 0.0F, false));

		dummy_toggle_a6 = new RendererModel(this);
		dummy_toggle_a6.setRotationPoint(27.38F, 7.46F, -47.1F);
		station_tilt_1.addChild(dummy_toggle_a6);
		dummy_toggle_a6.cubeList.add(new ModelBox(dummy_toggle_a6, 194, 157, -9.08F, -0.36F, -33.4F, 4, 4, 4, 0.0F, false));

		tilt11 = new RendererModel(this);
		tilt11.setRotationPoint(-7.08F, 1.74F, -32.4F);
		setRotationAngle(tilt11, 0.3491F, 0.0F, 0.0F);
		dummy_toggle_a6.addChild(tilt11);
		tilt11.cubeList.add(new ModelBox(tilt11, 213, 87, -1.0F, -2.0F, -1.0F, 2, 3, 4, 0.0F, false));

		tilt12 = new RendererModel(this);
		tilt12.setRotationPoint(-7.08F, 1.74F, -32.4F);
		setRotationAngle(tilt12, -0.3491F, 0.0F, 0.0F);
		dummy_toggle_a6.addChild(tilt12);
		tilt12.cubeList.add(new ModelBox(tilt12, 213, 87, -1.0F, -1.3F, -1.1F, 2, 3, 4, 0.0F, false));

		dummy_toggle_a7 = new RendererModel(this);
		dummy_toggle_a7.setRotationPoint(22.38F, 12.46F, -47.1F);
		station_tilt_1.addChild(dummy_toggle_a7);
		dummy_toggle_a7.cubeList.add(new ModelBox(dummy_toggle_a7, 198, 157, -9.08F, -0.36F, -33.4F, 4, 4, 4, 0.0F, false));

		tilt13 = new RendererModel(this);
		tilt13.setRotationPoint(-7.08F, 1.74F, -32.4F);
		setRotationAngle(tilt13, 0.3491F, 0.0F, 0.0F);
		dummy_toggle_a7.addChild(tilt13);
		tilt13.cubeList.add(new ModelBox(tilt13, 198, 102, -1.0F, -2.0F, -1.0F, 2, 3, 4, 0.0F, false));

		tilt14 = new RendererModel(this);
		tilt14.setRotationPoint(-7.08F, 1.74F, -32.4F);
		setRotationAngle(tilt14, -0.3491F, 0.0F, 0.0F);
		dummy_toggle_a7.addChild(tilt14);
		tilt14.cubeList.add(new ModelBox(tilt14, 198, 102, -1.0F, -1.3F, -1.1F, 2, 3, 4, 0.0F, false));

		dummy_toggle_a8 = new RendererModel(this);
		dummy_toggle_a8.setRotationPoint(27.38F, 12.46F, -47.1F);
		station_tilt_1.addChild(dummy_toggle_a8);
		dummy_toggle_a8.cubeList.add(new ModelBox(dummy_toggle_a8, 196, 158, -9.08F, -0.36F, -33.4F, 4, 4, 4, 0.0F, false));

		tilt15 = new RendererModel(this);
		tilt15.setRotationPoint(-7.08F, 1.74F, -32.4F);
		setRotationAngle(tilt15, 0.3491F, 0.0F, 0.0F);
		dummy_toggle_a8.addChild(tilt15);
		tilt15.cubeList.add(new ModelBox(tilt15, 190, 93, -1.0F, -2.0F, -1.0F, 2, 3, 4, 0.0F, false));

		tilt16 = new RendererModel(this);
		tilt16.setRotationPoint(-7.08F, 1.74F, -32.4F);
		setRotationAngle(tilt16, -0.3491F, 0.0F, 0.0F);
		dummy_toggle_a8.addChild(tilt16);
		tilt16.cubeList.add(new ModelBox(tilt16, 190, 93, -1.0F, -1.3F, -1.1F, 2, 3, 4, 0.0F, false));

		dummy_button_a1 = new RendererModel(this);
		dummy_button_a1.setRotationPoint(7.38F, 0.46F, -47.1F);
		station_tilt_1.addChild(dummy_button_a1);
		dummy_button_a1.cubeList.add(new ModelBox(dummy_button_a1, 182, 152, -25.08F, -0.36F, -33.4F, 12, 4, 4, 0.0F, false));
		dummy_button_a1.cubeList.add(new ModelBox(dummy_button_a1, 132, 153, -20.58F, 0.14F, -34.1F, 3, 3, 4, 0.0F, false));
		dummy_button_a1.cubeList.add(new ModelBox(dummy_button_a1, 133, 153, -24.38F, 0.14F, -34.4F, 3, 3, 4, 0.0F, false));
		dummy_button_a1.cubeList.add(new ModelBox(dummy_button_a1, 134, 153, -16.88F, 0.14F, -34.1F, 3, 3, 4, 0.0F, false));

		dummy_button_a2 = new RendererModel(this);
		dummy_button_a2.setRotationPoint(7.38F, 0.46F, -47.1F);
		station_tilt_1.addChild(dummy_button_a2);
		dummy_button_a2.cubeList.add(new ModelBox(dummy_button_a2, 182, 152, -13.08F, -0.36F, -33.4F, 12, 4, 4, 0.0F, false));
		dummy_button_a2.cubeList.add(new ModelBox(dummy_button_a2, 146, 155, -8.58F, 0.14F, -34.5F, 3, 3, 4, 0.0F, false));
		dummy_button_a2.cubeList.add(new ModelBox(dummy_button_a2, 133, 158, -12.38F, 0.14F, -34.1F, 3, 3, 4, 0.0F, false));
		dummy_button_a2.cubeList.add(new ModelBox(dummy_button_a2, 147, 160, -4.88F, 0.14F, -34.1F, 3, 3, 4, 0.0F, false));

		dummy_button_a3 = new RendererModel(this);
		dummy_button_a3.setRotationPoint(7.38F, 0.46F, -47.1F);
		station_tilt_1.addChild(dummy_button_a3);
		dummy_button_a3.cubeList.add(new ModelBox(dummy_button_a3, 182, 152, -1.08F, -0.36F, -33.4F, 12, 4, 4, 0.0F, false));
		dummy_button_a3.cubeList.add(new ModelBox(dummy_button_a3, 141, 154, 3.42F, 0.14F, -34.1F, 3, 3, 4, 0.0F, false));
		dummy_button_a3.cubeList.add(new ModelBox(dummy_button_a3, 146, 160, -0.38F, 0.14F, -34.1F, 3, 3, 4, 0.0F, false));
		dummy_button_a3.cubeList.add(new ModelBox(dummy_button_a3, 137, 159, 7.12F, 0.14F, -34.5F, 3, 3, 4, 0.0F, false));

		side2 = new RendererModel(this);
		side2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(side2, 0.0F, -1.0472F, 0.0F);
		controls.addChild(side2);

		station_tilt_2 = new RendererModel(this);
		station_tilt_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_tilt_2, -1.0472F, 0.0F, 0.0F);
		side2.addChild(station_tilt_2);
		station_tilt_2.cubeList.add(new ModelBox(station_tilt_2, 182, 152, -4.0F, 2.0F, -81.0F, 8, 12, 4, 0.0F, false));
		station_tilt_2.cubeList.add(new ModelBox(station_tilt_2, 182, 152, -19.0F, 2.0F, -81.0F, 12, 12, 4, 0.0F, false));
		station_tilt_2.cubeList.add(new ModelBox(station_tilt_2, 182, 152, 7.0F, 2.0F, -81.0F, 12, 12, 4, 0.0F, false));

		facing_dial = new RendererModel(this);
		facing_dial.setRotationPoint(-13.0F, 8.0F, -80.0F);
		setRotationAngle(facing_dial, 0.0F, 0.0F, 0.7854F);
		station_tilt_2.addChild(facing_dial);
		facing_dial.cubeList.add(new ModelBox(facing_dial, 73, 157, -5.0F, -5.0F, -2.0F, 10, 10, 4, 0.0F, false));
		facing_dial.cubeList.add(new ModelBox(facing_dial, 21, 159, -3.0F, -3.0F, -2.6F, 6, 6, 4, 0.0F, false));
		facing_dial.cubeList.add(new ModelBox(facing_dial, 182, 152, -1.0F, -1.0F, -3.0F, 2, 2, 4, 0.0F, false));
		facing_dial.cubeList.add(new ModelBox(facing_dial, 191, 103, -4.0F, 3.0F, -3.0F, 7, 1, 4, 0.0F, false));
		facing_dial.cubeList.add(new ModelBox(facing_dial, 195, 90, -3.0F, -4.0F, -3.0F, 7, 1, 4, 0.0F, false));
		facing_dial.cubeList.add(new ModelBox(facing_dial, 214, 92, 3.0F, -3.0F, -3.0F, 1, 7, 4, 0.0F, false));
		facing_dial.cubeList.add(new ModelBox(facing_dial, 198, 91, -4.0F, -4.0F, -3.0F, 1, 7, 4, 0.0F, false));

		landing_type = new RendererModel(this);
		landing_type.setRotationPoint(0.6F, 0.0F, 0.7F);
		station_tilt_2.addChild(landing_type);
		landing_type.cubeList.add(new ModelBox(landing_type, 133, 152, 8.0F, 4.0F, -82.3F, 9, 8, 4, 0.0F, false));

		slider_bar = new RendererModel(this);
		slider_bar.setRotationPoint(0.0F, 0.0F, 0.0F);
		landing_type.addChild(slider_bar);
		slider_bar.cubeList.add(new ModelBox(slider_bar, 73, 160, 9.0F, 6.0F, -84.4F, 7, 1, 4, 0.0F, false));

		slider_plate = new RendererModel(this);
		slider_plate.setRotationPoint(0.0F, 0.0F, 0.0F);
		landing_type.addChild(slider_plate);
		slider_plate.cubeList.add(new ModelBox(slider_plate, 183, 82, 8.5F, 10.0F, -83.3F, 8, 1, 4, 0.0F, false));
		slider_plate.cubeList.add(new ModelBox(slider_plate, 199, 88, 8.5F, 5.0F, -83.3F, 8, 1, 4, 0.0F, false));
		slider_plate.cubeList.add(new ModelBox(slider_plate, 184, 89, 8.5F, 6.0F, -83.3F, 1, 4, 4, 0.0F, false));
		slider_plate.cubeList.add(new ModelBox(slider_plate, 216, 80, 15.5F, 6.0F, -83.3F, 1, 4, 4, 0.0F, false));
		slider_plate.cubeList.add(new ModelBox(slider_plate, 200, 91, 10.5F, 6.0F, -83.3F, 4, 4, 4, 0.0F, false));

		up = new RendererModel(this);
		up.setRotationPoint(-13.0F, 8.0F, -80.0F);
		setRotationAngle(up, 0.0F, 0.0F, 0.7854F);
		landing_type.addChild(up);
		up.cubeList.add(new ModelBox(up, 203, 77, 13.0F, -23.0F, -2.0F, 5, 5, 4, 0.0F, false));

		up2 = new RendererModel(this);
		up2.setRotationPoint(-13.0F, 8.0F, -80.0F);
		setRotationAngle(up2, 0.0F, 0.0F, 0.7854F);
		landing_type.addChild(up2);
		up2.cubeList.add(new ModelBox(up2, 205, 95, 18.0F, -18.0F, -2.0F, 5, 5, 4, 0.0F, false));

		dummy_button_b1 = new RendererModel(this);
		dummy_button_b1.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_tilt_2.addChild(dummy_button_b1);
		dummy_button_b1.cubeList.add(new ModelBox(dummy_button_b1, 188, 12, -3.5F, 2.7F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_b1.cubeList.add(new ModelBox(dummy_button_b1, 188, 12, -1.0F, 2.7F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_b1.cubeList.add(new ModelBox(dummy_button_b1, 188, 12, 1.5F, 2.7F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_b1.cubeList.add(new ModelBox(dummy_button_b1, 188, 12, 1.5F, 5.5F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_b1.cubeList.add(new ModelBox(dummy_button_b1, 188, 12, -3.5F, 5.5F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_b1.cubeList.add(new ModelBox(dummy_button_b1, 188, 12, -1.0F, 5.5F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_b1.cubeList.add(new ModelBox(dummy_button_b1, 188, 12, 1.5F, 8.4F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_b1.cubeList.add(new ModelBox(dummy_button_b1, 188, 12, -3.5F, 8.4F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_b1.cubeList.add(new ModelBox(dummy_button_b1, 188, 12, -1.0F, 8.4F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_b1.cubeList.add(new ModelBox(dummy_button_b1, 188, 12, 1.5F, 11.4F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_b1.cubeList.add(new ModelBox(dummy_button_b1, 188, 12, -3.5F, 11.4F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_b1.cubeList.add(new ModelBox(dummy_button_b1, 188, 12, -1.0F, 11.4F, -81.3F, 2, 2, 4, 0.0F, false));

		randomizer = new RendererModel(this);
		randomizer.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_tilt_2.addChild(randomizer);
		randomizer.cubeList.add(new ModelBox(randomizer, 133, 155, -6.6F, -15.0F, -80.4F, 12, 9, 5, 0.0F, false));
		randomizer.cubeList.add(new ModelBox(randomizer, 182, 152, -7.6F, -15.0F, -81.4F, 1, 9, 5, 0.0F, false));
		randomizer.cubeList.add(new ModelBox(randomizer, 182, 152, 5.4F, -15.0F, -81.4F, 1, 9, 5, 0.0F, false));
		randomizer.cubeList.add(new ModelBox(randomizer, 182, 152, -7.6F, -6.0F, -81.4F, 14, 1, 5, 0.0F, false));
		randomizer.cubeList.add(new ModelBox(randomizer, 182, 152, -7.6F, -16.0F, -81.4F, 14, 1, 5, 0.0F, false));
		randomizer.cubeList.add(new ModelBox(randomizer, 205, 80, -2.5F, -5.7F, -82.1F, 4, 1, 6, 0.0F, false));
		randomizer.cubeList.add(new ModelBox(randomizer, 202, 92, -2.5F, -16.7F, -82.1F, 4, 1, 6, 0.0F, false));

		rotate_y = new RendererModel(this);
		rotate_y.setRotationPoint(-0.5F, -6.7F, -81.1F);
		randomizer.addChild(rotate_y);
		rotate_y.cubeList.add(new ModelBox(rotate_y, 181, 205, -4.0F, -8.0F, -4.0F, 8, 8, 8, 0.0F, false));
		rotate_y.cubeList.add(new ModelBox(rotate_y, 11, 225, -4.4F, -2.7F, -4.1F, 2, 3, 3, 0.0F, false));
		rotate_y.cubeList.add(new ModelBox(rotate_y, 4, 222, -1.4F, -6.3F, -4.6F, 5, 3, 3, 0.0F, false));
		rotate_y.cubeList.add(new ModelBox(rotate_y, 9, 221, -3.1F, -2.7F, 1.3F, 2, 3, 3, 0.0F, false));
		rotate_y.cubeList.add(new ModelBox(rotate_y, 12, 225, -4.3F, -6.2F, -2.1F, 2, 5, 5, 0.0F, false));
		rotate_y.cubeList.add(new ModelBox(rotate_y, 18, 219, -4.2F, -8.2F, 0.3F, 2, 3, 4, 0.0F, false));
		rotate_y.cubeList.add(new ModelBox(rotate_y, 13, 220, 2.2F, -4.8F, 0.3F, 2, 3, 4, 0.0F, false));
		rotate_y.cubeList.add(new ModelBox(rotate_y, 16, 219, 2.4F, -8.2F, -0.7F, 2, 4, 3, 0.0F, false));
		rotate_y.cubeList.add(new ModelBox(rotate_y, 8, 226, 2.2F, -2.8F, -4.2F, 2, 3, 4, 0.0F, false));
		rotate_y.cubeList.add(new ModelBox(rotate_y, 16, 220, 3.2F, -6.8F, -4.2F, 1, 1, 1, 0.0F, false));
		rotate_y.cubeList.add(new ModelBox(rotate_y, 202, 83, -0.5F, -8.4F, -5.0F, 1, 9, 10, 0.0F, false));
		rotate_y.cubeList.add(new ModelBox(rotate_y, 206, 83, -0.5F, -9.4F, -0.5F, 1, 11, 1, 0.0F, false));

		pipes_2 = new RendererModel(this);
		pipes_2.setRotationPoint(9.0F, -16.0F, -1.75F);
		station_tilt_2.addChild(pipes_2);
		pipes_2.cubeList.add(new ModelBox(pipes_2, 209, 92, -19.7F, 14.1F, -79.25F, 1, 4, 2, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 209, 92, -15.2F, 10.6F, -79.25F, 1, 4, 2, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 209, 92, 7.55F, 13.1F, -79.25F, 1, 5, 2, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 209, 92, 0.55F, 7.85F, -79.25F, 1, 4, 2, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 209, 92, -15.2F, -4.65F, -79.25F, 1, 4, 2, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 228, 17, -20.2F, 20.35F, -79.75F, 2, 2, 2, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 228, 17, -20.2F, 13.35F, -79.75F, 2, 2, 2, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 228, 17, -15.7F, 13.35F, -79.75F, 2, 2, 2, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 228, 17, 0.05F, 6.85F, -79.75F, 2, 2, 2, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 228, 17, -15.7F, -1.65F, -79.75F, 2, 2, 2, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 228, 17, 0.05F, 11.35F, -79.75F, 2, 2, 2, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 228, 17, 7.05F, 11.35F, -79.75F, 2, 2, 2, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 228, 17, -20.2F, 26.6F, -79.75F, 2, 2, 2, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 209, 92, -23.7F, 27.1F, -79.25F, 4, 1, 2, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 209, 92, 1.55F, 11.85F, -79.25F, 6, 1, 1, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 209, 92, -2.7F, 7.35F, -79.25F, 4, 1, 1, 0.0F, false));
		pipes_2.cubeList.add(new ModelBox(pipes_2, 209, 92, -18.7F, 13.85F, -79.25F, 4, 1, 2, 0.0F, false));

		side3 = new RendererModel(this);
		side3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(side3, 0.0F, -2.0944F, 0.0F);
		controls.addChild(side3);

		dummy_barometer = new RendererModel(this);
		dummy_barometer.setRotationPoint(0.0F, 0.0F, 0.0F);
		side3.addChild(dummy_barometer);
		dummy_barometer.cubeList.add(new ModelBox(dummy_barometer, 24, 153, -5.0F, -90.0F, -32.0F, 3, 16, 3, 0.0F, false));
		dummy_barometer.cubeList.add(new ModelBox(dummy_barometer, 206, 216, 2.5F, -78.0F, -32.0F, 2, 4, 2, 0.0F, false));
		dummy_barometer.cubeList.add(new ModelBox(dummy_barometer, 203, 201, 1.5F, -80.0F, -33.0F, 4, 2, 4, 0.0F, false));
		dummy_barometer.cubeList.add(new ModelBox(dummy_barometer, 205, 212, 5.5F, -79.5F, -31.0F, 2, 1, 1, 0.0F, false));
		dummy_barometer.cubeList.add(new ModelBox(dummy_barometer, 208, 207, 6.5F, -81.5F, -31.0F, 1, 2, 1, 0.0F, false));
		dummy_barometer.cubeList.add(new ModelBox(dummy_barometer, 25, 164, 6.5F, -83.5F, -31.0F, 1, 2, 1, 0.0F, false));
		dummy_barometer.cubeList.add(new ModelBox(dummy_barometer, 33, 161, 3.5F, -87.5F, -31.0F, 1, 4, 1, 0.0F, false));
		dummy_barometer.cubeList.add(new ModelBox(dummy_barometer, 23, 158, 3.0F, -87.1F, -31.5F, 2, 2, 2, 0.0F, false));
		dummy_barometer.cubeList.add(new ModelBox(dummy_barometer, 16, 167, 3.5F, -83.5F, -31.0F, 3, 1, 1, 0.0F, false));
		dummy_barometer.cubeList.add(new ModelBox(dummy_barometer, 22, 157, 1.5F, -82.0F, -33.0F, 4, 2, 4, 0.0F, false));
		dummy_barometer.cubeList.add(new ModelBox(dummy_barometer, 182, 152, -5.5F, -88.0F, -32.4F, 4, 1, 4, 0.0F, false));
		dummy_barometer.cubeList.add(new ModelBox(dummy_barometer, 182, 152, -5.5F, -80.0F, -32.4F, 4, 1, 4, 0.0F, false));
		dummy_barometer.cubeList.add(new ModelBox(dummy_barometer, 23, 161, -4.6F, -91.0F, -31.5F, 2, 1, 2, 0.0F, false));
		dummy_barometer.cubeList.add(new ModelBox(dummy_barometer, 28, 157, -4.1F, -93.4F, -31.0F, 1, 4, 1, 0.0F, false));

		station_tilt_3 = new RendererModel(this);
		station_tilt_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_tilt_3, -1.0472F, 0.0F, 0.0F);
		side3.addChild(station_tilt_3);
		station_tilt_3.cubeList.add(new ModelBox(station_tilt_3, 182, 152, -7.0F, -13.4F, -81.0F, 14, 5, 4, 0.0F, false));
		station_tilt_3.cubeList.add(new ModelBox(station_tilt_3, 182, 152, -7.6F, -14.0F, -80.6F, 15, 6, 4, 0.0F, false));
		station_tilt_3.cubeList.add(new ModelBox(station_tilt_3, 182, 152, -21.0F, 5.0F, -80.4F, 12, 12, 4, 0.0F, false));
		station_tilt_3.cubeList.add(new ModelBox(station_tilt_3, 182, 152, -19.4F, 13.4F, -81.2F, 9, 3, 4, 0.0F, false));
		station_tilt_3.cubeList.add(new ModelBox(station_tilt_3, 182, 152, -20.4F, 7.4F, -80.8F, 11, 5, 4, 0.0F, false));
		station_tilt_3.cubeList.add(new ModelBox(station_tilt_3, 182, 152, -19.4F, 5.4F, -80.8F, 9, 2, 4, 0.0F, false));
		station_tilt_3.cubeList.add(new ModelBox(station_tilt_3, 182, 152, 10.0F, 5.0F, -81.0F, 12, 12, 4, 0.0F, false));
		station_tilt_3.cubeList.add(new ModelBox(station_tilt_3, 182, 152, 9.4F, 4.4F, -80.5F, 13, 13, 4, 0.0F, false));
		station_tilt_3.cubeList.add(new ModelBox(station_tilt_3, 69, 152, -7.0F, -4.0F, -81.0F, 14, 21, 4, 0.0F, false));

		throttle = new RendererModel(this);
		throttle.setRotationPoint(-15.3F, -5.0F, -3.0F);
		station_tilt_3.addChild(throttle);

		throttle_lever_rotate_x = new RendererModel(this);
		throttle_lever_rotate_x.setRotationPoint(9.5F, 12.0F, -74.5F);
		throttle.addChild(throttle_lever_rotate_x);
		throttle_lever_rotate_x.cubeList.add(new ModelBox(throttle_lever_rotate_x, 192, 91, 0.3F, -1.5F, -10.5F, 3, 3, 12, 0.0F, false));
		throttle_lever_rotate_x.cubeList.add(new ModelBox(throttle_lever_rotate_x, 142, 156, 1.5F, -0.8F, -12.5F, 2, 2, 1, 0.0F, false));
		throttle_lever_rotate_x.cubeList.add(new ModelBox(throttle_lever_rotate_x, 141, 161, 1.5F, -0.8F, -13.7F, 2, 2, 1, 0.0F, false));
		throttle_lever_rotate_x.cubeList.add(new ModelBox(throttle_lever_rotate_x, 137, 157, 1.5F, -0.8F, -14.9F, 2, 2, 1, 0.0F, false));
		throttle_lever_rotate_x.cubeList.add(new ModelBox(throttle_lever_rotate_x, 192, 91, 2.0F, -0.2F, -15.1F, 1, 1, 6, 0.0F, false));

		throttle_wheel = new RendererModel(this);
		throttle_wheel.setRotationPoint(0.0F, 0.0F, 0.0F);
		throttle.addChild(throttle_wheel);
		throttle_wheel.cubeList.add(new ModelBox(throttle_wheel, 87, 161, 10.0F, 8.0F, -84.0F, 4, 8, 1, 0.0F, false));
		throttle_wheel.cubeList.add(new ModelBox(throttle_wheel, 16, 154, 10.5F, 8.0F, -83.0F, 3, 8, 6, 0.0F, false));
		throttle_wheel.cubeList.add(new ModelBox(throttle_wheel, 16, 154, 10.5F, 3.0F, -79.0F, 3, 8, 6, 0.0F, false));
		throttle_wheel.cubeList.add(new ModelBox(throttle_wheel, 32, 106, 10.0F, 2.3F, -78.4F, 4, 1, 8, 0.0F, false));

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(12.0F, 12.0F, -80.0F);
		setRotationAngle(bone2, 0.7854F, 0.0F, 0.0F);
		throttle_wheel.addChild(bone2);
		bone2.cubeList.add(new ModelBox(bone2, 16, 154, -1.5F, 1.0F, -5.0F, 3, 8, 6, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 16, 154, -1.5F, -5.0F, 1.0F, 3, 8, 6, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 81, 164, -2.0F, 0.0F, -5.7F, 4, 8, 1, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 75, 157, -2.0F, -5.7F, 0.0F, 4, 1, 8, 0.0F, false));

		spacer = new RendererModel(this);
		spacer.setRotationPoint(-22.0F, -5.0F, -3.0F);
		station_tilt_3.addChild(spacer);

		spacer_wheel1 = new RendererModel(this);
		spacer_wheel1.setRotationPoint(9.0F, 0.0F, 0.0F);
		spacer.addChild(spacer_wheel1);
		spacer_wheel1.cubeList.add(new ModelBox(spacer_wheel1, 221, 12, 12.0F, 8.0F, -84.0F, 2, 8, 1, 0.0F, false));
		spacer_wheel1.cubeList.add(new ModelBox(spacer_wheel1, 221, 12, 12.0F, 2.3F, -78.4F, 2, 1, 8, 0.0F, false));

		spacer_tilt1 = new RendererModel(this);
		spacer_tilt1.setRotationPoint(12.0F, 12.0F, -80.0F);
		setRotationAngle(spacer_tilt1, 0.7854F, 0.0F, 0.0F);
		spacer_wheel1.addChild(spacer_tilt1);
		spacer_tilt1.cubeList.add(new ModelBox(spacer_tilt1, 221, 12, 0.0F, 0.0F, -5.7F, 2, 8, 1, 0.0F, false));
		spacer_tilt1.cubeList.add(new ModelBox(spacer_tilt1, 221, 12, 0.0F, -5.7F, 0.0F, 2, 1, 8, 0.0F, false));

		handbreak = new RendererModel(this);
		handbreak.setRotationPoint(-8.7F, -5.0F, -3.0F);
		station_tilt_3.addChild(handbreak);

		handbreak_lever2_rotate_x = new RendererModel(this);
		handbreak_lever2_rotate_x.setRotationPoint(9.5F, 12.0F, -74.5F);
		handbreak.addChild(handbreak_lever2_rotate_x);
		handbreak_lever2_rotate_x.cubeList.add(new ModelBox(handbreak_lever2_rotate_x, 131, 166, 1.6F, -0.8F, -12.5F, 2, 2, 1, 0.0F, false));
		handbreak_lever2_rotate_x.cubeList.add(new ModelBox(handbreak_lever2_rotate_x, 144, 155, 1.6F, -0.8F, -13.7F, 2, 2, 1, 0.0F, false));
		handbreak_lever2_rotate_x.cubeList.add(new ModelBox(handbreak_lever2_rotate_x, 144, 147, 1.6F, -0.8F, -14.9F, 2, 2, 1, 0.0F, false));
		handbreak_lever2_rotate_x.cubeList.add(new ModelBox(handbreak_lever2_rotate_x, 200, 96, 2.1F, -0.2F, -15.1F, 1, 1, 6, 0.0F, false));
		handbreak_lever2_rotate_x.cubeList.add(new ModelBox(handbreak_lever2_rotate_x, 200, 96, 1.8F, -1.5F, -10.5F, 3, 3, 12, 0.0F, false));

		handbreak_wheel2 = new RendererModel(this);
		handbreak_wheel2.setRotationPoint(0.0F, 0.0F, 0.0F);
		handbreak.addChild(handbreak_wheel2);
		handbreak_wheel2.cubeList.add(new ModelBox(handbreak_wheel2, 83, 149, 10.0F, 8.0F, -84.0F, 4, 8, 1, 0.0F, false));
		handbreak_wheel2.cubeList.add(new ModelBox(handbreak_wheel2, 17, 161, 10.5F, 8.0F, -83.0F, 3, 8, 6, 0.0F, false));
		handbreak_wheel2.cubeList.add(new ModelBox(handbreak_wheel2, 23, 157, 10.5F, 3.0F, -79.0F, 3, 8, 6, 0.0F, false));
		handbreak_wheel2.cubeList.add(new ModelBox(handbreak_wheel2, 96, 97, 10.0F, 2.3F, -78.4F, 4, 1, 8, 0.0F, false));

		handbreak_tilt2 = new RendererModel(this);
		handbreak_tilt2.setRotationPoint(12.0F, 12.0F, -80.0F);
		setRotationAngle(handbreak_tilt2, 0.7854F, 0.0F, 0.0F);
		handbreak_wheel2.addChild(handbreak_tilt2);
		handbreak_tilt2.cubeList.add(new ModelBox(handbreak_tilt2, 32, 155, -1.5F, 1.0F, -5.0F, 3, 8, 6, 0.0F, false));
		handbreak_tilt2.cubeList.add(new ModelBox(handbreak_tilt2, 20, 147, -1.5F, -5.0F, 1.0F, 3, 8, 6, 0.0F, false));
		handbreak_tilt2.cubeList.add(new ModelBox(handbreak_tilt2, 86, 166, -2.0F, 0.0F, -5.7F, 4, 8, 1, 0.0F, false));
		handbreak_tilt2.cubeList.add(new ModelBox(handbreak_tilt2, 75, 156, -2.0F, -5.7F, 0.0F, 4, 1, 8, 0.0F, false));

		refueler = new RendererModel(this);
		refueler.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_tilt_3.addChild(refueler);
		refueler.cubeList.add(new ModelBox(refueler, 97, 233, -19.0F, 14.0F, -82.0F, 2, 2, 4, 0.0F, false));
		refueler.cubeList.add(new ModelBox(refueler, 92, 230, -16.0F, 14.0F, -82.0F, 2, 2, 4, 0.0F, false));
		refueler.cubeList.add(new ModelBox(refueler, 101, 236, -13.0F, 14.0F, -82.0F, 2, 2, 4, 0.0F, false));
		refueler.cubeList.add(new ModelBox(refueler, 14, 164, -19.0F, 9.0F, -81.2F, 8, 2, 4, 0.0F, false));
		refueler.cubeList.add(new ModelBox(refueler, 134, 161, -20.0F, 11.0F, -81.6F, 10, 1, 4, 0.0F, false));
		refueler.cubeList.add(new ModelBox(refueler, 105, 230, -15.5F, 8.0F, -81.6F, 1, 3, 4, 0.0F, false));
		refueler.cubeList.add(new ModelBox(refueler, 134, 161, -19.0F, 6.0F, -81.6F, 8, 1, 4, 0.0F, false));
		refueler.cubeList.add(new ModelBox(refueler, 134, 161, -19.0F, 7.0F, -81.6F, 1, 2, 4, 0.0F, false));
		refueler.cubeList.add(new ModelBox(refueler, 134, 161, -12.0F, 7.0F, -81.6F, 1, 2, 4, 0.0F, false));
		refueler.cubeList.add(new ModelBox(refueler, 134, 161, -11.0F, 8.0F, -81.6F, 1, 3, 4, 0.0F, false));
		refueler.cubeList.add(new ModelBox(refueler, 134, 161, -20.0F, 8.0F, -81.6F, 1, 3, 4, 0.0F, false));
		refueler.cubeList.add(new ModelBox(refueler, 16, 164, -18.0F, 7.0F, -81.2F, 6, 2, 4, 0.0F, false));

		dummy_dial_c1 = new RendererModel(this);
		dummy_dial_c1.setRotationPoint(-11.05F, -17.1F, -79.0F);
		setRotationAngle(dummy_dial_c1, 0.0F, 0.0F, 0.7854F);
		station_tilt_3.addChild(dummy_dial_c1);
		dummy_dial_c1.cubeList.add(new ModelBox(dummy_dial_c1, 182, 152, 3.45F, 4.9F, -1.4F, 7, 7, 4, 0.0F, false));
		dummy_dial_c1.cubeList.add(new ModelBox(dummy_dial_c1, 208, 64, 4.55F, 6.0F, -2.4F, 5, 5, 4, 0.0F, false));
		dummy_dial_c1.cubeList.add(new ModelBox(dummy_dial_c1, 136, 153, 5.05F, 6.6F, -3.4F, 4, 4, 4, 0.0F, false));
		dummy_dial_c1.cubeList.add(new ModelBox(dummy_dial_c1, 22, 158, 5.65F, 7.1F, -3.6F, 3, 3, 4, 0.0F, false));

		dummy_dial_c2 = new RendererModel(this);
		dummy_dial_c2.setRotationPoint(-11.05F, -17.1F, -79.0F);
		setRotationAngle(dummy_dial_c2, 0.0F, 0.0F, 0.7854F);
		station_tilt_3.addChild(dummy_dial_c2);
		dummy_dial_c2.cubeList.add(new ModelBox(dummy_dial_c2, 182, 152, 20.45F, -12.1F, -1.4F, 7, 7, 4, 0.0F, false));
		dummy_dial_c2.cubeList.add(new ModelBox(dummy_dial_c2, 200, 68, 21.55F, -11.0F, -2.4F, 5, 5, 4, 0.0F, false));
		dummy_dial_c2.cubeList.add(new ModelBox(dummy_dial_c2, 141, 160, 22.05F, -10.4F, -3.4F, 4, 4, 4, 0.0F, false));
		dummy_dial_c2.cubeList.add(new ModelBox(dummy_dial_c2, 23, 160, 22.65F, -9.9F, -3.6F, 3, 3, 4, 0.0F, false));

		dummy_button_C1 = new RendererModel(this);
		dummy_button_C1.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_tilt_3.addChild(dummy_button_C1);
		dummy_button_C1.cubeList.add(new ModelBox(dummy_button_C1, 196, 87, 16.4F, 5.6F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_C1.cubeList.add(new ModelBox(dummy_button_C1, 89, 152, 11.0F, 6.0F, -81.3F, 4, 10, 4, 0.0F, false));
		dummy_button_C1.cubeList.add(new ModelBox(dummy_button_C1, 196, 87, 19.3F, 5.6F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_C1.cubeList.add(new ModelBox(dummy_button_C1, 196, 87, 19.3F, 8.4F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_C1.cubeList.add(new ModelBox(dummy_button_C1, 196, 87, 16.4F, 8.4F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_C1.cubeList.add(new ModelBox(dummy_button_C1, 196, 87, 19.3F, 11.3F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_C1.cubeList.add(new ModelBox(dummy_button_C1, 196, 87, 16.4F, 11.3F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_C1.cubeList.add(new ModelBox(dummy_button_C1, 196, 87, 19.3F, 14.3F, -81.3F, 2, 2, 4, 0.0F, false));
		dummy_button_C1.cubeList.add(new ModelBox(dummy_button_C1, 196, 87, 16.4F, 14.3F, -81.3F, 2, 2, 4, 0.0F, false));

		side4 = new RendererModel(this);
		side4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(side4, 0.0F, 3.1416F, 0.0F);
		controls.addChild(side4);

		dimention_select = new RendererModel(this);
		dimention_select.setRotationPoint(-0.2F, -57.0F, 55.6F);
		side4.addChild(dimention_select);

		dialframe = new RendererModel(this);
		dialframe.setRotationPoint(0.2F, -29.1F, -84.3F);
		dimention_select.addChild(dialframe);
		dialframe.cubeList.add(new ModelBox(dialframe, 202, 79, -3.6F, -8.5F, -4.0F, 7, 2, 4, 0.0F, false));
		dialframe.cubeList.add(new ModelBox(dialframe, 202, 79, -1.2F, -0.9F, -5.0F, 2, 2, 4, 0.0F, false));
		dialframe.cubeList.add(new ModelBox(dialframe, 14, 155, -3.6F, -6.5F, -3.3F, 7, 14, 3, 0.0F, false));
		dialframe.cubeList.add(new ModelBox(dialframe, 21, 158, 3.4F, -6.5F, -3.3F, 2, 12, 3, 0.0F, false));
		dialframe.cubeList.add(new ModelBox(dialframe, 17, 154, -5.6F, -6.5F, -3.3F, 2, 12, 3, 0.0F, false));
		dialframe.cubeList.add(new ModelBox(dialframe, 14, 160, 5.4F, -4.5F, -3.3F, 2, 8, 3, 0.0F, false));
		dialframe.cubeList.add(new ModelBox(dialframe, 14, 158, -7.6F, -4.5F, -3.3F, 2, 8, 3, 0.0F, false));
		dialframe.cubeList.add(new ModelBox(dialframe, 202, 79, -3.6F, 6.4F, -4.0F, 7, 2, 4, 0.0F, false));
		dialframe.cubeList.add(new ModelBox(dialframe, 202, 79, 6.3F, -3.6F, -4.0F, 2, 7, 4, 0.0F, false));
		dialframe.cubeList.add(new ModelBox(dialframe, 202, 79, -8.6F, -3.6F, -4.0F, 2, 7, 4, 0.0F, false));
		dialframe.cubeList.add(new ModelBox(dialframe, 202, 79, -1.7F, 7.4F, -3.0F, 1, 4, 2, 0.0F, false));
		dialframe.cubeList.add(new ModelBox(dialframe, 202, 79, 0.7F, 7.4F, -3.0F, 1, 4, 2, 0.0F, false));

		dialframe2 = new RendererModel(this);
		dialframe2.setRotationPoint(0.2F, -29.1F, -84.3F);
		setRotationAngle(dialframe2, 0.0F, 0.0F, -0.7854F);
		dimention_select.addChild(dialframe2);
		dialframe2.cubeList.add(new ModelBox(dialframe2, 202, 79, -3.5F, -8.6F, -4.0F, 7, 2, 4, 0.0F, false));
		dialframe2.cubeList.add(new ModelBox(dialframe2, 202, 79, -3.5F, 6.3F, -4.0F, 7, 2, 4, 0.0F, false));
		dialframe2.cubeList.add(new ModelBox(dialframe2, 202, 79, 6.4F, -3.6F, -4.0F, 2, 7, 4, 0.0F, false));
		dialframe2.cubeList.add(new ModelBox(dialframe2, 202, 79, -8.5F, -3.6F, -4.0F, 2, 7, 4, 0.0F, false));

		needle_rotate_z = new RendererModel(this);
		needle_rotate_z.setRotationPoint(0.0F, -28.9F, -87.9F);
		setRotationAngle(needle_rotate_z, 0.0F, 0.0F, -1.9199F);
		dimention_select.addChild(needle_rotate_z);
		needle_rotate_z.cubeList.add(new ModelBox(needle_rotate_z, 144, 161, -0.5F, -6.1F, 0.0F, 1, 7, 0, 0.0F, false));

		station_tilt_4 = new RendererModel(this);
		station_tilt_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_tilt_4, -1.0472F, 0.0F, 0.0F);
		side4.addChild(station_tilt_4);
		station_tilt_4.cubeList.add(new ModelBox(station_tilt_4, 182, 152, -2.5F, -13.8F, -81.0F, 5, 5, 4, 0.0F, false));
		station_tilt_4.cubeList.add(new ModelBox(station_tilt_4, 182, 152, 8.5F, 4.4F, -81.0F, 11, 11, 4, 0.0F, false));
		station_tilt_4.cubeList.add(new ModelBox(station_tilt_4, 182, 152, 8.0F, 3.8F, -80.3F, 12, 12, 4, 0.0F, false));
		station_tilt_4.cubeList.add(new ModelBox(station_tilt_4, 69, 156, -21.0F, 4.0F, -81.0F, 14, 12, 4, 0.0F, false));

		waypoint_selector = new RendererModel(this);
		waypoint_selector.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_tilt_4.addChild(waypoint_selector);
		waypoint_selector.cubeList.add(new ModelBox(waypoint_selector, 181, 11, 10.0F, 6.0F, -82.6F, 8, 8, 4, 0.0F, false));
		waypoint_selector.cubeList.add(new ModelBox(waypoint_selector, 181, 13, 11.0F, 7.0F, -83.1F, 6, 6, 4, 0.0F, false));
		waypoint_selector.cubeList.add(new ModelBox(waypoint_selector, 178, 13, 11.0F, 14.0F, -82.0F, 6, 1, 4, 0.0F, false));
		waypoint_selector.cubeList.add(new ModelBox(waypoint_selector, 180, 17, 11.0F, 5.0F, -82.0F, 6, 1, 4, 0.0F, false));
		waypoint_selector.cubeList.add(new ModelBox(waypoint_selector, 185, 10, 9.0F, 7.0F, -82.0F, 1, 6, 4, 0.0F, false));
		waypoint_selector.cubeList.add(new ModelBox(waypoint_selector, 186, 13, 18.0F, 7.0F, -82.0F, 1, 6, 4, 0.0F, false));

		dummy_button_d1 = new RendererModel(this);
		dummy_button_d1.setRotationPoint(7.38F, 0.46F, -47.1F);
		station_tilt_4.addChild(dummy_button_d1);
		dummy_button_d1.cubeList.add(new ModelBox(dummy_button_d1, 14, 162, -27.33F, 4.64F, -34.65F, 12, 4, 4, 0.0F, false));
		dummy_button_d1.cubeList.add(new ModelBox(dummy_button_d1, 141, 154, -22.83F, 5.14F, -35.35F, 3, 3, 4, 0.0F, false));
		dummy_button_d1.cubeList.add(new ModelBox(dummy_button_d1, 146, 160, -26.63F, 5.14F, -35.35F, 3, 3, 4, 0.0F, false));
		dummy_button_d1.cubeList.add(new ModelBox(dummy_button_d1, 137, 159, -19.13F, 5.14F, -35.75F, 3, 3, 4, 0.0F, false));

		dummy_button_d2 = new RendererModel(this);
		dummy_button_d2.setRotationPoint(7.38F, 0.46F, -47.1F);
		station_tilt_4.addChild(dummy_button_d2);
		dummy_button_d2.cubeList.add(new ModelBox(dummy_button_d2, 14, 162, -27.33F, 9.64F, -34.65F, 12, 4, 4, 0.0F, false));
		dummy_button_d2.cubeList.add(new ModelBox(dummy_button_d2, 141, 154, -22.83F, 10.14F, -35.35F, 3, 3, 4, 0.0F, false));
		dummy_button_d2.cubeList.add(new ModelBox(dummy_button_d2, 146, 160, -26.63F, 10.14F, -35.35F, 3, 3, 4, 0.0F, false));
		dummy_button_d2.cubeList.add(new ModelBox(dummy_button_d2, 137, 159, -19.13F, 10.14F, -35.75F, 3, 3, 4, 0.0F, false));

		sonic_port = new RendererModel(this);
		sonic_port.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_tilt_4.addChild(sonic_port);
		sonic_port.cubeList.add(new ModelBox(sonic_port, 135, 161, -2.0F, 0.0F, -80.5F, 4, 4, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 182, 152, -3.0F, -1.0F, -81.4F, 1, 6, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 182, 152, 2.0F, -1.0F, -81.4F, 1, 6, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 182, 152, -2.0F, -2.0F, -81.4F, 4, 2, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 182, 152, -1.0F, -3.0F, -81.4F, 2, 1, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 182, 152, -1.0F, 6.0F, -81.4F, 2, 1, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 201, 95, -0.5F, 6.9F, -80.4F, 1, 5, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 206, 102, 10.8F, 1.0F, -80.4F, 1, 5, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 198, 96, -8.5F, 11.9F, -80.4F, 8, 1, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 188, 94, 2.8F, -0.1F, -80.4F, 8, 1, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 231, 18, -1.2F, 11.3F, -80.7F, 2, 2, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 182, 152, 1.5F, 5.6F, -80.7F, 1, 1, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 182, 152, 1.5F, -2.7F, -80.7F, 1, 1, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 182, 152, -2.6F, 5.6F, -80.7F, 1, 1, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 182, 152, -2.6F, -2.7F, -80.7F, 1, 1, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 228, 17, 10.3F, -0.7F, -80.7F, 2, 2, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 182, 152, -2.0F, 4.0F, -81.4F, 4, 2, 4, 0.0F, false));

		dummy_dial_d1 = new RendererModel(this);
		dummy_dial_d1.setRotationPoint(-11.05F, -17.1F, -79.0F);
		setRotationAngle(dummy_dial_d1, 0.0F, 0.0F, 0.7854F);
		station_tilt_4.addChild(dummy_dial_d1);
		dummy_dial_d1.cubeList.add(new ModelBox(dummy_dial_d1, 182, 152, 3.45F, 0.9F, -1.4F, 7, 7, 4, 0.0F, false));
		dummy_dial_d1.cubeList.add(new ModelBox(dummy_dial_d1, 204, 51, 4.55F, 2.0F, -2.4F, 5, 5, 4, 0.0F, false));
		dummy_dial_d1.cubeList.add(new ModelBox(dummy_dial_d1, 132, 166, 5.05F, 2.6F, -3.4F, 4, 4, 4, 0.0F, false));
		dummy_dial_d1.cubeList.add(new ModelBox(dummy_dial_d1, 24, 160, 5.65F, 3.1F, -3.6F, 3, 3, 4, 0.0F, false));

		dummy_dial_d2 = new RendererModel(this);
		dummy_dial_d2.setRotationPoint(-11.05F, -17.1F, -79.0F);
		setRotationAngle(dummy_dial_d2, 0.0F, 0.0F, 0.7854F);
		station_tilt_4.addChild(dummy_dial_d2);
		dummy_dial_d2.cubeList.add(new ModelBox(dummy_dial_d2, 182, 152, 16.45F, -12.1F, -1.4F, 7, 7, 4, 0.0F, false));
		dummy_dial_d2.cubeList.add(new ModelBox(dummy_dial_d2, 192, 77, 17.55F, -11.0F, -2.4F, 5, 5, 4, 0.0F, false));
		dummy_dial_d2.cubeList.add(new ModelBox(dummy_dial_d2, 131, 159, 18.05F, -10.4F, -3.4F, 4, 4, 4, 0.0F, false));
		dummy_dial_d2.cubeList.add(new ModelBox(dummy_dial_d2, 26, 162, 18.65F, -9.9F, -3.6F, 3, 3, 4, 0.0F, false));

		side5 = new RendererModel(this);
		side5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(side5, 0.0F, 2.0944F, 0.0F);
		controls.addChild(side5);

		telepathic_circuits = new RendererModel(this);
		telepathic_circuits.setRotationPoint(0.0F, 0.0F, 0.0F);
		side5.addChild(telepathic_circuits);
		telepathic_circuits.cubeList.add(new ModelBox(telepathic_circuits, 186, 84, -6.0F, -71.3F, -55.0F, 12, 12, 12, 0.0F, false));
		telepathic_circuits.cubeList.add(new ModelBox(telepathic_circuits, 186, 84, -7.0F, -72.2F, -56.0F, 14, 1, 14, 0.0F, false));

		station_tilt_5 = new RendererModel(this);
		station_tilt_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_tilt_5, -1.0472F, 0.0F, 0.0F);
		side5.addChild(station_tilt_5);
		station_tilt_5.cubeList.add(new ModelBox(station_tilt_5, 182, 152, -7.0F, 2.0F, -80.75F, 14, 16, 4, 0.0F, false));
		station_tilt_5.cubeList.add(new ModelBox(station_tilt_5, 167, 195, -4.0F, 3.0F, -90.4F, 8, 8, 4, 0.0F, false));
		station_tilt_5.cubeList.add(new ModelBox(station_tilt_5, 191, 79, -5.0F, 2.0F, -90.0F, 10, 10, 7, 0.0F, false));

		pipes_5 = new RendererModel(this);
		pipes_5.setRotationPoint(9.0F, -16.0F, -1.75F);
		station_tilt_5.addChild(pipes_5);
		pipes_5.cubeList.add(new ModelBox(pipes_5, 209, 92, -19.7F, 11.1F, -79.25F, 1, 17, 2, 0.0F, false));
		pipes_5.cubeList.add(new ModelBox(pipes_5, 209, 92, 7.55F, 16.1F, -79.25F, 1, 12, 2, 0.0F, false));
		pipes_5.cubeList.add(new ModelBox(pipes_5, 209, 92, 0.55F, 12.85F, -79.25F, 1, 4, 2, 0.0F, false));
		pipes_5.cubeList.add(new ModelBox(pipes_5, 209, 92, -9.2F, -4.65F, -79.25F, 1, 4, 2, 0.0F, false));
		pipes_5.cubeList.add(new ModelBox(pipes_5, 228, 17, -20.2F, 20.35F, -79.75F, 2, 2, 2, 0.0F, false));
		pipes_5.cubeList.add(new ModelBox(pipes_5, 228, 17, -20.2F, 10.85F, -79.75F, 2, 2, 2, 0.0F, false));
		pipes_5.cubeList.add(new ModelBox(pipes_5, 228, 17, 0.05F, 10.85F, -79.75F, 2, 2, 2, 0.0F, false));
		pipes_5.cubeList.add(new ModelBox(pipes_5, 228, 17, -9.7F, -0.65F, -79.75F, 2, 2, 2, 0.0F, false));
		pipes_5.cubeList.add(new ModelBox(pipes_5, 228, 17, 0.05F, 15.35F, -79.75F, 2, 2, 2, 0.0F, false));
		pipes_5.cubeList.add(new ModelBox(pipes_5, 228, 17, 7.05F, 15.35F, -79.75F, 2, 2, 2, 0.0F, false));
		pipes_5.cubeList.add(new ModelBox(pipes_5, 228, 17, -20.2F, 26.6F, -79.75F, 2, 2, 2, 0.0F, false));
		pipes_5.cubeList.add(new ModelBox(pipes_5, 209, 92, -23.7F, 27.1F, -79.25F, 4, 1, 2, 0.0F, false));
		pipes_5.cubeList.add(new ModelBox(pipes_5, 209, 92, 1.55F, 15.85F, -79.25F, 6, 1, 1, 0.0F, false));
		pipes_5.cubeList.add(new ModelBox(pipes_5, 209, 92, -18.7F, 20.85F, -79.25F, 4, 1, 2, 0.0F, false));

		bone = new RendererModel(this);
		bone.setRotationPoint(0.0F, 2.5F, -93.5F);
		setRotationAngle(bone, 0.6109F, 0.0F, 0.0F);
		station_tilt_5.addChild(bone);
		bone.cubeList.add(new ModelBox(bone, 195, 102, -4.6F, 1.7F, -2.8F, 9, 1, 6, 0.0F, false));

		bone3 = new RendererModel(this);
		bone3.setRotationPoint(0.0F, 2.5F, -93.5F);
		setRotationAngle(bone3, 0.6109F, 0.0F, 0.0F);
		station_tilt_5.addChild(bone3);
		bone3.cubeList.add(new ModelBox(bone3, 200, 94, -4.6F, 2.7F, -2.8F, 1, 7, 6, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 211, 88, 3.4F, 2.7F, -2.8F, 1, 7, 6, 0.0F, false));

		dummy_plate = new RendererModel(this);
		dummy_plate.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_tilt_5.addChild(dummy_plate);
		dummy_plate.cubeList.add(new ModelBox(dummy_plate, 51, 97, -11.75F, -15.0F, -81.0F, 24, 10, 4, 0.0F, false));
		dummy_plate.cubeList.add(new ModelBox(dummy_plate, 229, 13, -10.25F, -8.0F, -81.25F, 4, 2, 4, 0.0F, false));
		dummy_plate.cubeList.add(new ModelBox(dummy_plate, 229, 13, -4.75F, -8.0F, -81.25F, 4, 2, 4, 0.0F, false));
		dummy_plate.cubeList.add(new ModelBox(dummy_plate, 229, 13, 1.0F, -8.0F, -81.25F, 4, 2, 4, 0.0F, false));
		dummy_plate.cubeList.add(new ModelBox(dummy_plate, 229, 13, 6.75F, -8.0F, -81.25F, 4, 2, 4, 0.0F, false));

		dummy_bell_e1 = new RendererModel(this);
		dummy_bell_e1.setRotationPoint(-1.62F, -13.54F, -48.85F);
		dummy_plate.addChild(dummy_bell_e1);
		dummy_bell_e1.cubeList.add(new ModelBox(dummy_bell_e1, 195, 153, -9.08F, -0.36F, -33.4F, 5, 5, 6, 0.0F, false));
		dummy_bell_e1.cubeList.add(new ModelBox(dummy_bell_e1, 209, 92, -8.58F, 0.14F, -34.9F, 4, 4, 6, 0.0F, false));
		dummy_bell_e1.cubeList.add(new ModelBox(dummy_bell_e1, 209, 92, -8.08F, 0.64F, -35.65F, 3, 3, 6, 0.0F, false));
		dummy_bell_e1.cubeList.add(new ModelBox(dummy_bell_e1, 209, 92, -7.08F, 1.64F, -36.15F, 1, 1, 6, 0.0F, false));

		dummy_bell_e2 = new RendererModel(this);
		dummy_bell_e2.setRotationPoint(-1.62F, -13.54F, -48.85F);
		dummy_plate.addChild(dummy_bell_e2);
		dummy_bell_e2.cubeList.add(new ModelBox(dummy_bell_e2, 195, 153, -3.58F, -0.36F, -33.4F, 5, 5, 6, 0.0F, false));
		dummy_bell_e2.cubeList.add(new ModelBox(dummy_bell_e2, 209, 92, -3.08F, 0.14F, -34.9F, 4, 4, 6, 0.0F, false));
		dummy_bell_e2.cubeList.add(new ModelBox(dummy_bell_e2, 209, 92, -2.58F, 0.64F, -35.65F, 3, 3, 6, 0.0F, false));
		dummy_bell_e2.cubeList.add(new ModelBox(dummy_bell_e2, 209, 92, -1.58F, 1.64F, -36.15F, 1, 1, 6, 0.0F, false));

		dummy_bell_e3 = new RendererModel(this);
		dummy_bell_e3.setRotationPoint(-1.62F, -13.54F, -48.85F);
		dummy_plate.addChild(dummy_bell_e3);
		dummy_bell_e3.cubeList.add(new ModelBox(dummy_bell_e3, 195, 153, 2.17F, -0.36F, -33.4F, 5, 5, 6, 0.0F, false));
		dummy_bell_e3.cubeList.add(new ModelBox(dummy_bell_e3, 209, 92, 2.67F, 0.14F, -34.9F, 4, 4, 6, 0.0F, false));
		dummy_bell_e3.cubeList.add(new ModelBox(dummy_bell_e3, 209, 92, 3.17F, 0.64F, -35.65F, 3, 3, 6, 0.0F, false));
		dummy_bell_e3.cubeList.add(new ModelBox(dummy_bell_e3, 209, 92, 4.17F, 1.64F, -36.15F, 1, 1, 6, 0.0F, false));

		dummy_bell_e4 = new RendererModel(this);
		dummy_bell_e4.setRotationPoint(-1.62F, -13.54F, -48.85F);
		dummy_plate.addChild(dummy_bell_e4);
		dummy_bell_e4.cubeList.add(new ModelBox(dummy_bell_e4, 195, 153, 7.92F, -0.36F, -33.4F, 5, 5, 6, 0.0F, false));
		dummy_bell_e4.cubeList.add(new ModelBox(dummy_bell_e4, 209, 92, 8.42F, 0.14F, -34.9F, 4, 4, 6, 0.0F, false));
		dummy_bell_e4.cubeList.add(new ModelBox(dummy_bell_e4, 209, 92, 8.92F, 0.64F, -35.65F, 3, 3, 6, 0.0F, false));
		dummy_bell_e4.cubeList.add(new ModelBox(dummy_bell_e4, 209, 92, 9.92F, 1.64F, -36.15F, 1, 1, 6, 0.0F, false));

		fast_return = new RendererModel(this);
		fast_return.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_tilt_5.addChild(fast_return);
		fast_return.cubeList.add(new ModelBox(fast_return, 182, 152, -22.0F, 9.75F, -80.75F, 8, 5, 4, 0.0F, false));
		fast_return.cubeList.add(new ModelBox(fast_return, 29, 155, -21.5F, 10.75F, -81.75F, 7, 3, 4, 0.0F, false));
		fast_return.cubeList.add(new ModelBox(fast_return, 96, 231, -21.0F, 11.25F, -83.0F, 6, 2, 4, 0.0F, false));

		stablizer = new RendererModel(this);
		stablizer.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_tilt_5.addChild(stablizer);
		stablizer.cubeList.add(new ModelBox(stablizer, 182, 152, 15.0F, 10.0F, -80.5F, 8, 5, 4, 0.0F, false));
		stablizer.cubeList.add(new ModelBox(stablizer, 29, 155, 15.5F, 11.0F, -81.5F, 7, 3, 4, 0.0F, false));
		stablizer.cubeList.add(new ModelBox(stablizer, 8, 211, 16.0F, 11.5F, -82.75F, 6, 2, 4, 0.0F, false));

		side6 = new RendererModel(this);
		side6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(side6, 0.0F, 1.0472F, 0.0F);
		controls.addChild(side6);

		station_tilt_6 = new RendererModel(this);
		station_tilt_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_tilt_6, -1.0472F, 0.0F, 0.0F);
		side6.addChild(station_tilt_6);
		station_tilt_6.cubeList.add(new ModelBox(station_tilt_6, 182, 152, -10.0F, -14.4F, -82.0F, 20, 8, 4, 0.0F, false));
		station_tilt_6.cubeList.add(new ModelBox(station_tilt_6, 182, 152, -11.0F, -15.4F, -81.0F, 22, 10, 4, 0.0F, false));

		door = new RendererModel(this);
		door.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_tilt_6.addChild(door);

		door_plate = new RendererModel(this);
		door_plate.setRotationPoint(0.0F, 0.0F, 0.0F);
		door.addChild(door_plate);
		door_plate.cubeList.add(new ModelBox(door_plate, 82, 160, -1.0F, 9.0F, -81.0F, 2, 2, 4, 0.0F, false));
		door_plate.cubeList.add(new ModelBox(door_plate, 133, 159, -0.5F, 7.25F, -81.25F, 1, 2, 4, 0.0F, false));
		door_plate.cubeList.add(new ModelBox(door_plate, 133, 159, -1.0F, 5.25F, -81.25F, 2, 2, 4, 0.0F, false));
		door_plate.cubeList.add(new ModelBox(door_plate, 82, 160, -1.0F, -2.0F, -81.0F, 2, 2, 4, 0.0F, false));
		door_plate.cubeList.add(new ModelBox(door_plate, 82, 160, -2.5F, 8.45F, -80.75F, 1, 1, 4, 0.0F, false));
		door_plate.cubeList.add(new ModelBox(door_plate, 82, 160, 1.5F, 8.45F, -80.75F, 1, 1, 4, 0.0F, false));
		door_plate.cubeList.add(new ModelBox(door_plate, 82, 160, 1.5F, -0.55F, -80.75F, 1, 1, 4, 0.0F, false));
		door_plate.cubeList.add(new ModelBox(door_plate, 82, 160, -2.5F, -0.55F, -80.75F, 1, 1, 4, 0.0F, false));
		door_plate.cubeList.add(new ModelBox(door_plate, 82, 160, -2.0F, 0.0F, -81.0F, 4, 9, 4, 0.0F, false));
		door_plate.cubeList.add(new ModelBox(door_plate, 82, 160, -1.5F, 1.25F, -81.75F, 3, 3, 4, 0.0F, false));

		door_knob_rotate_z = new RendererModel(this);
		door_knob_rotate_z.setRotationPoint(0.0F, 3.0F, -83.6667F);
		door.addChild(door_knob_rotate_z);
		door_knob_rotate_z.cubeList.add(new ModelBox(door_knob_rotate_z, 203, 93, -0.5F, -0.5F, 0.1667F, 1, 1, 4, 0.0F, false));
		door_knob_rotate_z.cubeList.add(new ModelBox(door_knob_rotate_z, 23, 167, -1.5F, -1.5F, -1.8333F, 3, 3, 2, 0.0F, false));
		door_knob_rotate_z.cubeList.add(new ModelBox(door_knob_rotate_z, 20, 161, -1.0F, -1.0F, -2.3333F, 2, 2, 2, 0.0F, false));

		dummy_button_f1 = new RendererModel(this);
		dummy_button_f1.setRotationPoint(7.38F, 0.46F, -47.1F);
		station_tilt_6.addChild(dummy_button_f1);
		dummy_button_f1.cubeList.add(new ModelBox(dummy_button_f1, 182, 152, -26.08F, 0.64F, -33.4F, 12, 4, 4, 0.0F, false));
		dummy_button_f1.cubeList.add(new ModelBox(dummy_button_f1, 141, 154, -21.58F, 1.14F, -34.1F, 3, 3, 4, 0.0F, false));
		dummy_button_f1.cubeList.add(new ModelBox(dummy_button_f1, 96, 236, -25.38F, 1.14F, -34.35F, 3, 3, 4, 0.0F, false));
		dummy_button_f1.cubeList.add(new ModelBox(dummy_button_f1, 137, 159, -17.88F, 1.14F, -34.5F, 3, 3, 4, 0.0F, false));

		dummy_button_f2 = new RendererModel(this);
		dummy_button_f2.setRotationPoint(7.38F, 0.46F, -47.1F);
		station_tilt_6.addChild(dummy_button_f2);
		dummy_button_f2.cubeList.add(new ModelBox(dummy_button_f2, 182, 152, -26.08F, 5.64F, -33.4F, 12, 4, 4, 0.0F, false));
		dummy_button_f2.cubeList.add(new ModelBox(dummy_button_f2, 183, 17, -21.58F, 6.14F, -34.1F, 3, 3, 4, 0.0F, false));
		dummy_button_f2.cubeList.add(new ModelBox(dummy_button_f2, 146, 160, -25.38F, 6.14F, -34.1F, 3, 3, 4, 0.0F, false));
		dummy_button_f2.cubeList.add(new ModelBox(dummy_button_f2, 137, 159, -17.88F, 6.14F, -34.25F, 3, 3, 4, 0.0F, false));

		dummy_button_f3 = new RendererModel(this);
		dummy_button_f3.setRotationPoint(7.38F, 0.46F, -47.1F);
		station_tilt_6.addChild(dummy_button_f3);
		dummy_button_f3.cubeList.add(new ModelBox(dummy_button_f3, 182, 152, -26.08F, 10.64F, -33.4F, 12, 4, 4, 0.0F, false));
		dummy_button_f3.cubeList.add(new ModelBox(dummy_button_f3, 141, 154, -21.58F, 11.14F, -34.85F, 3, 3, 4, 0.0F, false));
		dummy_button_f3.cubeList.add(new ModelBox(dummy_button_f3, 146, 160, -25.38F, 11.14F, -34.1F, 3, 3, 4, 0.0F, false));
		dummy_button_f3.cubeList.add(new ModelBox(dummy_button_f3, 137, 159, -17.88F, 11.14F, -34.5F, 3, 3, 4, 0.0F, false));

		dummy_button_f4 = new RendererModel(this);
		dummy_button_f4.setRotationPoint(7.38F, 0.46F, -47.1F);
		station_tilt_6.addChild(dummy_button_f4);
		dummy_button_f4.cubeList.add(new ModelBox(dummy_button_f4, 182, 152, -1.08F, 0.64F, -33.4F, 12, 4, 4, 0.0F, false));
		dummy_button_f4.cubeList.add(new ModelBox(dummy_button_f4, 141, 154, 3.42F, 1.14F, -34.1F, 3, 3, 4, 0.0F, false));
		dummy_button_f4.cubeList.add(new ModelBox(dummy_button_f4, 146, 160, -0.38F, 1.14F, -34.1F, 3, 3, 4, 0.0F, false));
		dummy_button_f4.cubeList.add(new ModelBox(dummy_button_f4, 176, 19, 7.12F, 1.14F, -34.5F, 3, 3, 4, 0.0F, false));

		dummy_button_f5 = new RendererModel(this);
		dummy_button_f5.setRotationPoint(7.38F, 0.46F, -47.1F);
		station_tilt_6.addChild(dummy_button_f5);
		dummy_button_f5.cubeList.add(new ModelBox(dummy_button_f5, 182, 152, -1.08F, 5.64F, -33.4F, 12, 4, 4, 0.0F, false));
		dummy_button_f5.cubeList.add(new ModelBox(dummy_button_f5, 141, 154, 3.42F, 6.14F, -34.1F, 3, 3, 4, 0.0F, false));
		dummy_button_f5.cubeList.add(new ModelBox(dummy_button_f5, 146, 160, -0.38F, 6.14F, -34.6F, 3, 3, 4, 0.0F, false));
		dummy_button_f5.cubeList.add(new ModelBox(dummy_button_f5, 137, 159, 7.12F, 6.14F, -34.0F, 3, 3, 4, 0.0F, false));

		dummy_button_f6 = new RendererModel(this);
		dummy_button_f6.setRotationPoint(7.38F, 0.46F, -47.1F);
		station_tilt_6.addChild(dummy_button_f6);
		dummy_button_f6.cubeList.add(new ModelBox(dummy_button_f6, 182, 152, -1.08F, 10.64F, -33.4F, 12, 4, 4, 0.0F, false));
		dummy_button_f6.cubeList.add(new ModelBox(dummy_button_f6, 190, 16, 3.42F, 11.14F, -34.1F, 3, 3, 4, 0.0F, false));
		dummy_button_f6.cubeList.add(new ModelBox(dummy_button_f6, 146, 160, -0.38F, 11.14F, -34.1F, 3, 3, 4, 0.0F, false));
		dummy_button_f6.cubeList.add(new ModelBox(dummy_button_f6, 137, 159, 7.12F, 11.14F, -34.25F, 3, 3, 4, 0.0F, false));

		coms = new RendererModel(this);
		coms.setRotationPoint(0.0F, 0.0F, 0.0F);
		side6.addChild(coms);
		coms.cubeList.add(new ModelBox(coms, 182, 152, -7.0F, -99.0F, -33.0F, 14, 3, 3, 0.0F, false));
		coms.cubeList.add(new ModelBox(coms, 80, 148, 7.0F, -100.0F, -34.0F, 2, 31, 5, 0.0F, false));
		coms.cubeList.add(new ModelBox(coms, 83, 141, -9.0F, -100.0F, -34.0F, 2, 31, 5, 0.0F, false));

		bell_rotate_x = new RendererModel(this);
		bell_rotate_x.setRotationPoint(0.0F, -96.0F, -32.0F);
		coms.addChild(bell_rotate_x);
		bell_rotate_x.cubeList.add(new ModelBox(bell_rotate_x, 197, 89, -6.0F, 12.0F, -6.0F, 12, 2, 12, 0.0F, false));
		bell_rotate_x.cubeList.add(new ModelBox(bell_rotate_x, 122, 155, -5.4F, 12.4F, -5.4F, 11, 2, 11, 0.0F, false));
		bell_rotate_x.cubeList.add(new ModelBox(bell_rotate_x, 193, 99, -4.0F, 2.0F, -4.0F, 8, 10, 8, 0.0F, false));
		bell_rotate_x.cubeList.add(new ModelBox(bell_rotate_x, 207, 100, -1.0F, 0.0F, -1.0F, 2, 2, 2, 0.0F, false));

		time_rotor_slide_y = new RendererModel(this);
		time_rotor_slide_y.setRotationPoint(0.0F, -58.0F, 0.0F);

		rotor_plate = new RendererModel(this);
		rotor_plate.setRotationPoint(0.0F, 89.0F, 0.0F);
		time_rotor_slide_y.addChild(rotor_plate);
		rotor_plate.cubeList.add(new ModelBox(rotor_plate, 24, 109, -16.25F, -89.6F, -8.0F, 33, 2, 16, 0.0F, false));
		rotor_plate.cubeList.add(new ModelBox(rotor_plate, 59, 113, -6.25F, -98.6F, -6.0F, 12, 3, 12, 0.0F, false));
		rotor_plate.cubeList.add(new ModelBox(rotor_plate, 59, 113, -6.25F, -108.6F, -6.0F, 12, 3, 12, 0.0F, false));
		rotor_plate.cubeList.add(new ModelBox(rotor_plate, 26, 112, -12.75F, -89.6F, -17.0F, 26, 2, 9, 0.0F, false));
		rotor_plate.cubeList.add(new ModelBox(rotor_plate, 46, 112, -13.75F, -89.6F, 8.0F, 27, 2, 8, 0.0F, false));

		rotor_post_1 = new RendererModel(this);
		rotor_post_1.setRotationPoint(0.0F, -21.6F, 0.0F);
		time_rotor_slide_y.addChild(rotor_post_1);
		rotor_post_1.cubeList.add(new ModelBox(rotor_post_1, 198, 57, -2.0F, -20.0F, 9.0F, 4, 40, 4, 0.0F, false));
		rotor_post_1.cubeList.add(new ModelBox(rotor_post_1, 198, 57, -2.0F, -20.0F, -13.0F, 4, 40, 4, 0.0F, false));

		rotor_post_2 = new RendererModel(this);
		rotor_post_2.setRotationPoint(0.0F, -21.6F, 0.0F);
		setRotationAngle(rotor_post_2, 0.0F, -1.0472F, 0.0F);
		time_rotor_slide_y.addChild(rotor_post_2);
		rotor_post_2.cubeList.add(new ModelBox(rotor_post_2, 198, 57, -2.0F, -20.0F, 9.0F, 4, 40, 4, 0.0F, false));
		rotor_post_2.cubeList.add(new ModelBox(rotor_post_2, 198, 57, -2.0F, -20.0F, -13.0F, 4, 40, 4, 0.0F, false));

		rotor_post_3 = new RendererModel(this);
		rotor_post_3.setRotationPoint(0.0F, -21.6F, 0.0F);
		setRotationAngle(rotor_post_3, 0.0F, -2.0944F, 0.0F);
		time_rotor_slide_y.addChild(rotor_post_3);
		rotor_post_3.cubeList.add(new ModelBox(rotor_post_3, 198, 57, -2.0F, -20.0F, 9.0F, 4, 40, 4, 0.0F, false));
		rotor_post_3.cubeList.add(new ModelBox(rotor_post_3, 198, 57, -2.0F, -20.0F, -13.0F, 4, 40, 4, 0.0F, false));
	}

	public void render(ConsoleTile console) {
		
		//Rotor animations
		this.time_rotor_slide_y.offsetY = (float)Math.cos(console.flightTicks * 0.1) * 0.7F;
		this.glow_timerotor_slide_y.offsetY = this.time_rotor_slide_y.offsetY;
		
		//randomizer
		IncModControl inc = console.getControl(IncModControl.class);
		if(inc != null) {
			this.xyz_increment_rotate_z.rotateAngleZ = (float)Math.toRadians(inc.getAnimationTicks() * 18);
		}
		
		DimensionControl dim = console.getControl(DimensionControl.class);
		if(dim != null) {
			this.needle_rotate_z.rotateAngleZ = (float)Math.toRadians(dim.getAnimationTicks() * 18);
		}
		
		RandomiserControl rand = console.getControl(RandomiserControl.class);
		if(rand != null) {
			this.rotate_y.rotateAngleY = (float)Math.toRadians(rand.getAnimationTicks() * (18 * 2));
		}
		
		ThrottleControl throttle = console.getControl(ThrottleControl.class);
		if(throttle != null) {
			this.throttle_lever_rotate_x.rotateAngleX = -(float)Math.toRadians((90 * throttle.getAmount()) - 45);
		}
		
		HandbrakeControl hand = console.getControl(HandbrakeControl.class);
		if(hand != null) {
			this.handbreak_lever2_rotate_x.rotateAngleX = (float)Math.toRadians(hand.isFree() ? 45 : -45);
		}
		
		LandingTypeControl land = console.getControl(LandingTypeControl.class);
		if(land != null) {
			if(land.getLandType() == EnumLandType.DOWN) {
				this.slider_bar.offsetY = 0.2F;
			}
			else this.slider_bar.offsetY = 0F;
		}
		
		CommunicatorControl communicator = console.getControl(CommunicatorControl.class);
		if(communicator != null)
			this.bell_rotate_x.rotateAngleX = (float) Math.toRadians((Math.sin(Math.toRadians(communicator.getAnimationTicks() * 18))) * 10);
		
		
		
		
		this.side3.rotateAngleY = (float)Math.toRadians(0);
		this.side1.rotateAngleY = -(float)Math.toRadians(120);
		
		this.console.render(0.0625F);
		this.controls.render(0.0625F);
		this.time_rotor_slide_y.render(0.0625F);
		
		ModelHelper.renderPartBrightness(1F, glow_timerotor_slide_y, glow_baseoffset_1, glow_baseoffset_2, glow_baseoffset_3);
		
		GlStateManager.pushMatrix();
		GlStateManager.scaled(2, 2, 2);
		GlStateManager.translated(0.4, -1, 2);
		GlStateManager.rotated(-45, 1, 0, 0);
		GlStateManager.translated(-0.1, 0.6, 0.1);
		Minecraft.getInstance().getItemRenderer().renderItem(console.getSonicItem(), TransformType.NONE);
		GlStateManager.popMatrix();
	}
	
	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}