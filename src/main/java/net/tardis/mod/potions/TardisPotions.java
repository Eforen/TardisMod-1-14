package net.tardis.mod.potions;

import net.minecraft.potion.Effect;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.Helper;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class TardisPotions {

	public static final Effect MERCURY = register(new MercuryEffect(0xFFFFFF), "mercury");
	
	public static <T extends Effect> T register(T mercury, String name) {
		mercury.setRegistryName(Helper.createRL(name));
		return mercury;
	}
	
	@SubscribeEvent
	public static void register(Register<Effect> event) {
		event.getRegistry().registerAll(
				MERCURY
		);
	}
}
