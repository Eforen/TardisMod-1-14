package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction.Axis;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class YControl extends AxisControl{
	
	public YControl(ConsoleTile console, ControlEntity entity) {
		super(console, entity, Axis.Y);
	}

	@Override
	public EntitySize getSize() {

		if (this.getConsole() instanceof ToyotaConsoleTile){
			return EntitySize.flexible(0.125F, 0.125F);
		}
		if(getConsole() instanceof CoralConsoleTile)
			return EntitySize.flexible(0.0625F, 0.0625F);
		
		return EntitySize.flexible(0.0625F, 0.0625F);
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(-6.5 / 16.0, 10.5 / 16.0, 4 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(-0.4568998228936493, 0.625, 0.2641350176457876);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(0.3180649539937758, 0.5, -0.65);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(-0.8503918002329903, 0.5625, 0.49217136);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(1.0426042660083035, 0.4375, 0.2004973626698041);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.5625482953017267, 0.71875, 0.33457478063369095);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return new Vec3d(0.6938372031211344, 0.4, 0.39478559304915184);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vec3d(0.7204998497509241, 0.5, 0.6790756118658776);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vec3d(0.8001080830727285, 0.4375, -0.4954128603912813);
		
		return new Vec3d(-8.1 / 16.0, 9.85 / 16.0, -4.75 / 16.0);
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

}
