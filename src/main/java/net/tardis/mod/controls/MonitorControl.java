package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.constants.Constants.Gui;
import net.tardis.mod.contexts.gui.EntityContext;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.IMonitor;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorMode;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorView;

public class MonitorControl extends BaseControl implements IMonitor{

	private MonitorMode mode = MonitorMode.INFO;
	private MonitorView view = MonitorView.PANORAMIC;
	
	public MonitorControl(ConsoleTile console, ControlEntity entity) {
		super(console, entity);
	}

	@Override
	public EntitySize getSize() {

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.3125F, 0.3125F);
		}
		if (getConsole() instanceof ToyotaConsoleTile) {
			return EntitySize.flexible(0.5F, 0.5F);
		}
		if(this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.3125F, 0.3125F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.3125F, 0.3125F);
		
		return EntitySize.flexible(0.25F, 0.25F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		
		if(console.getWorld().isRemote) {
			
			if(Helper.InEitherHand(player, stack -> stack.getItem() == TItems.MONITOR_REMOTE)) {
				Tardis.proxy.openGUI(Gui.MONITOR_REMOTE, new EntityContext(this.getEntity()));
				return true;
			}
			
			if(console instanceof GalvanicConsoleTile) {
				Tardis.proxy.openGUI(Constants.Gui.MONITOR_MAIN_GALVANIC, null);
				return true;
			}
			if (console instanceof XionConsoleTile) {
				Tardis.proxy.openGUI(Constants.Gui.MONITOR_MAIN_XION, null);
				return true;
			}
			if (console instanceof ToyotaConsoleTile) {
				Tardis.proxy.openGUI(Constants.Gui.MONITOR_MAIN_TOYOTA, null);
				return true;
			}
			if (console instanceof CoralConsoleTile) {
				Tardis.proxy.openGUI(Constants.Gui.MONITOR_MAIN_CORAL, null);
				return true;
			}
			else {
				Tardis.proxy.openGUI(Constants.Gui.MONITOR_MAIN_EYE, null);
				return true;
			}
		}
		return true;
	}

	@Override
	public Vec3d getPos() {

		if(getConsole() instanceof CoralConsoleTile) {
			return new Vec3d(0.35, 0.6875, 0.525789599508169);
		}
		if (getConsole() instanceof ToyotaConsoleTile) {
			return new Vec3d(0.8083854212452479, 0.45, -0.4987289635469828);
		}
		if(this.getConsole() instanceof XionConsoleTile)
			return new Vec3d(0.29892208354437577, 0.8125, 0.478818161338896);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vec3d(0.010354376833276668, 0.4375, -0.8105755249479349);
		
		return new Vec3d(-0.5282378865176893, 0.59375, -0.2954572166748212);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return null;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putInt("mode", this.mode.ordinal());
		tag.putInt("view", this.view.ordinal());
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.mode = MonitorMode.values()[nbt.getInt("mode")];
		this.view = MonitorView.values()[nbt.getInt("view")];
	}

	@Override
	public MonitorMode getMode() {
		return mode;
	}

	@Override
	public MonitorView getView() {
		return this.view;
	}

	@Override
	public void setMode(MonitorMode mode) {
		this.mode = mode;
		this.markDirty();
	}

	@Override
	public void setView(MonitorView view) {
		this.view = view;
		this.markDirty();
	}

}
