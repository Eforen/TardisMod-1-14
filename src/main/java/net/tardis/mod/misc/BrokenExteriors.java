package net.tardis.mod.misc;

import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.exterior.ExteriorRegistry;

public enum BrokenExteriors {

	WATER(new BrokenExteriorType(() -> ExteriorRegistry.TRUNK, () -> TBlocks.console_nemo, () -> ConsoleRoom.BROKEN_PANAMAX)),
	STEAM(new BrokenExteriorType(() -> ExteriorRegistry.STEAMPUNK, () -> TBlocks.console_steam, () -> ConsoleRoom.BROKEN_STEAM));
	
	private BrokenExteriorType type;
	
	BrokenExteriors(BrokenExteriorType type){
		this.type = type;
	}
	
	public BrokenExteriorType getType() {
		return this.type;
	}
}
