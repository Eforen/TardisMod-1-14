package net.tardis.mod.misc;

import java.util.function.Supplier;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.blocks.ConsoleBlock;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.BrokenExteriorTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class BrokenExteriorType {

	private static final TranslationTextComponent HAVE_TARDIS = new TranslationTextComponent("status.exterior.broken.have_tardis");
	private static final TranslationTextComponent OTHERS_TARDIS = new TranslationTextComponent("status.exterior.broken.other_tardis");
	
	Supplier<IExterior> exterior;
	Supplier<ConsoleRoom> interior;
	Supplier<ConsoleBlock> consoleBlock;
	
	private ExteriorTile renderTile;
	
	public BrokenExteriorType(Supplier<IExterior> exterior, Supplier<ConsoleBlock> consoleBlock, Supplier<ConsoleRoom> room) {
		this.exterior = exterior;
		this.interior = room;
		this.consoleBlock = consoleBlock;
	}
	
	/**
	 * 
	 * @param world - world that contains the broken exterior
	 * @param pos - postion that the broken exterior is at
	 * @param type - the tardis interior dimension
	 */
	public void swapWithReal(ServerWorld world, BlockPos pos, DimensionType type, Direction dir) {
		
		TardisHelper.getConsole(world.getServer(), type).ifPresent(tile -> {
			
			tile.setExterior(this.exterior.get());
			tile.getUnlockManager().addExterior(this.exterior.get());
			tile.setLocation(world.getDimension().getType(), pos.down());
			tile.setDestination(world.getDimension().getType(), pos.down());
			tile.getExterior().place(tile, world.getDimension().getType(), pos.down());
			
		});
	}
	
	/**
	 * 
	 * @param world
	 * @param player
	 * @return - true if setup, if not it will warn the player of the reason on it's own
	 */
	public boolean setup(ServerWorld world, PlayerEntity player, BrokenExteriorTile ext) {
		if(!TardisHelper.hasTARDIS(world.getServer(), player.getUniqueID())) {
			if(ext.getConsoleDimension() == null) {
				DimensionType interior = TardisHelper.setupPlayersTARDIS((ServerPlayerEntity)player, this.consoleBlock.get(), this.interior.get());
				ext.setConsoleDimension(interior);
				return true;
			}
			player.sendStatusMessage(OTHERS_TARDIS, true);
			return false;
		}
		
		player.sendStatusMessage(HAVE_TARDIS, true);
		return false;
	}
	
	/**
	 * For rendering purposes only
	 * @return
	 */
	public ExteriorTile getExteriorTile(World world) {
		if(this.renderTile == null || this.renderTile.getWorld() == null) {
			this.renderTile = (ExteriorTile)this.exterior.get().getDefaultState().createTileEntity(world);
			this.renderTile.setWorld(world);
		}
		return this.renderTile;
	}
	
	/**
	 * Spawns the heart effect
	 * @param world
	 * @param pos
	 * @param rand
	 */
    public void spawnHappyPart(World world, BlockPos pos) {
		for(int i = 0; i < 18; ++ i) {
			double angle = Math.toRadians(i * 20);
			double x = Math.sin(angle);
			double z = Math.cos(angle);
			
			world.addParticle(ParticleTypes.HEART, pos.getX() + 0.5 + x, pos.getY() + world.getRandom().nextDouble(), pos.getZ() + 0.5 + z, 0, 0, 0);
		}
	}
	
}
