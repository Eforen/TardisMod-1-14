package net.tardis.mod.misc;

import net.minecraft.util.SoundEvent;
import net.tardis.mod.sounds.TSounds;

public enum DoorSounds implements IDoorSoundScheme{

	BASE(TSounds.DOOR_OPEN, TSounds.DOOR_CLOSE),
	WOOD(TSounds.WOOD_DOOR_OPEN, TSounds.WOOD_DOOR_CLOSE);
	
	final SoundEvent open;
	final SoundEvent closed;
	
	DoorSounds(SoundEvent open, SoundEvent closed){
		this.open = open;
		this.closed = closed;
	}
	
	@Override
	public SoundEvent getOpenSound() {
		return this.open;
	}
	@Override
	public SoundEvent getClosedSound() {
		return this.closed;
	}
}
