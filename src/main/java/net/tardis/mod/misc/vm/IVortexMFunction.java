package net.tardis.mod.misc.vm;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.world.World;

/**
 * Created by 50ap5ud5
 * on 5 May 2020 @ 8:03:22 pm
 */
public interface IVortexMFunction{
	/**
	 * General purpose methods that is called when you select a function on the gui
	 * @implNote Usually for Client side only
	 * @param world
	 * @param player
	 */
	void onActivated(World world,PlayerEntity player);
	/**
	 * Call this from a packet if we need to execute a function on the server
	 * @implNote Do not call this from the gui, as you cannot cast a ClientWorld to ServerWorld
	 * @param world
	 */
	void sendActionToServer(World world, ServerPlayerEntity player);
	/**
	 * Name of the function that shows in the GUI textbox
	 */
	String getNameKey();
	/**
	 * Returns if the player can access this function
	 */
	Boolean stateComplete();
	/**
	 * If the function is to be used on the server only, determines if we send a packet from the client side gui
	 */
	Boolean isServerSide();
}
