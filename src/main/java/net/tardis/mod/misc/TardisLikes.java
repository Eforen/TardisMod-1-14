package net.tardis.mod.misc;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.item.Item;
import net.minecraft.item.Items;

public class TardisLikes {

	
	public static List<TardisLikes> LIKES = new ArrayList<TardisLikes>();
	
	static {
		LIKES.add(new TardisLikes(Items.COMPASS, 5));
		LIKES.add(new TardisLikes(Items.FILLED_MAP, 10));
		LIKES.add(new TardisLikes(Items.ENDER_PEARL, 15));
		LIKES.add(new TardisLikes(Items.CLOCK, 20));
		LIKES.add(new TardisLikes(Items.ENDER_EYE, 20));
		LIKES.add(new TardisLikes(Items.BELL, 25));
	}
	
	
	private int loyaltyMod = 0;
	private Item item;
	
	public TardisLikes(Item item, int amount) {
		this.loyaltyMod = amount;
		this.item = item;
	}
	
	public Item getItem() {
		return item;
	}
	
	public int getLoyaltyMod() {
		return this.loyaltyMod;
	}
}
