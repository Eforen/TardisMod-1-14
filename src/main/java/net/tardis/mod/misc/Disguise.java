package net.tardis.mod.misc;

import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import org.apache.logging.log4j.Level;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.resources.IResource;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistryEntry;
import net.tardis.mod.Tardis;

public class Disguise implements IForgeRegistryEntry<Disguise>{

	private ResourceLocation registryName;
	private Supplier<BlockState> top;
	private Supplier<BlockState> bottom;
	private HashMap<BlockPos, BlockState> blocks = new HashMap<BlockPos, BlockState>();
	private int width, height;
	BiFunction<World, BlockPos, Boolean> isValid = (world, pos) -> false;
	
	
	public Disguise(Supplier<BlockState> top, Supplier<BlockState> bottom) {
		this.top = top;
		this.bottom = bottom;
	}
	
	public Disguise(Supplier<BlockState> both) {
		this(both, both);
	}
	
	public void readData(MinecraftServer server) {
		try {
			this.blocks.clear();
			IResource resource = server.getResourceManager().getResource(new ResourceLocation(this.registryName.getNamespace(), "disguises/" + this.registryName.getPath() + ".json"));
			JsonArray root = new JsonParser().parse(new InputStreamReader(resource.getInputStream())).getAsJsonArray();
			for(JsonElement ele : root) {
				JsonObject obj = ele.getAsJsonObject();
				Block block = ForgeRegistries.BLOCKS.getValue(new ResourceLocation(obj.get("block").getAsString()));
				int x = obj.get("x").getAsInt();
				int y = obj.get("y").getAsInt();
				int z = obj.get("z").getAsInt();
				this.blocks.put(new BlockPos(x, y, z).toImmutable(), block.getDefaultState());
			}
			resource.close();
			
		} catch (Exception e) {
			Tardis.LOGGER.catching(Level.DEBUG, e);
		}
		
		//set height and width
		for(BlockPos pos : this.blocks.keySet()) {
			if(Math.abs(pos.getX()) > this.width)
				this.width = Math.abs(pos.getX());
			if(Math.abs(pos.getZ()) > this.width)
				this.width = Math.abs(pos.getZ());
			
			if(pos.getY() > this.height)
				this.height = pos.getY();
		}
		
	}
	
	public void setValidationFunction(BiFunction<World, BlockPos, Boolean> func) {
		this.isValid = func;
	}

	public BlockState getTopState() {
		return top.get();
	}

	public BlockState getBottomState() {
		return this.bottom.get();
	}
	public HashMap<BlockPos, BlockState> getOtherBlocks() {
		return this.blocks;
	}

	public boolean getIsValid(World world, BlockPos pos) {
		return isValid.apply(world, pos);
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}

	@Override
	public Disguise setRegistryName(ResourceLocation name) {
		this.registryName = name;
		return this;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return this.registryName;
	}

	@Override
	public Class<Disguise> getRegistryType() {
		return Disguise.class;
	}
}
