package net.tardis.mod.itemgroups;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.items.TItems;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class TItemGroups {

    public static ItemGroup MAIN = new ItemGroup(Tardis.MODID + ".main") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(TItems.KEY_01);
        }
    };

    public static ItemGroup ROUNDELS = new ItemGroup(Tardis.MODID + ".roundels") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(TBlocks.roundel_concrete_black);
        }
    };

    public static ItemGroup FUTURE = new ItemGroup(Tardis.MODID + ".future") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(TBlocks.steel_grate_solid);
        }
    };

    public static ItemGroup MAINTENANCE = new ItemGroup(Tardis.MODID + ".maintenance") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(TItems.DEMAT_CIRCUIT);
        }
    };

    //Needs to exist
    @SubscribeEvent
    public static void registerItemGroups(FMLCommonSetupEvent event) {}

}
