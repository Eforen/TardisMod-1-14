package net.tardis.mod.containers;


import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Direction;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.containers.slot.EngineSlot;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public class EngineContainer extends Container{
	
	Direction panel;
	
	protected EngineContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	//Client
	public EngineContainer(int id, PlayerInventory inv, PacketBuffer buf) {
		this(TContainers.ENGINE, id);
		this.panel = Direction.byIndex(buf.readInt());
		DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
			Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(world -> {
				this.init(inv, world.getEngineInventoryForSide(this.panel));
			});
		});
	}
	
	//Server
	public EngineContainer(int id, PlayerInventory inv, PanelInventory engine, Direction panel) {
		this(TContainers.ENGINE, id);
		this.panel = panel;
		this.init(inv, engine);
	}
	
	public void init(PlayerInventory inv, PanelInventory engine) {
		
		for(int i = 0; i < engine.getSizeInventory(); ++i) {
			this.addSlot(new EngineSlot(this.panel, engine, i, 8 + (i * 18), 14));
		}
		
		//Add Players
		for(Slot s : Helper.createPlayerInv(inv, 0, -40)) {
			this.addSlot(s);
		}
		
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}

	//They need to actually place this for the minigame system
	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
	   return ItemStack.EMPTY;
	}
	
	public Direction getPanelDirection(){
		return this.panel;
	}
	

}
