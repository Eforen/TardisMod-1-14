package net.tardis.mod.contexts.gui;

import net.minecraft.client.Minecraft;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.monitors.IMonitorGui;
import net.tardis.mod.misc.GuiContext;

public class MonitorContext extends GuiContext{

	int guiID;
	
	public MonitorContext(int guiID) {
		this.guiID = guiID;
	}
	
	@OnlyIn(Dist.CLIENT)
	public IMonitorGui getMonitor() {
		Tardis.proxy.openGUI(guiID, null);
		return (IMonitorGui)Minecraft.getInstance().currentScreen;
	}
}
