package net.tardis.mod.entity.ai.humanoids;

import net.minecraft.block.Block;
import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.ai.goal.MoveToBlockGoal;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.tardis.mod.blocks.ChairBlock;
import net.tardis.mod.entity.ChairEntity;

public class SitInChairGoal extends MoveToBlockGoal{
	
	private int timeInChair = 0;
	private boolean stop = false;


	public SitInChairGoal(CreatureEntity p_i45888_1_, double p_i45888_2_, int p_i45888_4_) {
		super(p_i45888_1_, p_i45888_2_, p_i45888_4_);
	}
	
	@Override
	public boolean shouldContinueExecuting() {
		return !stop && super.shouldContinueExecuting();
	}

	@Override
	public void tick() {
		super.tick();
		
		if(this.creature.getRidingEntity() instanceof ChairEntity) {
			
			this.creature.rotationYaw = this.creature.getRidingEntity().rotationYaw;
			
			++this.timeInChair;
			if(this.timeInChair > 20 * 10) {
				this.creature.stopRiding();
				this.timeInChair = 0;
				this.stop = true;
			}
		}
		else {
			if(this.destinationBlock.withinDistance(this.creature.getPosition(), 2)) {
				Block block = this.creature.world.getBlockState(this.destinationBlock).getBlock();
				if(block instanceof ChairBlock)
					((ChairBlock)block).sit(this.creature.world, this.destinationBlock, this.creature);
			}
			
		}
	}

	@Override
	public boolean shouldExecute() {
		this.timeInChair = 0;
		this.stop = false;
		return super.shouldExecute();
	}

	@Override
	protected boolean shouldMoveTo(IWorldReader world, BlockPos pos) {
		if(world.getBlockState(pos).getBlock() instanceof ChairBlock) {
			if(world instanceof World && !ChairBlock.isOccupied((World)world, pos))
				return true;
		}
		return false;
	}

	@Override
	public void resetTask() {
		super.resetTask();
		this.stop = false;
		this.timeInChair = 0;
		this.creature.stopRiding();
	}

	@Override
	protected int getRunDelay(CreatureEntity creatureIn) {
		//Ten seconds + x seconds
		return 200 + creatureIn.getRNG().nextInt(40) * 20;
	}

}
