package net.tardis.mod.entity.ai.dalek.types;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.entity.DalekEntity;
import net.tardis.mod.items.TItems;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.registries.TardisRegistries;

/**
 * Created by Swirtzly & Spectre0789
 * on 20/10/2019 @ 18:17
 */
public interface DalekType extends IRegisterable<DalekType> {

    static void registerAll() {
    	TardisRegistries.registerRegisters(() -> {
	        TardisRegistries.DALEK_TYPE.register(Constants.DalekTypes.DALEK_TIMEWAR, new DalekBaseType("time_war").setSpawnItem(TItems.SPAWN_EGG_DALEK_TIMEWAR));
	        TardisRegistries.DALEK_TYPE.register(Constants.DalekTypes.DALEK_CLASSIC, new DalekBaseType("classic").setCanFly(false).setSpawnItem(TItems.SPAWN_EGG_DALEK_CLASSIC));
	        TardisRegistries.DALEK_TYPE.register(Constants.DalekTypes.DALEK_SW, new DalekTypeSpecialWeapon("special_weapons").setSpawnItem(TItems.SPAWN_EGG_DALEK_SW));
	        TardisRegistries.DALEK_TYPE.register(Constants.DalekTypes.DALEK_SEC, new DalekBaseType("sec").setSpawnItem(TItems.SPAWN_EGG_DALEK_SEC));
	        TardisRegistries.DALEK_TYPE.register(Constants.DalekTypes.DALEK_SUPREME, new DalekBaseType("supreme").setSpawnItem(TItems.SPAWN_EGG_DALEK_SUPREME));
	        TardisRegistries.DALEK_TYPE.register(Constants.DalekTypes.DALEK_IMPERIAL, new DalekBaseType("imperial").setSpawnItem(TItems.SPAWN_EGG_DALEK_IMPERIAL));
	
	        TItems.SPAWN_EGG_DALEK_TIMEWAR.setDalekType(Constants.DalekTypes.DALEK_TIMEWAR);
	        TItems.SPAWN_EGG_DALEK_CLASSIC.setDalekType(Constants.DalekTypes.DALEK_CLASSIC);
	        TItems.SPAWN_EGG_DALEK_SW.setDalekType(Constants.DalekTypes.DALEK_SW);
	        TItems.SPAWN_EGG_DALEK_SEC.setDalekType(Constants.DalekTypes.DALEK_SEC);
	        TItems.SPAWN_EGG_DALEK_SUPREME.setDalekType(Constants.DalekTypes.DALEK_SUPREME);
	        TItems.SPAWN_EGG_DALEK_IMPERIAL.setDalekType(Constants.DalekTypes.DALEK_IMPERIAL);
    	});
    }

    float getLaserScale();

    void specialUpdate(DalekEntity dalek);

    Vec3d getLaserColor(DalekEntity dalek);

    boolean canFly();

    DalekType setCanFly(boolean canFly);
    DalekType setLaserColor(Vec3d color);

    DalekType setLaserScale(float laserWidth);

    ResourceLocation getTexture();

    ResourceLocation getRegistryName();

    void onAttack(DalekEntity dalek, LivingEntity livingEntity, float distanceFactor);

    IDalekWeapon getDalekWeapon(DalekEntity dalek);

    Item getSpawnItem();

    SoundEvent getAttackSounds(DalekEntity dalek);

    SoundEvent getAmbientSounds(DalekEntity dalek);

    SoundEvent getDeathSound(DalekEntity dalek);

    SoundEvent getLaserSound(DalekEntity dalek);

    ITextComponent getName();

    DalekType setDalekWeapon(IDalekWeapon dalekWeapon);

    DalekType setAttackDamage(float damage);
    float getAttackDamage(DalekEntity dalek);

    interface IDalekWeapon {
        void onHit(DalekEntity dalek, World world, RayTraceResult rayTraceResult);
    }

    //TODO Spectre
    boolean isPowered(DalekEntity dalek, BlockPos pos);

    DalekType setSpawnItem(Item spawnTime);

}
