package net.tardis.mod.sounds;

public class SoundSchemeMaster extends SoundSchemeBasic {

	public SoundSchemeMaster() {
		super(() -> TSounds.MASTER_TAKEOFF, () -> TSounds.MASTER_LAND, () -> TSounds.TARDIS_FLY_LOOP);
	}
	
	@Override
	public int getLandTime() {
		return 260;
	}

	@Override
	public int getTakeoffTime() {
		return 360;
	}
	
}
