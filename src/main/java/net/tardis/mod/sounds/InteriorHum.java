package net.tardis.mod.sounds;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.registries.TardisRegistries;

public class InteriorHum implements IRegisterable<InteriorHum> {

    private ResourceLocation name;
    private SoundEvent event;
    private int duration;

    /**
     * @param event SoundEvent of the chosen hum
     * @param duration Duration of the sound in ticks
     */
    private InteriorHum(SoundEvent event, int duration) {
        this.event = event;
        this.duration = duration;
    }

    public static void registerAll(){
        TardisRegistries.registerRegisters(() -> {
        	TardisRegistries.HUM_REGISTRY.register("disabled", new InteriorHum(null, 0));
            TardisRegistries.HUM_REGISTRY.register("63", new InteriorHum(TSounds.TARDIS_HUM_63, 50));
            TardisRegistries.HUM_REGISTRY.register("70", new InteriorHum(TSounds.TARDIS_HUM_70, 620));
            TardisRegistries.HUM_REGISTRY.register("80", new InteriorHum(TSounds.TARDIS_HUM_80, 600));
            TardisRegistries.HUM_REGISTRY.register("copper", new InteriorHum(TSounds.TARDIS_HUM_COPPER, 620));
            TardisRegistries.HUM_REGISTRY.register("coral", new InteriorHum(TSounds.TARDIS_HUM_CORAL, 1060));
            TardisRegistries.HUM_REGISTRY.register("toyota", new InteriorHum(TSounds.TARDIS_HUM_TOYOTA, 580));
        });
    }

    public SoundEvent getEvent() {
        return event;
    }

    /**
     * @return Duration of the SoundEvent attr. in ticks
     */
    public int getDurationInTicks() {
        return duration;
    }

    public String getTranslatedName() {
        return new TranslationTextComponent("hum." + name).getFormattedText();
    }

    @Override
    public InteriorHum setRegistryName(ResourceLocation regName) {
        this.name = regName;
        return this;
    }

    @Override
    public ResourceLocation getRegistryName() {
        return name;
    }
}

