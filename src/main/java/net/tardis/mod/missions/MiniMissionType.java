package net.tardis.mod.missions;

import java.util.function.BiFunction;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class MiniMissionType extends ForgeRegistryEntry<MiniMissionType>{

	private IMissionBuilder<MiniMission> supplier;
	private BiFunction<ServerWorld, BlockPos, BlockPos> spawner;
	
	public MiniMissionType(IMissionBuilder<MiniMission> supplier, BiFunction<ServerWorld, BlockPos, BlockPos> spawner) {
		this.supplier = supplier;
		this.spawner = spawner;
	}
	
	public MiniMission create(World world, BlockPos pos, int range) {
		return this.supplier.create(world, pos, range);
	}
	
	public BlockPos spawnMissionStage(ServerWorld world, BlockPos pos) {
		return this.spawner.apply(world, pos);
	}
	
	
	
	public static interface IMissionBuilder<T extends MiniMission>{
		T create(World world, BlockPos pos, int range);
	}
	
}
