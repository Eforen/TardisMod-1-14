package net.tardis.mod.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.client.particle.ParticleManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.misc.IDontBreak;
import net.tardis.mod.misc.INeedItem;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.BrokenExteriorTile;

public class BrokenExteriorBlock extends TileBlock implements INeedItem, IDontBreak {

	public BlockItem ITEM = new BlockItemBrokenExterior(this);
	
	public BrokenExteriorBlock() {
		super(Properties.create(Material.BARRIER).hardnessAndResistance(9999F));
		this.setDefaultState(this.stateContainer.getBaseState().with(BlockStateProperties.WATERLOGGED, false));
	}
	
	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {

		if(worldIn.isRemote)
			return true;
		
		ItemStack stack = player.getHeldItem(handIn);
		TileEntity te = worldIn.getTileEntity(pos);
		
		if(te instanceof BrokenExteriorTile) {
			((BrokenExteriorTile)te).onPlayerRightClick(player, stack);
		}
		
		return true;
	}
	
	@Override
	@OnlyIn(Dist.CLIENT)
	public void animateTick(BlockState stateIn, World worldIn, BlockPos pos, Random rand) {
		if(rand.nextDouble() < 0.25) {
			for(int i = 0; i < 18; ++ i) {
				double angle = Math.toRadians(i * 20);
				double x = Math.sin(angle);
				double z = Math.cos(angle);


				worldIn.playSound(pos.getX(), pos.getY(), pos.getZ(), TSounds.ELECTRIC_SPARK, SoundCategory.BLOCKS, 0.05F, 1F, false);
				worldIn.addParticle(ParticleTypes.LAVA, pos.getX() + 0.5 + x, pos.getY() + rand.nextDouble(), pos.getZ() + 0.5 + z, 0, 0, 0);
			}
		}

	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		builder.add(BlockStateProperties.HORIZONTAL_FACING, BlockStateProperties.WATERLOGGED);
	}

	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		if(context.getPlayer() != null)
			return this.getDefaultState().with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite());
		return this.getDefaultState();
	}

	@Override
	public Item getItem() {
		return ITEM;
	}


    //This have been added to "increase" immersion, by removing running particles and breaking particles etc
    @Override
    public boolean addLandingEffects(BlockState state1, ServerWorld worldserver, BlockPos pos, BlockState state2, LivingEntity entity, int numberOfParticles) {
        return false;
    }

    @Override
    public boolean addRunningEffects(BlockState state, World world, BlockPos pos, Entity entity) {
        return false;
    }

    @Override
    public boolean addHitEffects(BlockState state, World worldObj, RayTraceResult target, ParticleManager manager) {
        return false;
    }

    @Override
    public boolean addDestroyEffects(BlockState state, World world, BlockPos pos, ParticleManager manager) {
        return false;
    }


	public static class BlockItemBrokenExterior extends BlockItem{

		public BlockItemBrokenExterior(Block blockIn) {
			super(blockIn, new Item.Properties().group(TItemGroups.MAIN));
		}

		@Override
		protected boolean canPlace(BlockItemUseContext cont, BlockState state) {
			return cont.getFace() == Direction.UP && cont.getWorld().getBlockState(cont.getPos().up()).isReplaceable(cont);
		}

		@Override
		protected boolean placeBlock(BlockItemUseContext context, BlockState state) {
			return context.getWorld().setBlockState(context.getPos().up(), state);
		}
		
	}


}
