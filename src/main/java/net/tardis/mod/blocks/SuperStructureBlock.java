package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.StructureBlock;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.tileentities.SuperStructureTile;
/**
 * Created by Spectre0987 on 19/5/2020
 * 
 */
public class SuperStructureBlock extends StructureBlock{
	
	public SuperStructureBlock() {
		super(Block.Properties.create(Material.IRON));
	}

	@Override
	public TileEntity createNewTileEntity(IBlockReader p_196283_1_) {
		return (TileEntity)new SuperStructureTile();
	}
	 
	 

}
