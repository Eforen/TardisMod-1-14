package net.tardis.mod.blocks;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.Tardis;

public class ExteriorFortuneBlock extends ExteriorBlock {

	private static String[] fortunes = {};
	
	public ExteriorFortuneBlock(){
		super();
	}
	
	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		//if(handIn == player.getActiveHand()) {
			if(hit.getFace() == state.get(BlockStateProperties.HORIZONTAL_FACING).getOpposite()) {
				if(worldIn.isRemote) {
					read();
					if(fortunes.length > 0)
						player.sendMessage(new StringTextComponent(fortunes[worldIn.rand.nextInt(fortunes.length - 1)]));
				}
				return true;
			}
		//}
		return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
	}
	
	@OnlyIn(Dist.CLIENT)
	public void read() {
		try {
			
			List<String> list = new ArrayList<String>();
			
			InputStream is = Minecraft.getInstance().getResourceManager().getResource(new ResourceLocation(Tardis.MODID, "fortunes.json")).getInputStream();
			JsonArray array = new JsonParser().parse(new JsonReader(new InputStreamReader(is))).getAsJsonArray();
			for(JsonElement ele : array){
				list.add(ele.getAsString());
			}
			fortunes = list.toArray(new String[0]);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}

}
