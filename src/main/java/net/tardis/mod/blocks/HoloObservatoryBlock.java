package net.tardis.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.contexts.gui.BlockPosGuiContext;

public class HoloObservatoryBlock extends TileBlock {

	public HoloObservatoryBlock(Properties prop) {
		super(prop);
	}

	@Override
	public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
		
		if(worldIn.isRemote)
			Tardis.proxy.openGUI(Constants.Gui.HOLO_OBSERVATORY, new BlockPosGuiContext(pos));
		
		return true;
	}

}
