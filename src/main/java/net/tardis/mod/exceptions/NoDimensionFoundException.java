package net.tardis.mod.exceptions;

public class NoDimensionFoundException extends Throwable {
    private String username;

    public NoDimensionFoundException(String username) {
        this.username = username;
    }

    @Override
    public String getMessage() {
        return "No TARDIS found for the player: " + username;
    }
}
