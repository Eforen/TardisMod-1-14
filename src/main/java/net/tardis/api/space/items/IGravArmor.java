package net.tardis.api.space.items;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;

public interface IGravArmor {

	boolean useNormalGrav(LivingEntity entity, ItemStack stack);
}
