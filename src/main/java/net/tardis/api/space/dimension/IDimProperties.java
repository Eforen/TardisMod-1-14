package net.tardis.api.space.dimension;

import net.minecraft.entity.Entity;
import net.minecraft.util.math.Vec3d;

public interface IDimProperties {

	Vec3d modMotion(Entity mot);
	
	default boolean hasAir() {
		return true;
	}
}
